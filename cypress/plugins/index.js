// ***********************************************************
// This example plugins/index.js can be used to load plugins
//
// You can change the location of this file or turn off loading
// the plugins file with the 'pluginsFile' configuration option.
//
// You can read more here:
// https://on.cypress.io/plugins-guide
// ***********************************************************

// This function is called when a project is opened or re-opened (e.g. due to
// the project's config changing)

const cucumber = require('cypress-cucumber-preprocessor').default
// const browserify = require('@cypress/browserify-preprocessor');
// const resolve = require('resolve');
const { initializeApp, cert } = require('firebase-admin/app');
const { getFirestore } = require('firebase-admin/firestore');
const _ = require('lodash');

db = null;

module.exports = (on, config) => {
  // require('@cypress/code-coverage/task')(on, config)
  // const options = {
  //   ...browserify.defaultOptions,
  //   typescript: resolve.sync('typescript', { baseDir: config.projectRoot }),
  // };

  on('task', {
    initFirebase(firebase_service_account) {
      console.log("Start:Inilializing Firebase database");
      return new Promise((res, reject) => {
        try {
          initializeApp({ credential: cert(firebase_service_account) });
          db = getFirestore();
        } catch (e) {
          console.log(e);
        }
        res('');
        console.log("End: Initializing Firebase database");
      });
    },
    deleteOrdersCollection(merchantId) {
      console.log("Start:Deleting Orders");
      return new Promise((res, reject) => {
        try {
          db.collection("Merchant").doc(merchantId).collection("orders").get().then((docs) => {
            docs.forEach(doc => {
              let id = doc.id;
              db.collection("Merchant").doc(merchantId).collection("orders").doc(id).delete();
            });
          });
        } catch (e) {
          console.log(e);
        }
        console.log("End: Deleting Orders");
        res('');
      })
    },
    addOrders(data) {
      console.log("Start:Creating Orders");
      return new Promise((res, reject) => {
        try {
          data.ordersData.forEach(order => {
            db.collection("Merchant").doc(data.merchantId).collection("orders").doc(order.id).set(order);
          });
        } catch (e) {
          console.log(e);
        }
        res('');
        console.log("End: Creating Orders");
      })
    },
    createMerchant(merchant) {
      console.log("Start:Creating the merchant using seed data");
      return new Promise((res, reject) => {
        const docRef = db.collection('Merchant').doc(merchant.id);
        console.log(merchant.doc);
        docRef.set(merchant.doc).then(data => {
          console.log("End: Creating the merchant using seed data");
          res('');
        }).catch(error => console.error(error));
      })
    },
    async updateMerchant(merchantDetails) {
      console.log(`Start:Creating the merchant with id: ${merchantDetails.id}`);
      return new Promise((res, reject) => {
        db.collection("Merchant").doc(merchantDetails.merchantId).collection("merchant").doc(merchantDetails.id).update(merchantDetails.merchant).then(() => {
          console.log('End:Merchant created');
          res('');
        }).catch(e => console.log(e));
      });
    },
    async setOrder(orderDetails) {
      console.log(`Start:Creating the order with id: ${orderDetails.id}`);
      return new Promise((res, reject) => {
        db.collection("Merchant").doc(orderDetails.merchantId).collection("orders").doc(orderDetails.id).set(orderDetails.order).then(() => {
          console.log('End:Order created');
          res('');
        }).catch(e => console.log(e));
      });
    },
    async getOrderById(data) {
      console.log(`Start:Getting the order with id:${data.url} ${data.orderId}, ${data.merchantId}`);
      return new Promise((res, reject) => {
        db.collection("Merchant").doc(data.merchantId).collection("orders").doc(data.orderId).get().then(order => {
          console.log("End:Order recieved", order.data());
          res(order.data());
        }).catch(e => console.log(e));
      })
    },
    async getMerchantDetails(data) {
      console.log(`Start:Getting the merchant details for merchant Id : ${data.merchantId}`);
      let merchantDetails = await db.collection('Merchant').doc(data.merchantId).get();
      console.log(merchantDetails.data());
      console.log(`End:Getting the merchant details for merchant Id : ${data.merchantId}`);
      if (!merchantDetails.exists) {
        console.log('No such document!');
      } else {
        console.log('Document data:', merchantDetails.data());
      }
      return merchantDetails.data();
    },
    async createSubcollection(data) {
      let merchantId = data.merchantId;
      let subCollectionName = data.subCollectionName;

      let index = _.findIndex(data.subcollections, o => o.id == subCollectionName);

      console.log(`For ${subCollectionName} subcollection index is ${index} and data is ${data.subcollections[index]}`)

      let subCollectionData = data.subcollections[index].subcollectionData;

      console.log(`Start:Creating the ${subCollectionName} using seed data`);
      console.log(`${subCollectionName} collection length : `, subCollectionData.length);
      let batch = db.batch();
      let count = 0;
      for (let element of subCollectionData) {
        let docRef = db.collection('Merchant').doc(merchantId).collection(subCollectionName).doc(element.documentId);
        batch.set(docRef, element);
        count = count + 1;
        if (count % 200 == 0) {
          console.log(`Started commiting ${subCollectionName} for count:`, count);
          await batch.commit();
          batch = db.batch();
          console.log(`Ended commiting ${subCollectionName} for count:`, count);
        }
      }
      if (count % 200 != 0) {
        console.log(`Started commiting ${subCollectionName} for count:`, count);
        await batch.commit();
        console.log(`Ended commiting ${subCollectionName} for count:`, count);
      }
      console.log(`End: Creating the ${subCollectionName} using seed data`);
      return null;
    },
    log(message) {
      console.log(message);
      return null
    },
  });

  on('file:preprocessor', cucumber())
  // add other tasks to be registered here

  // IMPORTANT to return the config object
  // with the any changed environment variables
  // return config
}

