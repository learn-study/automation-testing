const { FirebaseSDK } = require("./script-firebase-sdk");

const merchantID = "Demo";

const firebaseapp = setupFirebase();

cleanupCollection("Catalog");
cleanupCollection("display_positions");
cleanupCollection("itemsType_details");

function setupFirebase() {
    const firebaseapp = new FirebaseSDK();
    firebaseapp.initializeFirebase();
    firebaseapp.initializeFirestore();
    return firebaseapp;
};

async function cleanupCollection(collectionPath) {
    const collection = await firebaseapp.getFirestoreCollection(`Merchant/${merchantID}/${collectionPath}`)
    collection.forEach(document => {
        firebaseapp.deleteFirestoreDocument(`Merchant/${merchantID}/${collectionPath}`, document.id);
    });
};