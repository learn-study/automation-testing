const { FirebaseSDK } = require("./script-firebase-sdk");
const { Products } = require("./seed");
const { ProductsDisplayPosition } = require("./seed");
const { ItemTypes } = require("./seed");
const { ItemTypesDisplayPosition } = require("./seed");

const merchantID = "Demo";

const firebaseapp = setupFirebase();
setupProducts();
setupProductsDisplayPosition();
setupItemTypes();
setupItemTypesDisplayPosition();

function setupFirebase() {
    const firebaseapp = new FirebaseSDK();
    firebaseapp.initializeFirebase();
    firebaseapp.initializeFirestore();
    return firebaseapp;
};

function setupProducts() {
    Products.forEach(product => {
        firebaseapp.createFirestoreDocumentWithID(`Merchant/${merchantID}/Catalog`, product.id, product);
    });
};

function setupProductsDisplayPosition() {
    firebaseapp.createFirestoreDocumentWithID(`Merchant/${merchantID}/display_positions`, 'product_display_position', { productDisplayPosition: ProductsDisplayPosition });
};

function setupItemTypes() {
    ItemTypes.forEach(itemType => {
        firebaseapp.createFirestoreDocumentWithID(`Merchant/${merchantID}/itemsType_details`, itemType.id, itemType);
    });
};

function setupItemTypesDisplayPosition() {
    firebaseapp.createFirestoreDocumentWithID(`Merchant/${merchantID}/display_positions`, 'itemTypesDisplayPosition', { itemTypesDisplayPosition: ItemTypesDisplayPosition });
};