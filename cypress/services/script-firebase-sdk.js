const firebaseAdmin = require("firebase-admin");

class FirebaseSDK {
    credentials = {
        type: "service_account",
        project_id: "just-command-int01",
        private_key_id: "2bf104df1962ec4ba929d58ab1438a07c5f96088",
        private_key: "-----BEGIN PRIVATE KEY-----\nMIIEvQIBADANBgkqhkiG9w0BAQEFAASCBKcwggSjAgEAAoIBAQCOVhDlUb7TiMCr\nX3Fu8FJ64quf9QPhXZzJMKg6A4mUvkCGsdSZDVL+V3wuKhzZ02q5zQm4ttlV/iVi\nh9Qe6PyRhEOHoYJsZFxtHBRRTdUEMfjXeBBzB2oY47Ljz6C9+rNIcu/90G6vImbg\nBBFiD+bC/qie9stP9XQo5G7LkXUI+0s/IYNlB2QCY6cT6wcV+qHIBpGQ3vnkXTey\nOatZRd2WWrveVwgBTVdOlbJtzao4uqS65GAPUGWVQ+7dWjW+pA5iofE3DQjr5lv9\na4rIeu7eECsQYS7Yak0dhT6VRN9QNvXj3kc4vO8Ov3JQN5kU43d6zwCvh1UpA5+K\nt8Ll0hF7AgMBAAECggEACThfX9DWdl16yEnRJJ6XY951v6bfhCR27jwjVElmYCby\n3HF96WwziXD2r7GKrObvwu7FR8c7IkGUVQ2iGrPSU0V+GmBxUmmDPeN+bXCH8VL2\nC6nQvE1+p0jDYyklVdPSSUnTbLL+eISB+tZIn1GTC+PzMwOe/U7+d1JU2wWmedkK\nCK+3z1xoGnD3PlkBdohuTyRunqmaCoKCo44DnOvdfFQce258YMpkCdAMSYhSdmhf\nXF+0KiTfTsHPg1al3JekUiFuyadZO40nd//BG6xP7IqiMnJYAV1PpreRQucNCyZx\nbUT9OrAULJjkCTH5b4/4IT3c6hCZbt8NqdcDdPxCAQKBgQDE3VimDEMq+xLn2wpN\nOef0z0bkp133OeZCWsaA2aMIQH0Gj4xZpH8TzBzjUBXzbRm1ZFkCvm8nKK/vvJ/3\nQs1VVXb+3CdK3IDQkpqB+3QgJSb9wVqcnjugQtqKDOhEdIypWDromVTooyegVQE2\nDgnvDiHqVfQOLfiG6+poSV3DlwKBgQC5F4xfVPl7MEGHoBlXJ2A2fB8BoUoiQdQT\nNP+ZHZO7wYIpb5PbwL0/t+8dVRPveZvDUCsr1nIKbSdlu17+7ntUw36CaP/wswgY\nnTHutGsX1i8KwEeMiUyGBK8MV/DelaqBYwzd/7q/OUcO9gahU3fxhG9zI1+b6sXW\nedtYmeYNvQKBgHuEIV21beMmuprsTI9dmJJ6BpHqDMPT+QuC5u7rHiZMR+V45WdN\nFUo3k5xvoAYmWq37svlMwHLCTXjOUVOS/z/eKpYHTUPkC5JRmU2aliahCNDHovGH\nOgP4TiO1q3lf4u1RZUyQ/7wiufcXvnXK4yKxLEhZ+5F1+Gu201DxTzorAoGAK/Hz\nWg8KzLzAUjsoyRCIP2do1TI+L7QBbi3/IZNsJ//hr4TWTi6MwDwjgYtZmFn/KXg3\nR8k6ek9AM18xRsXIcm+X8c6MNdHL8kwgNtSPOLGZqavNUQioKomiJgQbJD3UpbOm\nsm6tIAmxXVP8bwEqUSVocKXNZW9wWWPFOFwRyuECgYEAtr239XJCAUwsk1ihQ7av\nHkh0f9HAGY3vIVR07jvhhGABAoA0VBqIXAPcmsX5lbwW2btLTo32ZFASur90sFlQ\n1/aLXvVZ5p/QCq+cY60zA4UMTNkH8/w9Y58tv6YZEvXTkk8Rr3urPhVvBzWHQBIv\nXVlgls4rGrMrx9MTtyfv56U=\n-----END PRIVATE KEY-----\n",
        client_email: "firebase-adminsdk-9j3ax@just-command-int01.iam.gserviceaccount.com",
        client_id: "109713937479093870718",
        auth_uri: "https://accounts.google.com/o/oauth2/auth",
        token_uri: "https://oauth2.googleapis.com/token",
        auth_provider_x509_cert_url: "https://www.googleapis.com/oauth2/v1/certs",
        client_x509_cert_url: "https://www.googleapis.com/robot/v1/metadata/x509/firebase-adminsdk-9j3ax%40just-command-int01.iam.gserviceaccount.com"
    };

    firestore;

    constructor(merchantID) {
        this.merchantID = merchantID;
    };

    initializeFirebase() {
        firebaseAdmin.initializeApp({
            credential: firebaseAdmin.credential.cert(this.credentials),
            storageBucket: `${this.credentials.project_id}.appspot.com`
        });
    }

    initializeFirestore() {
        this.firestore = firebaseAdmin.firestore();
    }

    getFirestoreCollection(collection) {
        return this.firestore.collection(`${collection}`).get();
    }

    getFirestoreDocument(collection, documentID) {
        return this.firestore.collection(`${collection}`).doc(`${documentID}`).get();
    }

    createFirestoreDocumentWithID(collection, documentID, newDocument) {
        return this.firestore.collection(`${collection}`).doc(`${documentID}`).set(newDocument);
    }

    updateFirestoreDocument(collection, documentID, updatedDocument) {
        return this.firestore.collection(`${collection}`).doc(`${documentID}`).update(updatedDocument);
    }

    deleteFirestoreDocument(collection, documentID) {
        return this.firestore.collection(`${collection}`).doc(`${documentID}`).delete();
    }
};

module.exports.FirebaseSDK = FirebaseSDK;