const Products = [
    {
        id: "LK97LKK23D0D6DVG",
        name: "Manchow Soup",
        name_hi: "Manchow Soup",
        name_gu: "Manchow Soup",
        name_mr: "Manchow Soup",
        price: 10,
        finalPrice: 10,
        rating: 5,
        downloadURLs: [
            "https://www.chefkunalkapur.com/wp-content/uploads/2021/03/Veg-Manchow-Soup-1300x867.jpeg?v=1621618246"
        ],
        itemTypes: [
            "Dinner"
        ],
        customizations: [
            {
                item: "Chicken",
                extraPrice: 5,
                type: "add"
            },
            {
                item: "Shredded Garlic",
                extraPrice: 0,
                type: "add"
            },
            {
                item: "Spices",
                extraPrice: 0,
                type: "remove"
            },
            {
                item: "Noodles",
                extraPrice: 5,
                type: "remove"
            }
        ],
        prepareTime: 20,
        offerAmount: 10,
        description: "Mixed with fresh green vegetables, this delicacy is designed with perfection and destined for success!",
        description_hi: "Mixed with fresh green vegetables, this delicacy is designed with perfection and destined for success!",
        description_gu: "Mixed with fresh green vegetables, this delicacy is designed with perfection and destined for success!",
        description_mr: "Mixed with fresh green vegetables, this delicacy is designed with perfection and destined for success!",
        acceptedDelay: 10,
        status: 1
    },
];

const ProductsDisplayPosition = [
    {
        id: "LK97LKK23D0D6DVG",
        displayPosition: 1
    }
];

const ItemTypes = [
    {
        ItemsTypeStatus: "Active",
        id: "A234DHJ3HGB9S09A",
        name: "Dinner",
        photo: {
            url: "https://images.cdn1.stockunlimited.net/preview1300/word-dinner_1531251.jpg"
        }
    }
];

const ItemTypesDisplayPosition = [
    {
        id: "A234DHJ3HGB9S09A",
        displayPosition: 1
    }
];

module.exports.Products = Products;
module.exports.ProductsDisplayPosition = ProductsDisplayPosition;
module.exports.ItemTypes = ItemTypes;
module.exports.ItemTypesDisplayPosition = ItemTypesDisplayPosition;