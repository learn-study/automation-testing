const axios = require('axios');

const API_KEY = "AIzaSyD96ejpsTfzNUbcCaWBdq6Z42cNLJhbFVY";

const SIGN_IN_ANONYMOUS = `https://identitytoolkit.googleapis.com/v1/accounts:signUp?key=${API_KEY}`;

const EXCHANGE_TOKENS = `https://identitytoolkit.googleapis.com/v1/accounts:signInWithCustomToken?key=${API_KEY}`;

const FIRESTORE_REST_API_URL = `https://firestore.googleapis.com/v1/projects/just-command-int01/databases/(default)/documents`;

const HEADERS = { headers: { 'Content-Type': 'application/json' } };

const CUSTOM_CLAIMS = "https://just-command-int01.uc.r.appspot.com/claims/";
class FirebaseSDK {
    IDToken;

    customToken;

    accessToken;

    firestore;

    merchantID;

    constructor(merchantID) {
        this.merchantID = merchantID;
    };

    async getIDToken() {
        await axios.post(SIGN_IN_ANONYMOUS, JSON.stringify({ returnSecureToken: true }), HEADERS)
            .then(response => {
                this.IDToken = response.data.idToken;
                return this.IDToken;
            })
            .catch(error => {
                console.log(error.response);
                return error.response;
            });
    };

    async getCustomToken(email, domain) {
        console.log(this.IDToken, domain)
        await axios.get(`${CUSTOM_CLAIMS}admin?email=${email}&domainName=localhost&Bearer=${this.IDToken}`, { headers: { 'Content-Type': 'application/json' } })
            .then(response => {
                this.customToken = response.data.customToken;
                return this.customToken;
            })
            .catch(error => {
                console.log(error.response);
                return error.response;
            });
    }

    async getAccessToken() {
        await axios.post(`${EXCHANGE_TOKENS}`, JSON.stringify({ token: this.customToken, returnSecureToken: true }), HEADERS)
            .then(response => {
                this.accessToken = response.data.idToken;
                return this.accessToken;
            })
            .catch(error => {
                console.log(error.response);
                return error.response;
            });
    }

    async getFirestoreDocument(path) {
        return await axios.get(`${FIRESTORE_REST_API_URL}/${path}`, { headers: { Authorization: `Bearer ${this.accessToken}` } })
            .then(response => {
                return response.data;
            })
            .catch(error => {
                console.log(error.response);
                return error.response;
            });
    };
};

module.exports.FirebaseSDK = FirebaseSDK;