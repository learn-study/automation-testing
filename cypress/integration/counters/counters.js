import { Before, Given, When, Then, And } from "cypress-cucumber-preprocessor/steps";
import { orders } from '../../fixtures/counters-test-data.json';

const { FirebaseSDK } = require("../../services/cypress-firebase-sdk");
import * as settings from "../../fixtures/settings.json";

let firebase;

Before({ tags: "@callBefore" }, async () => {
    cy.task('initFirebase', settings.firebase_service_account).then(async () => {
        cy.task('deleteOrdersCollection', "M1_Prakash_Parvat_Test").then(async () => {
            cy.wait(3000);
            cy.task('addOrders', { ordersData: orders, merchantId: "M1_Prakash_Parvat_Test" }).then(async () => {
                firebase = new FirebaseSDK("M1_Prakash_Parvat_Test");
                await firebase.getIDToken();
                await firebase.getCustomToken('som.shrivastava@gmail.com', "localhost");
                await firebase.getAccessToken();
                cy.visit(`http://localhost:4200/login?bearer=${firebase.customToken}&email=som.shrivastava@gmail.com&profileURL=https://brandyourself.com/blog/wp-content/uploads/linkedin-profile-picture-tips.png&profileName=Server`)
                window.sessionStorage.setItem(btoa('ADMIN_PROFILE_EMAIL'), "som.shrivastava@gmail.com");
                cy.wait(5000);
                cy.visit(`http://localhost:4200/mIndex/M1_Prakash_Parvat_Test/orders/grid?language=en`);
            });
        });
    });
});

Given('No of orders visible are {int}', (noOfOrders) => {
    cy.get('[data-cy=orders-grid-order-row]').should('have.length', noOfOrders);
});

Given('I am on orders grid page', () => {
    cy.url().should('include', 'orders/grid');
});

When('I verify counters and order rows', (table) => {
    const data = table.rowsHash();
    cy.get('[data-cy=counters-incart-label]').then(el => {
        expect(el.text().trim()).to.equal(data['inCartLabel']);
    });
    cy.get('[data-cy=counters-incart-orders]').then(el => {
        expect(el.text().trim()).to.equal(data['inCartOrdersCount']);
    });
    cy.get('[data-cy=counters-incart-suborders]').then(el => {
        expect(el.text().trim()).to.equal(data['inCartSubordersCount']);
    });

    cy.get('[data-cy=counters-in-approval-label]').then(el => {
        expect(el.text().trim()).to.equal(data['inApprovalLabel']);
    });
    cy.get('[data-cy=counters-in-approval-orders]').then(el => {
        expect(el.text().trim()).to.equal(data['inApprovalOrdersCount']);
    });
    cy.get('[data-cy=counters-in-approval-suborders]').then(el => {
        expect(el.text().trim()).to.equal(data['inApprovalSubordersCount']);
    });

    cy.get('[data-cy=counters-open-label]').then(el => {
        expect(el.text().trim()).to.equal(data['openLabel']);
    });
    cy.get('[data-cy=counters-open-orders]').then(el => {
        expect(el.text().trim()).to.equal(data['openOrdersCount']);
    });
    cy.get('[data-cy=counters-open-suborders]').then(el => {
        expect(el.text().trim()).to.equal(data['openSubordersCount']);
    });

    cy.get('[data-cy=counters-in-progress-label]').then(el => {
        expect(el.text().trim()).to.equal(data['inProgressLabel']);
    });
    cy.get('[data-cy=counters-in-progress-orders]').then(el => {
        expect(el.text().trim()).to.equal(data['inProgressOrdersCount']);
    });
    cy.get('[data-cy=counters-in-progress-suborders]').then(el => {
        expect(el.text().trim()).to.equal(data['inProgressSubordersCount']);
    });

    cy.get('[data-cy=counters-ready-label]').then(el => {
        expect(el.text().trim()).to.equal(data['readyLabel']);
    });
    cy.get('[data-cy=counters-ready-orders]').then(el => {
        expect(el.text().trim()).to.equal(data['readyOrdersCount']);
    });
    cy.get('[data-cy=counters-ready-suborders]').then(el => {
        expect(el.text().trim()).to.equal(data['readySubordersCount']);
    });

    cy.get('[data-cy=counters-in-delivery-label]').then(el => {
        expect(el.text().trim()).to.equal(data['inDeliveryLabel']);
    });
    cy.get('[data-cy=counters-in-delivery-orders]').then(el => {
        expect(el.text().trim()).to.equal(data['inDeliveryOrdersCount']);
    });
    cy.get('[data-cy=counters-in-delivery-suborders]').then(el => {
        expect(el.text().trim()).to.equal(data['inDeliverySubordersCount']);
    });

    cy.get('[data-cy=counters-delivered-label]').then(el => {
        expect(el.text().trim()).to.equal(data['deliveredLabel']);
    });
    cy.get('[data-cy=counters-delivered-orders]').then(el => {
        expect(el.text().trim()).to.equal(data['deliveredOrdersCount']);
    });
    cy.get('[data-cy=counters-delivered-suborders]').then(el => {
        expect(el.text().trim()).to.equal(data['deliveredSubordersCount']);
    });

    cy.get('[data-cy=counters-missing-table-label]').then(el => {
        expect(el.text().trim()).to.equal(data['missingTableLabel']);
    });
    cy.get('[data-cy=counters-missing-table-orders]').then(el => {
        expect(el.text().trim()).to.equal(data['missingTableOrdersCount']);
    });

    cy.get('[data-cy=counters-pending-payment-label]').then(el => {
        expect(el.text().trim()).to.equal(data['pendingPaymentLabel']);
    });
    cy.get('[data-cy=counters-pending-payment-orders]').then(el => {
        expect(el.text().trim()).to.equal(data['pendingPaymentOrdersCount']);
    });

    cy.get('[data-cy=counters-reset-label]').then(el => {
        expect(el.text().trim()).to.equal('Reset');
    });

    cy.get('[data-cy=counters-all-label]').then(el => {
        expect(el.text().trim()).to.equal('All');
    });
});

When('I filter orders by status {string}', (status) => {
    const ids = {
        "In-Cart": "incart",
        "Open": "open",
        "In-Approval": "in-approval",
        "In-Progress": "in-progress",
        "Prepared": "ready",
        "In-Delivery": "in-delivery",
        "Delivered": "delivered",
    };
    cy.get(`[data-cy=counters-${ids[status]}-label]`).click();
});

Then('I verify orders', (table) => {
    const data = table.rowsHash();
    cy.get('[data-cy=orders-grid-order-row]').should('have.length', data['noOfOrders']);
    for (let i = 0; i < data['noOfOrders']; i++) {
        if (data['status']) {
            cy.get('[data-cy=orders-grid-order-row]').eq(i).get('[data-cy=orders-grid-order-status]').eq(i).children().eq(0).children().eq(1).then(el => {
                expect(el.text().trim()).to.equal(data['status'].split(',')[i]);
            });
        }
        //     cy.get('[data-cy=orders-grid-order-row]').eq(i).get('[data-cy=orders-grid-payment-status]').eq(i).children().eq(0).children().eq(1).then(el => {
        //         expect(el.text().trim()).to.equal(data['paymentStatus'].split(',')[i]);
        //     });
        //     cy.get('[data-cy=orders-grid-order-row]').eq(i).get('[data-cy=orders-grid-del-qty]').eq(i).then(el => {
        //         expect(el.text().trim()).to.equal(data['orderQuantities'].split(',')[i]);
        //     });
        //     cy.get('[data-cy=orders-grid-order-row]').eq(i).get('[data-cy=orders-gid-grand-total-value]').eq(i).then(el => {
        //         expect(el.text().trim()).to.equal(data['grandTotals'].split(',')[i]);
        //     });
    }
});

Given('The orders are filtered by {string} status and label is {string}', (status, filterLabel) => {
    const ids = {
        "In-Cart": "incart",
        "Open": "open",
        "In-Approval": "in-approval",
        "In-Progress": "in-progress",
        "Prepared": "ready",
        "In-Delivery": "in-delivery",
        "Delivered": "delivered",
    };
    cy.get('[data-cy=orders-grid-filter-by-status]').then(el => {
        expect(el.text().trim()).to.equal(filterLabel);
    });
});

When('I close filter', () => {
    cy.get('[data-cy=close-filter-button]').click({ force: true });
});