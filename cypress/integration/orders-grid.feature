Feature: Orders Grid Test

    @callBefore
    Scenario: Verify the order on orders page
        Given I am on orders grid page
        Then I verify order at position 1
            | orderType            | Dine In                                                     |
            | status               | Open                                                        |
            | paymentStatus        | Pending                                                     |
            | table                | R-43                                                        |
            | servers              | (Ritul, rver1)                                              |
            | token                | YY-14                                                       |
            | totalPrice           | ₹347.42                                                     |
            | totalQuantity        | 4 / 0                                                       |
            | deliveryTime         | 53 mins                                                     |
            | noOfSuborders        | 4                                                           |
            | items                | Pancake,Chocolate Fantacy,Soup Deliccy,Cupcake Strawberries |
            | itemPrices           | ₹30.00,₹250.00,₹28.50,₹19.25                                |
            | itemQuantities       | 1,1,1,1                                                     |
            | suborderStatus       | Open,Open,Open,Open                                         |
            | suborderDeliveryTime | 16 mins,53 mins,10 mins,7 mins                              |
            | suborderPrepareTime  | 16 mins,53 mins,10 mins,7 mins                              |
        And I go to "prepare" page

    Scenario: Verify the suborders on prepare page
        Given I am on "prepare" page "grid" view
        And I see 4 suborders
        Then I verify the suborders in prepare page
            | noOfSuborders | 4                                                           |
            | items         | Pancake,Chocolate Fantacy,Soup Deliccy,Cupcake Strawberries |
            | quantities    | 1,1,1,1                                                     |
            | deliveryTime  | 16 mins,53 mins,10 mins,7 mins                              |
            | prepareTime   | 16 mins,53 mins,10 mins,7 mins                              |
            | tables        | R-43,R-43,R-43,R-43                                         |
            | status        | Open,Open,Open,Open                                         |
        And I go to "orders" page

    Scenario: Add notes in orders page and verify in prepare
        When I add note: "less spicy" to suborder at position 1
        And I go to "prepare" page
        Then I verify the suborders in prepare page
            | noOfSuborders | 4                                                           |
            | items         | Pancake,Chocolate Fantacy,Soup Deliccy,Cupcake Strawberries |
            | quantities    | 1,1,1,1                                                     |
            | deliveryTime  | 16 mins,53 mins,10 mins,7 mins                              |
            | prepareTime   | 16 mins,53 mins,10 mins,7 mins                              |
            | tables        | R-43,R-43,R-43,R-43                                         |
            | status        | Open,Open,Open,Open                                         |
            | notes         | less spicy                                                  |
        And I go to "orders" page

    Scenario: Add Customization from orders page and verify in prepare page
        When I select customization: "asdf" of type: "add" to suborder at position 2
        And I go to "prepare" page
        Then I verify the suborders in prepare page
            | noOfSuborders  | 4                                                           |
            | items          | Pancake,Chocolate Fantacy,Soup Deliccy,Cupcake Strawberries |
            | quantities     | 1,1,1,1                                                     |
            | deliveryTime   | 16 mins,53 mins,10 mins,7 mins                              |
            | prepareTime    | 16 mins,53 mins,10 mins,7 mins                              |
            | tables         | R-43,R-43,R-43,R-43                                         |
            | status         | Open,Open,Open,Open                                         |
            | notes          | less spicy                                                  |
            | customizations | asdf(+)                                                     |
        And I go to "orders" page

    Scenario: Remove Customization from orders page and verify in prepare page
        When I select customization: "asdf" of type: "add" to suborder at position 2
        And I go to "prepare" page
        Then I verify the suborders in prepare page
            | noOfSuborders  | 4                                                           |
            | items          | Pancake,Chocolate Fantacy,Soup Deliccy,Cupcake Strawberries |
            | quantities     | 1,1,1,1                                                     |
            | deliveryTime   | 16 mins,53 mins,10 mins,7 mins                              |
            | prepareTime    | 16 mins,53 mins,10 mins,7 mins                              |
            | tables         | R-43,R-43,R-43,R-43                                         |
            | status         | Open,Open,Open,Open                                         |
            | notes          | less spicy                                                  |
            | customizations |                                                             |
        And I go to "orders" page

    Scenario: Add Quantity to a suborder from orders page
        When I "add" quantity 1 to suborder with quantity 1 at position 1
        Then I verify order at position 1
            | orderType            | Dine In                                                     |
            | status               | Open                                                        |
            | paymentStatus        | Pending                                                     |
            | table                | R-43                                                        |
            | servers              | (Ritul, rver1)                                              |
            | token                | YY-14                                                       |
            | totalPrice           | ₹379.22                                                     |
            | totalQuantity        | 5 / 0                                                       |
            | deliveryTime         | 53 mins                                                     |
            | noOfSuborders        | 4                                                           |
            | items                | Pancake,Chocolate Fantacy,Soup Deliccy,Cupcake Strawberries |
            | itemPrices           | ₹30.00,₹250.00,₹28.50,₹19.25                                |
            | itemQuantities       | 2,1,1,1                                                     |
            | suborderStatus       | Open,Open,Open,Open                                         |
            | suborderDeliveryTime | 16 mins,53 mins,10 mins,7 mins                              |
            | suborderPrepareTime  | 16 mins,53 mins,10 mins,7 mins                              |
        And I go to "prepare" page

    Scenario: Verify suborders in prepare after changing suborder item quantity
        Given I am on "prepare" page "grid" view
        And I see 4 suborders
        Then I verify the suborders in prepare page
            | noOfSuborders | 4                                                           |
            | items         | Pancake,Chocolate Fantacy,Soup Deliccy,Cupcake Strawberries |
            | quantities    | 2,1,1,1                                                     |
            | deliveryTime  | 16 mins,53 mins,10 mins,7 mins                              |
            | prepareTime   | 16 mins,53 mins,10 mins,7 mins                              |
            | tables        | R-43,R-43,R-43,R-43                                         |
            | status        | Open,Open,Open,Open                                         |
        And I go to "orders" page

    Scenario: Remove Quantity to a suborder from orders page
        When I "remove" quantity 1 to suborder with quantity 2 at position 1
        Then I verify order at position 1
            | orderType            | Dine In                                                     |
            | status               | Open                                                        |
            | paymentStatus        | Pending                                                     |
            | table                | R-43                                                        |
            | servers              | (Ritul, rver1)                                              |
            | token                | YY-14                                                       |
            | totalPrice           | ₹347.42                                                     |
            | totalQuantity        | 4 / 0                                                       |
            | deliveryTime         | 53 mins                                                     |
            | noOfSuborders        | 4                                                           |
            | items                | Pancake,Chocolate Fantacy,Soup Deliccy,Cupcake Strawberries |
            | itemPrices           | ₹30.00,₹250.00,₹28.50,₹19.25                                |
            | itemQuantities       | 1,1,1,1                                                     |
            | suborderStatus       | Open,Open,Open,Open                                         |
            | suborderDeliveryTime | 16 mins,53 mins,10 mins,7 mins                              |
            | suborderPrepareTime  | 16 mins,53 mins,10 mins,7 mins                              |
        And I go to "prepare" page

    Scenario: Verify suborders in prepare after changing suborder item quantity
        Given I am on "prepare" page "grid" view
        And I see 4 suborders
        Then I verify the suborders in prepare page
            | noOfSuborders | 4                                                           |
            | items         | Pancake,Chocolate Fantacy,Soup Deliccy,Cupcake Strawberries |
            | quantities    | 1,1,1,1                                                     |
            | deliveryTime  | 16 mins,53 mins,10 mins,7 mins                              |
            | prepareTime   | 16 mins,53 mins,10 mins,7 mins                              |
            | tables        | R-43,R-43,R-43,R-43                                         |
            | status        | Open,Open,Open,Open                                         |

    Scenario: Change status from prepare and verify in orders page
        Given Suborder at position 1 has name "Pancake" and status "open" with label "Open"
        When I change the status of suborder at positions "1" to "in_progress"
        And I go to "orders" page
        Then I verify order at position 1
            | orderType            | Dine In                                                     |
            | status               | Open                                                        |
            | paymentStatus        | Pending                                                     |
            | table                | R-43                                                        |
            | servers              | (Ritul, rver1)                                              |
            | token                | YY-14                                                       |
            | totalPrice           | ₹347.42                                                     |
            | totalQuantity        | 4 / 0                                                       |
            | deliveryTime         | 53 mins                                                     |
            | noOfSuborders        | 4                                                           |
            | items                | Pancake,Chocolate Fantacy,Soup Deliccy,Cupcake Strawberries |
            | itemPrices           | ₹30.00,₹250.00,₹28.50,₹19.25                                |
            | itemQuantities       | 1,1,1,1                                                     |
            | suborderStatus       | In-Progress,Open,Open,Open                                  |
            | suborderDeliveryTime | 15 mins,53 mins,10 mins,7 mins                              |
            | suborderPrepareTime  | 16 mins,53 mins,10 mins,7 mins                              |

    Scenario: Add delivery time in suborder from orders page and verify in prepare page
        When I "add" 5 minutes to the suborder at position 1
        Then I verify order at position 1
            | orderType            | Dine In                                                     |
            | status               | Open                                                        |
            | paymentStatus        | Pending                                                     |
            | table                | R-43                                                        |
            | servers              | (Ritul, rver1)                                              |
            | token                | YY-14                                                       |
            | totalPrice           | ₹347.42                                                     |
            | totalQuantity        | 4 / 0                                                       |
            | deliveryTime         | 53 mins                                                     |
            | noOfSuborders        | 4                                                           |
            | items                | Pancake,Chocolate Fantacy,Soup Deliccy,Cupcake Strawberries |
            | itemPrices           | ₹30.00,₹250.00,₹28.50,₹19.25                                |
            | itemQuantities       | 1,1,1,1                                                     |
            | suborderStatus       | In-Progress,Open,Open,Open                                  |
            | suborderDeliveryTime | 20 mins,53 mins,10 mins,7 mins                              |
            | suborderPrepareTime  | 16 mins,53 mins,10 mins,7 mins                              |
        And I go to "prepare" page
        Then I verify the suborders in prepare page
            | noOfSuborders | 4                                                           |
            | items         | Pancake,Chocolate Fantacy,Soup Deliccy,Cupcake Strawberries |
            | quantities    | 1,1,1,1                                                     |
            | deliveryTime  | 20 mins,53 mins,10 mins,7 mins                              |
            | prepareTime   | 16 mins,53 mins,10 mins,7 mins                              |
            | tables        | R-43,R-43,R-43,R-43                                         |
            | status        | In Progress,Open,Open,Open                                  |

    Scenario: Change suborder time from prepare page and change suborder status to prepared
        # change suborder time
        Given Suborder at position 1 has name "Pancake" and status "in-progress" with label "In Progress"
        When I change the status of suborder at positions "1" to "prepared"
        And I go to "orders" page
        Then I verify order at position 1
            | orderType            | Dine In                                                     |
            | status               | Open                                                        |
            | paymentStatus        | Pending                                                     |
            | table                | R-43                                                        |
            | servers              | (Ritul, rver1)                                              |
            | token                | YY-14                                                       |
            | totalPrice           | ₹347.42                                                     |
            | totalQuantity        | 4 / 0                                                       |
            | deliveryTime         | 53 mins                                                     |
            | noOfSuborders        | 4                                                           |
            | items                | Pancake,Chocolate Fantacy,Soup Deliccy,Cupcake Strawberries |
            | itemPrices           | ₹30.00,₹250.00,₹28.50,₹19.25                                |
            | itemQuantities       | 1,1,1,1                                                     |
            | suborderStatus       | Prepared,Open,Open,Open                                     |
            | suborderDeliveryTime | 20 mins,53 mins,10 mins,7 mins                              |
            | suborderPrepareTime  | 16 mins,53 mins,10 mins,7 mins                              |

    Scenario: Change suborder status to "In Delivery" in orders page and verify in prepare page
        When I change the status to "In-Delivery" of suborder at positions "1" in orders page
        And I go to "prepare" page
        Then I verify the suborders in prepare page
            | noOfSuborders | 3                                                   |
            | items         | Chocolate Fantacy,Soup Deliccy,Cupcake Strawberries |
            | quantities    | 1,1,1                                               |
            | deliveryTime  | 53 mins,10 mins,7 mins                              |
            | prepareTime   | 53 mins,10 mins,7 mins                              |
            | tables        | R-43,R-43,R-43                                      |
            | status        | Open,Open,Open                                      |

    Scenario: Change status of all suborder to prepare from prepare page
        When I change the status of suborder at positions "1,2,3" to "prepared"
        And I change the status of suborder at positions "1,2,3" to "prepared"
        And I go to "orders" page
        Then I verify order at position 1
            | orderType            | Dine In                                                     |
            | status               | Prepared                                                    |
            | paymentStatus        | Pending                                                     |
            | table                | R-43                                                        |
            | servers              | (Ritul, rver1)                                              |
            | token                | YY-14                                                       |
            | totalPrice           | ₹347.42                                                     |
            | totalQuantity        | 4 / 0                                                       |
            | deliveryTime         | 53 mins                                                     |
            | noOfSuborders        | 4                                                           |
            | items                | Pancake,Chocolate Fantacy,Soup Deliccy,Cupcake Strawberries |
            | itemPrices           | ₹30.00,₹250.00,₹28.50,₹19.25                                |
            | itemQuantities       | 1,1,1,1                                                     |
            | suborderStatus       | In-Delivery,Prepared,Prepared,Prepared                      |
            | suborderDeliveryTime | 19 mins,53 mins,10 mins,7 mins                              |
            | suborderPrepareTime  | 16 mins,53 mins,10 mins,7 mins                              |

    Scenario: Change status of all the suborders to delivered
        When I change the status to "Delivered" of suborder at positions "1,2,3,4" in orders page
        And I close orders row
        Then I verify order at position 1
            | orderType            | Dine In                                                     |
            | status               | Delivered                                                   |
            | paymentStatus        | Pending                                                     |
            | table                | R-43                                                        |
            | servers              | (Ritul, rver1)                                              |
            | token                | YY-14                                                       |
            | totalPrice           | ₹347.42                                                     |
            | totalQuantity        | 4 / 4                                                       |
            | deliveryTime         |                                                             |
            | noOfSuborders        | 4                                                           |
            | items                | Pancake,Chocolate Fantacy,Soup Deliccy,Cupcake Strawberries |
            | itemPrices           | ₹30.00,₹250.00,₹28.50,₹19.25                                |
            | itemQuantities       | 1,1,1,1                                                     |
            | suborderStatus       | Delivered,Delivered,Delivered,Delivered                     |
            | suborderDeliveryTime |                                                             |
            | suborderPrepareTime  | 16 mins,53 mins,10 mins,7 mins                              |
        And I go to "prepare" page
        Then I verify the suborders in prepare page
            | noOfSuborders | 0 |

    # Scenario: Change status of order to complete

# Scenario: Open Orders Grid Page
#     Given I am on orders grid page
#     # When I verify counters
#     Then I verify orders
#         | noOfOrders      | 7                                                                   |
#         | tables          | New Seat,R-234,,,R-43,,R-43                                         |
#         | status          | In-Progress,Open,Delivered,Prepared,In-Delivery,In-Approval,In-Cart |
#         | paymentStatus   | Pending,Pending,Cash On Delivery,Pending,Pending,Pending,Pending    |
#         | grandTotals     | ₹181.99,₹150.19,₹137.64,₹99.64,₹181.99,₹549.61,₹347.42              |
#         | orderTypes      | Dine In,Dine In,Delivery,Dine In,Dine In,Pick Up,Dine In            |
#         | orderQuantities | 5 / 0,4 / 0,3 / 3,2 / 0,5 / 0,6 / 0,4 / 0                           |
#     And I go to "Seats" view page

# Scenario: Filter Order By Seat
#     Given I am on seats view page
#     And I see table "R-43" with servers "Ritul, rver1"
#     When I select seat "R-43"
#     Then I verify orders
#         | noOfOrders      | 2                   |
#         | tables          | R-43,R-43           |
#         | status          | In-Delivery,In-Cart |
#         | paymentStatus   | Pending,Pending     |
#         | grandTotals     | ₹181.99,₹347.42     |
#         | orderTypes      | Dine In,Dine In     |
#         | orderQuantities | 5 / 0,4 / 0         |
#     And I verify filter label "Filtered By Seat: R-43"

# Scenario: Close Filter By Seat
#     Given Filter label is "Filtered By Seat: R-43"
#     When I close filter
#     Then I verify orders
#         | noOfOrders      | 7                                                                   |
#         | tables          | New Seat,R-234,,,R-43,,R-43                                         |
#         | status          | In-Progress,Open,Delivered,Prepared,In-Delivery,In-Approval,In-Cart |
#         | paymentStatus   | Pending,Pending,Cash On Delivery,Pending,Pending,Pending,Pending    |
#         | grandTotals     | ₹181.99,₹150.19,₹137.64,₹99.64,₹181.99,₹549.61,₹347.42              |
#         | orderTypes      | Dine In,Dine In,Delivery,Dine In,Dine In,Pick Up,Dine In            |
#         | orderQuantities | 5 / 0,4 / 0,3 / 3,2 / 0,5 / 0,6 / 0,4 / 0                           |

# Scenario: Open Orders Grid Page
#     Given I am on orders grid page
#     # When I verify counters
#     Then I verify orders
#         | noOfOrders      | 7                                                                   |
#         | tables          | New Seat,R-234,,,R-43,,R-43                                         |
#         | status          | In-Progress,Open,Delivered,Prepared,In-Delivery,In-Approval,In-Cart |
#         | paymentStatus   | Pending,Pending,Cash On Delivery,Pending,Pending,Pending,Pending    |
#         | grandTotals     | ₹181.99,₹150.19,₹137.64,₹99.64,₹181.99,₹549.61,₹347.42              |
#         | orderTypes      | Dine In,Dine In,Delivery,Dine In,Dine In,Pick Up,Dine In            |
#         | orderQuantities | 5 / 0,4 / 0,3 / 3,2 / 0,5 / 0,6 / 0,4 / 0                           |
#     And I go to "Suborder" view page

# Scenario: Filter Order By Suborder
#     Given I am on suborders view page
#     And I see suborder at position 2
#         | item        | Chocolate Fantacy |
#         | quantity    | 1                 |
#         | status      | Open              |
#         | itemPrice   | ₹250.00           |
#         | prepareTime | 53 mins           |
#     When I select Suborder at position 2
#     Then I verify orders
#         | noOfOrders      | 1       |
#         | tables          | R-43    |
#         | status          | In-Cart |
#         | paymentStatus   | Pending |
#         | grandTotals     | ₹347.42 |
#         | orderTypes      | Dine In |
#         | orderQuantities | 4 / 0   |
#         | token           | YY-14   |
#     And I verify token filter label "Filtered By Token Code - YY-14"

# Scenario: Close Filter By Suborder
#     Given Filter by token label is "Filtered By Token Code - YY-14"
#     When I close filter
#     Then I verify orders
#         | noOfOrders      | 7                                                                   |
#         | tables          | New Seat,R-234,,,R-43,,R-43                                         |
#         | status          | In-Progress,Open,Delivered,Prepared,In-Delivery,In-Approval,In-Cart |
#         | paymentStatus   | Pending,Pending,Cash On Delivery,Pending,Pending,Pending,Pending    |
#         | grandTotals     | ₹181.99,₹150.19,₹137.64,₹99.64,₹181.99,₹549.61,₹347.42              |
#         | orderTypes      | Dine In,Dine In,Delivery,Dine In,Dine In,Pick Up,Dine In            |
#         | orderQuantities | 5 / 0,4 / 0,3 / 3,2 / 0,5 / 0,6 / 0,4 / 0                           |

# # Scenario: Verify Suborders on Prepare Page
# #     Given I am on Prepare Page
# #     And I verify counters and suborders on prepare page
# #     When I change status To "in-progress" of suborder at position 1
# #     Then I verify counters and suborders on prepare page

# # Scenario: Scenario name
