
import { Given, Before, Then, And, When } from "cypress-cucumber-preprocessor/steps";
import * as merchant from '../../fixtures/M1_Prakash_Parvat_merchant.json'
import * as settings from "../../fixtures/settings.json";
import * as _ from 'lodash';
let userId = ""
Given('I open guest order menu page', () => {
  cy.task("initFirebase", settings.firebase_service_account);
  cy.visit('http://localhost:58676/go/mIndex/M1_Prakash_Parvat_Test?language=en');
  cy.contains('Prakash', { timeout: 30000 });
  cy.url().should('include', 'http://localhost:58676/M1_Prakash_Parvat_Test');
  let url = cy.url().toString();
  userId = url.substr(url.indexOf('user_id=') + 8, 28);
});
let orderDetails = {
  "documentId": "2d9n7IGi5vX4ZfNlJ0gp",
  "entryDate": {
      "seconds": 1634924867,
      "nanoseconds": 831000000
  },
  "tokenCode": "AA-44",
  "merchantId": "M1_Prakash_Parvat_Test",
  "adjustedType": "Less",
  "userId": "jlHUP4pHUsNjJUWjBoypxmeFL0b9",
  "adjustedAmt": 0,
  "id": "2d9n7IGi5vX4ZfNlJ0gp",
  "totalQuantity": 1,
  "versionId": 20,
  "delivery": 0,
  "status": 8,
  "date": {
      "seconds": 1635307755,
      "nanoseconds": 912000000
  },
  "totalPrice": 30,
  "orderBy": 1,
  "paymentStatus": 0,
  "subOrders": {
      "9OnbBop3svv1KHfFIEpn": {
          "status": 8,
          "itemId": "M1_D4_Veggie_Grilled_Sandwich_Without_Cheese",
          "addedTime": 0,
          "itemPrice": 30,
          "orderId": "2d9n7IGi5vX4ZfNlJ0gn",
          "customNote": "",
          "startTime": 0,
          "entryDate": {
              "nanoseconds": 129000000,
              "seconds": 1635044895
          },
          "itemQuantity": 1,
          "readyTime": 0,
          "itemName": "Veggie Sandwich",
          "customizations": [],
          "date": {
              "seconds": 1634924875,
              "nanoseconds": 804000000
          },
          "acceptedDelay": 0,
          "id": "9OnbBop3svv1KHfFIEpn",
          "tokenCode": "AA-44-01"
      }
  },
  "tableNo": "",
  "tip": 0,
  "orderType": "DineIn"
};
Given('I am on menu page', () => {
  // cy.visit('/mIndex/M1_Prakash_Parvat_Test?language=en');

  cy.contains('Prakash', { timeout: 30000 });
  cy.url().should('include', '/M1_Prakash_Parvat_Test');
});
Before({ tags: "@callBefore" },async () => {
  cy.task("initFirebase", settings.firebase_service_account);
  cy.task("getMerchantDetails", { id: merchant.Id, merchantId: "M1_Prakash_Parvat_Test" });
  cy.task("setOrder", { id: orderDetails.id, order: orderDetails, merchantId: "M1_Prakash_Parvat_Test" });
  cy.wait(2000);
  cy.visit(`http://localhost:58676/go/M1_Prakash_Parvat_Test/merchant-information?language=en&order_id=${merchant.id}&user_id=${merchant.userId}`);
  cy.visit(`http://localhost:58676/go/M1_Prakash_Parvat_Test/history?language=en&user_id=${orderDetails.userId}`);
});

Given('I open guest order menu page', () => {
    cy.visit('/mIndex/M1_Prakash_Parvat_Test?language=en');
    cy.contains('Prakash', { timeout: 30000 });
    cy.url().should('include', 'http://localhost:58676/M1_Prakash_Parvat_Test');
  });
  When('I see {int} products in the menu page', async (number) => {
    cy.get('[data-cy=menu-product]').should('have.length', number);
});
Then('I open a category called {string}', async (category) => {
  cy.contains(category).click();
});
When('I open a category called {string}', async (category) => {
    cy.contains(category).click();
  });
Then('I select a product called {string}', async (name) => {
    cy.contains(name).click();
    });
 
Given('I am on the customization page with title: {string}', (title) => {
        cy.wait(2000);
        cy.get('[data-cy=guest-customization-product-name]').then(el => {
          expect(el.text().trim()).to.equal(title);
        });
      });
      Given('I am on merchant details page with title: {string}', (title) => {
        cy.wait(2000);
        cy.get('[data-cy=merchant-title]').then(el => {
          expect(el.text().trim()).to.equal(title);
        });
      });
      Then('I am on merchant details page with title: {string}', (title) => {
        cy.wait(2000);
        cy.get('[data-cy=merchant-title]').then(el => {
          expect(el.text().trim()).to.equal(title);
        });
      });

      When('I verify merchant details page', async (table) => {
        const expected = table.rowsHash();
        if (expected["Open Hours"]) {
            cy.get('[data-cy=merchant-open-hours]').then(element => {
                expect(element.text().trim()).to.equal(expected['Open Hours'])
            });
        }
        if (expect["Call"]) {
            cy.get('[data-cy=merchant-call]').then(element => {
                expect(element.text().trim()).to.equal(expected['Call'])
            });
        }
        if (expected["Email"]) {
            cy.get('[data-cy=merchant-email]').then(element => {
                expect(element.text().trim()).to.equal(expected['Email'])
            });
        }
        if (expected["Address"]) {
          cy.get('[data-cy=merchant-address]').then(element => {
              expect(element.text().trim()).to.equal(expected['Address'])
          });
      }
      });
      When('I go to menu', () => {
        cy.wait(2000)
        cy.get('[data-cy=footer-menu]').click();
    });
    When('I go to menu', () => {
      cy.wait(2000)
      cy.get('[data-cy=footer-menu]').click();
  });
  When('I go to merchant details', () => {
    cy.get('[data-cy=guest-header-logo]').click()
});
Then('I go back from merchant details', () => {
  cy.get('[data-cy=guest-merchant-details-back]').click()
});

  When('I go to merchant with id: {word}', async (merchantID) => {
    cy.viewport(1400, 660);
    cy.visit(`${GUEST_URL}/go/${merchantID}`);
    firebase = new FirebaseSDK(merchantID);
    await firebase.getIDToken();
    await firebase.getCustomToken();
    await firebase.getAccessToken();
    cy.wait(5000);
    console.clear();
});

  Then('I can see one product', () => {
    cy.wait(2000);
    cy.get('[data-cy=order-cart-details]').click();
    });
  
  When('I go to footer cart', () => {
    cy.wait(2000)
    cy.get('[data-cy=footer-cart]').click();
});
    Given('I am on menu', () => {
      cy.wait(2000)
      cy.get('[data-cy=footer-menu]').click();
  });
  Given('I am on cart details page with title: {string}', (title) => {
    cy.wait(2000);
    cy.get('[data-cy=order-cart-details]').then(el => {
      expect(el.text().trim()).to.equal(title);
    });
  });
    Then('I go to header-cart', () => {
        cy.get('[data-cy=guest-header-cart]').click({ force: true })
       });
       Then('I go to header-cart', () => {
        cy.get('[data-cy=guest-header-cart]').click({ force: true })
       });
    And('I go to cart', () => {
        cy.wait(1000);
        cy.get('[data-cy=guest-header-cart]').click();
    });
    Then('I land on the login page', async () => {
      cy.url().should('include', '/login');
      cy.get('[data-cy=guest-login-option]').should('have.length', 2);
    });
    Given('I am on login page with title: {string}', (title) => {
      cy.wait(2000);
      cy.get('[data-cy=login-sign-in]').then(el => {
        expect(el.text().trim()).to.equal(title);
      });
    });
    Then('I place the order with {string} login', (login) => {
      let loginButtonIds = {
        Guest: 'guest-login-with-guest'
      };
      cy.wait(1000);
      cy.get('[data-cy=guest-header-cart]').click({ force: true });
      cy.get('[data-cy=cart-place-order]').click();
      cy.wait(3000);
      cy.get(`[data-cy=${loginButtonIds[login]}]`).click();
    });
    Then('I select {string} order type', (orderType) => {
      let orderTypeIds = {
        dinein: 'dine-in-button'
      }
      cy.get(`[data-cy=${orderTypeIds[orderType]}]`).click();
    });
    Given('I am on track order page with title: {string}', (title) => {
      cy.get('[data-cy=track-order-title]').then(el => {
        expect(el.text().trim()).to.equal(title);
      });
    });
    When('I verify in order tracking', async (table) => {
      const expected = table.rowsHash();
      if (expected["Payment Status"]) {
          cy.get('[data-cy=order-tracking-payment-status]').then(element => {
              expect(element.text().trim()).to.equal(expected['Payment Status'])
          });
      }
      if (expect["Status"]) {
          cy.get('[data-cy=order-tracking-status]').then(element => {
              expect(element.text().trim()).to.equal(expected['Status'])
          });
      }
      if (expected["Table"]) {
          cy.get('[data-cy=order-tracking-table]').then(element => {
              expect(element.text().trim()).to.equal(expected['Table'])
          });
      }
      if (expected["Number Of Suborders"]) {
          cy.get('[data-cy=order-tracking-product]').should('have.length', expected['Number Of Suborders']);
      }
    });
    And('I can see the empty cart', () => {
        cy.wait(2000);
        cy.get('[data-cy=guest-order-empty-cart]').click(); 
        });
        Given('I can see the empty cart', () => {
          cy.wait(2000);
          cy.get('[data-cy=guest-order-empty-cart]').click(); 
          });
        When('I place the order', () => {
          cy.wait(2000);
          cy.get('[data-cy=cart-place-order]').click({ force: true });
        });
        Then('I add the product to my cart', () => {
          cy.wait(5000);
          cy.get('[data-cy=customization-add-to-cart]').click();
        });
        When('I verify first Products in cart customization', (table) => {
          const data = table.rowsHash();
          cy.wait(2000);
          cy.get('[data-cy=guest-customization-product-name]').eq(0).then(element => {
              expect(element.text().trim()).to.equal(data['Name']);
          });
          cy.get('[data-cy=guest-customization-price]').eq(0).then(element => {
              expect(element.text().trim()).to.equal(data['Price']);
          });
          // cy.get('[data-cy=cart-items-customization]').eq(0).click();
      
      });
      Then('I change hindi language to english language', () => {
        cy.get('[data-cy=guest-select-english-language]').click({multiple:true})
    });

      When('I go to language', () => {
        cy.get('[data-cy=guest-header-language]').click()
    });
    And('I come to menu page where I can see different category', () => {
      cy.get('[data-cy=guest-header-language]').click()
  });
    Then('I select hindi language', () => {
      cy.get('[data-cy=guest-select-hindi-language]').click({multiple:true})
  });
  Then('I cancel the language', () => {
      cy.wait(2000)
      cy.get('[data-cy=Guest-header-language-cancel]').click()
  });
  And('I save the language', () => {
    cy.wait(2000)
    cy.get('[data-cy=Guest-header-language-save]').click()
});
When('I go to phone', () => {
  cy.get('[data-cy=guest-header-phone]').click()
});