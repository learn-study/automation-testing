

 Feature:Product page



Scenario:Product Page
    When Admin: I go to merchant with id: M1_Prakash_Parvat and email: riyapolade@gmail.com
    Then Admin:I go to header
    Then Admin:I open products page
    Then Admin:I click on Add
    Then Admin:I Added image: 'product-test-image.jpg'
    Then Admin:I added name: 'Pizza'
    Then Admin:I added namehindi: 'पिज़्ज़ा '
    Then Admin:I added namemarathi: 'पिझ्झा'
    Then Admin:I added namegujarati: 'પિઝા'
    Then Admin:I added Price: '₹100.00'
    Then Admin:I added Rating: '5'
    Then Admin:I added Prepare Time: '10 min'
    Then Admin:I added Offers: '5%'
    Then Admin:I select Item Type: "Lunch"
    Then Admin:I added Description: "Pizza is tasty"
    Then Admin:I added Description in Hindi: "पिज़्ज़ा स्वादिस्ट है"
    Then Admin:I added Description in Marathi: "पिझ्झा चवदार आहे"
    Then Admin:I added Description in Gujarati: "પિઝા ટેસ્ટિ છે"
    Then Admin:I select Veg
    Then Admin:I select NonVeg
    Then Admin:I select Veg
    Then Admin:I checked status
    Then Admin:I checked status
    Then Admin:I save product

    Then Admin: I open the first product
    Then Admin:I click on add button
    Then Admin:I Enter Item Name: "Cheese"
    Then Admin:I select customization type Add: "Add"
     Then Admin:I select customization type Add: "Remove"
      Then Admin:I select customization type Add: "Choose One" 
      Then Admin:I Click on add of customization dialog box
   Then Admin:I Enter Item Name In Choose One: "cheese"
   Then Admin:I click on delete Item Name
    Then Admin:I click on Addbutton
    Then Admin:I close the first product
    Then Admin:I edit product
    Then Admin:I save product  
  
     
    Then Admin:I verify the product in products page
      | Name         | Pizza          |
      | Price        |₹100.00         |
      | Rating       | 5              |
      | Prepare Time | 10 min         |
      | Offers       | 5%             |
      | Status       | Active         |
      | Description  |                |
      |Item Type   | Lunch          |   
    Then Admin:I close the dialog box of Description
    Then Admin:I search product: "Veggie Bowl"
    Then Admin:I clear search box of product page

    


 Scenario:  Add Products To Cart
   Then I clear the cache of my application
  When Guest: I go to merchant with id: M1_Prakash_Parvat 
 Then Guest:Menu: I select a category: "Lunch"
  Then Guest:Menu: I select a product: "Pizza"
  Then Guest: I verify the product
  | Name | Pizza |
   | Price | ₹95.00 |
   | Veg |    |
   | Prepare Time | 10   |
   |Rating | 5 |
   | Description |   Description     |
 

 
   Scenario:Save the product in fast order screen
   When Admin: I go to merchant with id: M1_Prakash_Parvat and email: riyapolade@gmail.com
     Then Admin:I go to header
     Then Admin:I open orders page
     Then Admin:I click on new order
     Then Admin:I search product in fast order screen: "Pizza"
    Then Admin:I click on product in fast order screen
    Then Admin:I click on customization: "cheese"
    Then Admin:I write notes in customization: "Pizza is tasty"
    Then Admin:I delete the product in fast order screen
 
  
