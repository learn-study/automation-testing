
import { Given, Before, Then, And, When } from "cypress-cucumber-preprocessor/steps";
import * as merchant from '../../fixtures/M1_Prakash_Parvat_merchant.json'
import * as settings from "../../fixtures/settings.json";
import * as _ from 'lodash';
let userId = ""
Given('I open guest order menu page', () => {
  cy.task("initFirebase", settings.firebase_service_account);
  cy.visit('http://localhost:4200/go/M1_Prakash_Parvat_Test?language');
  cy.contains('Prakash', { timeout: 30000 });
  cy.url().should('include', '/M1_Prakash_Parvat_Test');
  let url = cy.url().toString();
  userId = url.substr(url.indexOf('user_id=') + 8, 28);
});
Given('I am on menu page', () => {
  // cy.visit('/mIndex/M1_Prakash_Parvat_Test?language=en');

  cy.contains('Prakash', { timeout: 30000 });
  cy.url().should('include', '/M1_Prakash_Parvat_Test');
});
let orderDetails = {
  "documentId": "2d9n7IGi5vX4ZfNlJ0gp",
  "entryDate": {
      "seconds": 1634924867,
      "nanoseconds": 831000000
  },
  "tokenCode": "AA-44",
  "merchantId": "M1_Prakash_Parvat_Test",
  "adjustedType": "Less",
  "userId": "jlHUP4pHUsNjJUWjBoypxmeFL0b9",
  "adjustedAmt": 0,
  "id": "2d9n7IGi5vX4ZfNlJ0gp",
  "totalQuantity": 1,
  "versionId": 20,
  "delivery": 0,
  "status": 8,
  "date": {
      "seconds": 1635307755,
      "nanoseconds": 912000000
  },
  "totalPrice": 30,
  "orderBy": 1,
  "paymentStatus": 0,
  "subOrders": {
      "9OnbBop3svv1KHfFIEpn": {
          "status": 8,
          "itemId": "M1_D4_Veggie_Grilled_Sandwich_Without_Cheese",
          "addedTime": 0,
          "itemPrice": 30,
          "orderId": "2d9n7IGi5vX4ZfNlJ0gn",
          "customNote": "",
          "startTime": 0,
          "entryDate": {
              "nanoseconds": 129000000,
              "seconds": 1635044895
          },
          "itemQuantity": 1,
          "readyTime": 0,
          "itemName": "Veggie Sandwich",
          "customizations": [],
          "date": {
              "seconds": 1634924875,
              "nanoseconds": 804000000
          },
          "acceptedDelay": 0,
          "id": "9OnbBop3svv1KHfFIEpn",
          "tokenCode": "AA-44-01"
      }
  },
  "tableNo": "",
  "tip": 0,
  "orderType": "DineIn"
};
let merchantDetails = {
  "documentId": "2d9n7IGi5vX4ZfNlJ0gp",
  "entryDate": {
      "seconds": 1634924867,
      "nanoseconds": 831000000
  },
  "tokenCode": "AA-44",
  "merchantId": "M1_Prakash_Parvat_Test",
  "adjustedType": "Less",
  "userId": "jlHUP4pHUsNjJUWjBoypxmeFL0b9",
  "adjustedAmt": 0,
  "id": "2d9n7IGi5vX4ZfNlJ0gp",
  "totalQuantity": 1,
  "versionId": 20,
  "delivery": 0,
  "status": 8,
  "date": {
      "seconds": 1635307755,
      "nanoseconds": 912000000
  },
  "totalPrice": 30,
  "orderBy": 1,
  "paymentStatus": 0,
  "subOrders": {
      "9OnbBop3svv1KHfFIEpn": {
          "status": 8,
          "itemId": "M1_D4_Veggie_Grilled_Sandwich_Without_Cheese",
          "addedTime": 0,
          "itemPrice": 30,
          "orderId": "2d9n7IGi5vX4ZfNlJ0gn",
          "customNote": "",
          "startTime": 0,
          "entryDate": {
              "nanoseconds": 129000000,
              "seconds": 1635044895
          },
          "itemQuantity": 1,
          "readyTime": 0,
          "itemName": "Veggie Sandwich",
          "customizations": [],
          "date": {
              "seconds": 1634924875,
              "nanoseconds": 804000000
          },
          "acceptedDelay": 0,
          "id": "9OnbBop3svv1KHfFIEpn",
          "tokenCode": "AA-44-01"
      }
  },
  "tableNo": "",
  "tip": 0,
  "orderType": "DineIn"
};
Before({ tags: "@callBefore" },async () => {
  cy.task("initFirebase", settings.firebase_service_account);
  cy.task("setOrder", { id: orderDetails.id, order: orderDetails, merchantId: "M1_Prakash_Parvat_Test" });
  // cy.wait(5000);
  // cy.task("updateMerchant", { id: merchantDetails.id,merchant: merchantDetails, merchantId: "M1_Prakash_Parvat_Test" });
  cy.task("getMerchantDetails", { id: merchant.Id, merchantId: "M1_Prakash_Parvat_Test" });
  cy.wait(2000);
  cy.visit(`http://localhost:4200/go/M1_Prakash_Parvat_Test/orderType?&order_id=${orderDetails.id}&user_id=${orderDetails.userId}`);
  cy.visit(`http://localhost:4200/go/M1_Prakash_Parvat_Test/login?order_id=${merchantDetails.id}&user_id=${merchantDetails.userId}`);
});
When('I add {int} quantity of item at {int}', () => {
    cy.get('[data-cy=item-quantity-add]').click();
  cy.wait(5000);
});

Then('I decrease {int} quantity of item at {int}', () => {
  // for (let i = 0; i < qty; i++) {
    cy.get('[data-cy=item-quantity-remove]').click();
  // }
});
Given('I open guest order menu page', () => {
    cy.visit('/mIndex/M1_Prakash_Parvat_Test?language=en');
    cy.contains('Prakash', { timeout: 30000 });
    cy.url().should('include', '/M1_Prakash_Parvat_Test');
  });
  When('I place the order', () => {
    cy.get('[data-cy=cart-place-order]').click({ force: true });
  });
  Then('I again place the order', () => {
    cy.get('[data-cy=cart-place-order]').click({ force: true });
  });
  And('I click on modify order', () => {
    cy.get('[data-cy=cart-place-order]').click({ force: true });
  });
  And('I place the order', () => {
    cy.get('[data-cy=cart-place-order]').click({ force: true });
  });
  When('I see {int} products in the menu page', async (number) => {
    cy.get('[data-cy=menu-product]').should('have.length', number);
});
When('I open a category called {string}', async (category) => {
    cy.contains(category).click();
  });
  // Given('I am on menu page', () => {
  //   cy.visit('/mIndex/M1_Prakash_Parvat_Test?language=en');
  //   cy.contains('Prakash', { timeout: 30000 });
  //   cy.url().should('include', '/M1_Prakash_Parvat_Test');
  // });
  When('I open a category called {string}', async (category) => {
    cy.contains(category).click();
  });
  
  Then('I see {int} products in the menu page', async (number) => {
    cy.get('[data-cy=menu-product]').should('have.length', number);
  });
  When('I select a product called {string}', async (name) => {
    cy.contains(name).click();
    });
    Then('I select a product called {string}', async (name) => {
      cy.contains(name).click();
      });
    Then('I select a product called {string}', async (name) => {
      cy.contains(name).click();
      });
      Then('I select a product called {string}', async (name) => {
        cy.contains(name).click();
        });
      When('I add the product to my cart', () => {
        cy.wait(5000);
        cy.get('[data-cy=customization-add-to-cart]').click();
      });
      Then('I go to cart page by clicking on cart', () => {
        cy.get('[data-cy=guest-header-cart]').click();
      });
    Given('I am on the customization page with title: {string}', (title) => {
        cy.wait(2000);
        cy.get('[data-cy=guest-customization-product-name]').then(el => {
          expect(el.text().trim()).to.equal(title);
        });
      });
      Then('I am on cart details page with title: {string}', (title) => {
        cy.wait(2000);
        cy.get('[data-cy=order-cart-details]').then(el => {
          expect(el.text().trim()).to.equal(title);
        });
      });
      Given('I am on cart details page with title: {string}', (title) => {
        cy.wait(2000);
        cy.get('[data-cy=order-cart-details]').then(el => {
          expect(el.text().trim()).to.equal(title);
        });
      });
      Then('I add the product to my cart', () => {
        cy.get('[data-cy=customization-add-to-cart]').click();
        });
        Then('I go to cart page', () => {
            cy.get('[data-cy=footer-cart]').click();
            });
            When('I go back from order type page', () => {
              cy.get('[data-cy=order-type-back-button]').click();
              });
            When('I place the order with {string} login', (login) => {
                let loginButtonIds = {
                  Guest: 'guest-login-with-guest'
                };
                cy.wait(1000);
                cy.get('[data-cy=guest-header-cart]').click({ force: true });
                cy.get('[data-cy=cart-place-order]').click();
                cy.wait(3000);
                cy.get(`[data-cy=${loginButtonIds[login]}]`).click();
              });
              Then('I land on the login page', async () => {
                cy.url().should('include', '/login');
                cy.get('[data-cy=guest-login-option]').should('have.length', 2);
              });
              Given('I am on login page with title: {string}', (title) => {
                cy.wait(2000);
                cy.get('[data-cy=login-sign-in]').then(el => {
                  expect(el.text().trim()).to.equal(title);
                });
              });
              Then('I place the order with {string} login', (login) => {
                let loginButtonIds = {
                  Guest: 'guest-login-with-guest'
                };
                cy.wait(1000);
                cy.get('[data-cy=guest-header-cart]').click({ force: true });
                cy.get('[data-cy=cart-place-order]').click();
                cy.wait(3000);
                cy.get(`[data-cy=${loginButtonIds[login]}]`).click();
              });
              And('I select {string} order type', (orderType) => {
                let orderTypeIds = {
                  dinein: 'dine-in-button'
                }
                cy.get(`[data-cy=${orderTypeIds[orderType]}]`).click();
              });
            //   Then('I select {string} order type', (orderType) => {
            //     let orderTypeIds = {
            //       pickup: 'pick-up-button'
            //     }
            //     cy.get(`[data-cy=${orderTypeIds[orderType]}]`).click();
            //   });
            Then('I modify order', () => {
              cy.get('[data-cy=order-tracking-modify-order]').click();
            });
            And('verify the image with quantity', () => {
              cy.get('[data-cy=order-tracking-item-qty-badge]').click();
            });
            When('I modify order', () => {
              cy.get('[data-cy=order-tracking-modify-order]').click();
            });
            Then('I go on Order Type page', () => {
              cy.get('[data-cy=order-type-header]').click();
            });
            When('I modify order', () => {
              cy.get('[data-cy=order-tracking-modify-order]').click();
            });
            And('I click on pay', () => {
              cy.get('[data-cy=order-tracking-make-payment]').click();
            });
            And('I can see the title Notes', () => {
              cy.get('[data-cy=title-notes]').click();
            });
            When('I click on customization icon', () => {
              cy.get('[data-cy=suborder-grid-customization-icon]').click();
            });
            Then('dialog box appears with add notes to item', () => {
              cy.get('[data-cy=customization-add-notes]').click();
            });
            Then('I write notes: {string}', (Notes) => {
              cy.get('[data-cy=customization-notes]').type(Notes);
          });
            When('I click on Notes', () => {
              cy.get('[data-cy=suborder-grid-notes-icon]').click();
            });
            When('I close the notes', () => {
              cy.get('[data-cy=notes-close]').click();
            });
            Then('I cancel notes', () => {
              cy.get('[data-cy=notes-cancel]').click();
            });
            Then('I select ingredient from Add', () => {
              cy.get('[data-cy=select-add]').click({multiple:true});
            });
            When('I close the customization dialog box', () => {
              cy.get('[data-cy=custmoziation-close]').click();
            });
            Then('I am on track order page', () => {
              cy.get('[data-cy=track-order-title]').click();
            });
            Then('I add customizations in the customizations page', async (table) => {
              const customizations = table.rowsHash();
              for (let customization in customizations) {
                  cy.contains(customization).click();
                  cy.get('[data-cy=guest-customization-price]').then(element => {
                      expect(element.text().trim()).to.equal(customizations[customization]);
                  });
              };
          });
            And('I save the notes', () => {
              cy.get('[data-cy=notes-save]').click();
            });
            Then('I cancel the customized ingredient', () => {
              cy.get('[data-cy=customization-save-button]').click();
            });
            And('I save the customized ingredient', () => {
              cy.get('[data-cy=customization-save-cancel]').click();
            });
            Then('Notes dialog box disappears and I am on track order page', () => {
              cy.get('[data-cy=track-order-title]').click();
            });
            And('I can see the title Customized Ingredients', () => {
              cy.get('[data-cy=customization-ingredients-title]').click();
            });
            Then('Dialog box appears with different ingredient', () => {
              cy.get('[data-cy=customization-ingredients-title]').click();
            });
            Then('I click on pay', () => {
              cy.get('[data-cy=order-tracking-make-payment]').click();
            });
            Then('I cancel the payment', () => {
              cy.get('[data-cy=payment-details-cancel-payment]').click();
            });
            When('I click on pay', () => {
              cy.get('[data-cy=order-tracking-make-payment]').click();
            });
            When('I go back to track order', () => {
              cy.get('[data-cy=payment-back-button]').click();
            });
            Then('I verify payment details', (table) => {
              let data = table.rowsHash();
              cy.get('[data-cy=payment-details-token]').then(el => {
                expect(el.text().trim().length).to.equal(5);
              });
              cy.get('[data-cy=payment-details-grand-total]').then(el => {
                expect(el.text().trim()).to.equal(data['grandTotal']);
              });
              cy.get('[data-cy=payment-details-total-qty]').then(el => {
                expect(el.text().trim()).to.equal(data['totalQty']);
              });
              cy.get('[data-cy=payment-details-subtotal]').then(el => {
                expect(el.text().trim()).to.equal(data['subTotal']);
              });
              cy.get('[data-cy=payment-details-tip]').then(el => {
                expect(el.text().trim()).to.equal(data['tip']);
              });
              if (data['SGST']) {
                cy.get('[data-cy=payment-details-sgst]').then(el => {
                  expect(el.text().trim()).to.equal(data['SGST']);
                });
                cy.get('[data-cy=payment-details-sgst%]').then(el => {
                  expect(el.text().trim()).to.equal(`SGST ${data['sgst%']} On ${data['subTotal']}`);
                });
              }
              if (data['CGST']) {
                cy.get('[data-cy=payment-details-cgst%]').then(el => {
                  expect(el.text().trim()).to.equal(`CGST ${data['cgst%']} On ${data['subTotal']}`);
                });
                cy.get('[data-cy=payment-details-cgst]').then(el => {
                  expect(el.text().trim()).to.equal(data['CGST']);
                });
              }
              if (data['tax']) {
                cy.get('[data-cy=payment-details-tax]').then(el => {
                  expect(el.text().trim()).to.equal(data['tax']);
                });
                cy.get('[data-cy=payment-details-tax-label]').then(el => {
                  expect(el.text().trim()).to.equal(`Tax ${data['tax%']} On ${data['subTotal']}`);
                });
              }
              for (let i = 0; i < +data['noOfItems']; i++) {
                cy.get('[data-cy=payment-details-item-name]').eq(0).then(el => {
                  let item = data['items'].split(',')[i];
                  expect(el.text().trim()).to.equal(item + '.');
                });
            
                cy.get('[data-cy=payment-details-item-price]').eq(0).then(el => {
                  let itemPrice = data['itemPrice'].split(',')[i];
                  expect(el.text().trim()).to.equal(itemPrice);
                });
            
                cy.get('[data-cy=payment-details-item-qty]').eq(0).then(el => {
                  let itemQty = data['itemQty'].split(',')[i];
                  expect(el.text().trim()).to.equal(itemQty);
                });
              }
            });
            
            Then('I verify payment details for second product', (table) => {
              let data = table.rowsHash();
              cy.get('[data-cy=payment-details-token]').then(el => {
                expect(el.text().trim().length).to.equal(5);
              });
              cy.get('[data-cy=payment-details-grand-total]').then(el => {
                expect(el.text().trim()).to.equal(data['grandTotal']);
              });
              cy.get('[data-cy=payment-details-total-qty]').then(el => {
                expect(el.text().trim()).to.equal(data['totalQty']);
              });
              cy.get('[data-cy=payment-details-subtotal]').then(el => {
                expect(el.text().trim()).to.equal(data['subTotal']);
              });
              cy.get('[data-cy=payment-details-tip]').then(el => {
                expect(el.text().trim()).to.equal(data['tip']);
              });
              if (data['SGST']) {
                cy.get('[data-cy=payment-details-sgst]').then(el => {
                  expect(el.text().trim()).to.equal(data['SGST']);
                });
                cy.get('[data-cy=payment-details-sgst%]').then(el => {
                  expect(el.text().trim()).to.equal(`SGST ${data['sgst%']} On ${data['subTotal']}`);
                });
              }
              if (data['CGST']) {
                cy.get('[data-cy=payment-details-cgst%]').then(el => {
                  expect(el.text().trim()).to.equal(`CGST ${data['cgst%']} On ${data['subTotal']}`);
                });
                cy.get('[data-cy=payment-details-cgst]').then(el => {
                  expect(el.text().trim()).to.equal(data['CGST']);
                });
              }
              if (data['tax']) {
                cy.get('[data-cy=payment-details-tax]').then(el => {
                  expect(el.text().trim()).to.equal(data['tax']);
                });
                cy.get('[data-cy=payment-details-tax-label]').then(el => {
                  expect(el.text().trim()).to.equal(`Tax ${data['tax%']} On ${data['subTotal']}`);
                });
              // }
              // for (let i = 0; i < +data['noOfItems']; i++) {
              //   cy.get('[data-cy=payment-details-item-name]').eq(0).then(el => {
              //     let item = data['items'].split(',')[i];
              //     expect(el.text().trim()).to.equal(item + '.');
              //   });
            
                // cy.get('[data-cy=payment-details-item-price]').eq(0).then(el => {
                //   let itemPrice = data['itemPrice'].split(',');
                //   expect(el.text().trim()).to.equal(itemPrice);
                // });
            
                // cy.get('[data-cy=payment-details-item-qty]').eq(0).then(el => {
                //   let itemQty = data['itemQty'].split(',')[i];
                //   expect(el.text().trim()).to.equal(itemQty);
                // });
              }
            });
            When('I verify total amount to be {string} and select {string}', (total, type) => {
              let paymentOptionsIds = {
                payAtCounter: 'payment-options-pay-at-counter'
              };
              // // cy.get(`[data-cy=payment-options-grand-total]`).then(el => {
              // //   expect(el.text().trim()).to.equal(total);
              // // });
              cy.get(`[data-cy=${paymentOptionsIds['payAtCounter']}]`).click();
              cy.get('[data-cy=payment-options-make-payment]').click();
            });
            
            Given('I am on payment options page with title: {string}', (title) => {
              cy.get('[data-cy=payment-options-title]').then(el => {
                expect(el.text().trim()).to.equal(title);
              });
            });
            And('I cancel modify order', () => {
              cy.get('[data-cy=cart-cancel]').click();
            });
            And('I go to the payment options', () => {
              cy.get('[data-cy=payment-details-make-payment]').click();
            });
            When('I go to the payment options', () => {
              cy.get('[data-cy=payment-details-make-payment]').click();
            });
            // And('I verify grand total', async (table) => {
            //   const expected 
            //   cy.get('[data-cy=order-tracking-grand-total]').then(el => {
            //     expect(el.text().trim()).to.equal(expected['grandTotal']);
            //   });
            // });
            Then('I verify in order tracking', async (table) => {
              const expected = table.rowsHash();
              if (expected["Payment Status"]) {
                  cy.get('[data-cy=order-tracking-payment-status]').then(element => {
                      expect(element.text().trim()).to.equal(expected['Payment Status'])
                  });
              }
              if (expect["Status"]) {
                  cy.get('[data-cy=order-tracking-status]').then(element => {
                      expect(element.text().trim()).to.equal(expected['Status'])
                  });
              }
              if (expected["Table"]) {
                  cy.get('[data-cy=order-tracking-table]').then(element => {
                      expect(element.text().trim()).to.equal(expected['Table'])
                  });
              }
              if (expected["Number Of Suborders"]) {
                  cy.get('[data-cy=order-tracking-product]').should('have.length', expected['Number Of Suborders']);
              }
            
            });
            When('I verify in order tracking', async (table) => {
              const expected = table.rowsHash();
              if (expected["Payment Status"]) {
                  cy.get('[data-cy=order-tracking-payment-status]').then(element => {
                      expect(element.text().trim()).to.equal(expected['Payment Status'])
                  });
              }
              if (expect["Status"]) {
                  cy.get('[data-cy=order-tracking-status]').then(element => {
                      expect(element.text().trim()).to.equal(expected['Status'])
                  });
              }
              if (expected["Table"]) {
                  cy.get('[data-cy=order-tracking-table]').then(element => {
                      expect(element.text().trim()).to.equal(expected['Table'])
                  });
              }
              if (expected["Number Of Suborders"]) {
                  cy.get('[data-cy=order-tracking-product]').should('have.length', expected['Number Of Suborders']);
              }
              if (expected["grandTotal"]) {
                cy.get('[data-cy=order-tracking-grand-total]').then(el => {
                  expect(el.text().trim()).to.equal(expected['grandTotal'])
                });
              }
            });
            When('I verify in order tracking', async (table) => {
              const expected = table.rowsHash();
              if (expected["Payment Status"]) {
                  cy.get('[data-cy=order-tracking-payment-status]').then(element => {
                      expect(element.text().trim()).to.equal(expected['Payment Status'])
                  });
              }
              if (expect["Status"]) {
                  cy.get('[data-cy=order-tracking-status]').then(element => {
                      expect(element.text().trim()).to.equal(expected['Status'])
                  });
              }
              if (expected["Table"]) {
                  cy.get('[data-cy=order-tracking-table]').then(element => {
                      expect(element.text().trim()).to.equal(expected['Table'])
                  });
              }
              if (expected["Number Of Suborders"]) {
                  cy.get('[data-cy=order-tracking-product]').should('have.length', expected['Number Of Suborders']);
              }
            });
            Given('I am on track order page with title: {string}', (title) => {
              cy.get('[data-cy=track-order-title]').then(el => {
                expect(el.text().trim()).to.equal(title);
              });
            });
            And('I am on track order page with title: {string}', (title) => {
              cy.get('[data-cy=track-order-title]').then(el => {
                expect(el.text().trim()).to.equal(title);
              });
            });
            Then('I am on track order page with title: {string}', (title) => {
              cy.get('[data-cy=track-order-title]').then(el => {
                expect(el.text().trim()).to.equal(title);
              });
            });
            // Given('I am on order type page with title : {string}', (title) => {
            //   cy.get('[data-cy=order-type-header]').then(el => {
            //     expect(el.text().trim()).to.equal(title);
            //   });
            // });
            Given('I add item from track order', () => {
              cy.get('[data-cy=order-tracking-add-item]').click();
            });
            When('I open a category called {string}', async (category) => {
              cy.contains(category).click();
          });
          When('I open a category called {string}', async (category) => {
            cy.contains(category).click();
        });
        Then('I can see the Live Update title', () => {
            cy.get('[data-cy=live-update-title]').click();
          });
          When('I am on Make payment page', () => {
            cy.get('[data-cy=payment-details-title]').click();
          });
          Then('I am on Make payment page', () => {
            cy.get('[data-cy=payment-details-title]').click();
          });
          Then('I go back on track order page', () => {
            cy.get('[data-cy=back-button-payment]').click();
          });
          Given('I am on order type page with title', () => {
            cy.get('[data-cy=order-type-header]').click();
          });
          And('I verify order in database', () => {
            cy.wait(4000);
            cy.url().then(url => {
              let startI = url.indexOf("order_id=") + 9;
              let endI = url.indexOf("&");
              let id = `${url.substring(startI, endI)}`; 
              // console.log('.............................',startI, endI, id);
              cy.task("getOrderById", { merchantId: "M1_Prakash_Parvat_Test", orderId: id, url: typeof url }).then(order => {
                expect(order.status).to.equal(2);
                expect(order.paymentStatus).to.equal(7);
                expect(order.table).to.equal();
                expect(order.numberofsuborders).to.equal();
                expect(order.grandTotal).to.equal();
                expect(order.noOfItems).to.equal();
                expect(order.totalQuantity).to.equal(3);
                expect(order.subTotal).to.equal();
                expect(order.tip).to.equal(0);
              })
            })
          });
          