import { Before, Given, When, Then, And } from "cypress-cucumber-preprocessor/steps";

const { FirebaseSDK } = require("../../services/cypress-firebase-sdk");
import * as settings from "../../fixtures/settings.json";

let firebase;

Before({ tags: "@callBefore" }, async () => {
    cy.task('initFirebase', settings.firebase_service_account);
    cy.task('deleteOrdersCollection', "M1_Prakash_Parvat_Test");
    firebase = new FirebaseSDK("M1_Prakash_Parvat_Test");
    await firebase.getIDToken();
    await firebase.getCustomToken('som.shrivastava@gmail.com', "localhost");
    await firebase.getAccessToken();
    cy.visit(`http://localhost:4200/login?bearer=${firebase.customToken}&email=som.shrivastava@gmail.com&profileURL=https://brandyourself.com/blog/wp-content/uploads/linkedin-profile-picture-tips.png&profileName=Server`)
    window.sessionStorage.setItem(btoa('ADMIN_PROFILE_EMAIL'), "som.shrivastava@gmail.com");
    cy.wait(5000);
    cy.visit(`http://localhost:4200/mIndex/M1_Prakash_Parvat_Test/orders/grid?language=en`);
});

Given('I am on orders grid page', () => {
    cy.url().should('include', 'orders/grid');
});

When('I verify counters and order rows', (table) => {
    const data = table.rowsHash();
    cy.get('[data-cy=counters-incart-label]').then(el => {
        expect(el.text().trim()).to.equal(data['inCartLabel']);
    });
    cy.get('[data-cy=counters-incart-orders]').then(el => {
        expect(el.text().trim()).to.equal(data['inCartOrdersCount']);
    });
    cy.get('[data-cy=counters-incart-suborders]').then(el => {
        expect(el.text().trim()).to.equal(data['inCartSubordersCount']);
    });

    cy.get('[data-cy=counters-in-approval-label]').then(el => {
        expect(el.text().trim()).to.equal(data['inApprovalLabel']);
    });
    cy.get('[data-cy=counters-in-approval-orders]').then(el => {
        expect(el.text().trim()).to.equal(data['inCartSubordersCount']);
    });
    cy.get('[data-cy=counters-in-approval-suborders]').then(el => {
        expect(el.text().trim()).to.equal(data['inCartSubordersCount']);
    });

    cy.get('[data-cy=counters-open-label]').then(el => {
        expect(el.text().trim()).to.equal(data['openLabel']);
    });
    cy.get('[data-cy=counters-open-orders]').then(el => {
        expect(el.text().trim()).to.equal(data['openOrdersCount']);
    });
    cy.get('[data-cy=counters-open-suborders]').then(el => {
        expect(el.text().trim()).to.equal(data['openSubordersCount']);
    });

    cy.get('[data-cy=counters-in-progress-label]').then(el => {
        expect(el.text().trim()).to.equal(data['inProgressLabel']);
    });
    cy.get('[data-cy=counters-in-progress-orders]').then(el => {
        expect(el.text().trim()).to.equal(data['inProgressOrdersCount']);
    });
    cy.get('[data-cy=counters-in-progress-suborders]').then(el => {
        expect(el.text().trim()).to.equal(data['inProgressSubordersCount']);
    });

    cy.get('[data-cy=counters-ready-label]').then(el => {
        expect(el.text().trim()).to.equal(data['readyLabel']);
    });
    cy.get('[data-cy=counters-ready-orders]').then(el => {
        expect(el.text().trim()).to.equal(data['readyOrdersCount']);
    });
    cy.get('[data-cy=counters-ready-suborders]').then(el => {
        expect(el.text().trim()).to.equal(data['readySubordersCount']);
    });

    cy.get('[data-cy=counters-in-delivery-label]').then(el => {
        expect(el.text().trim()).to.equal(data['inDeliveryLabel']);
    });
    cy.get('[data-cy=counters-in-delivery-orders]').then(el => {
        expect(el.text().trim()).to.equal(data['inDeliveryOrdersCount']);
    });
    cy.get('[data-cy=counters-in-delivery-suborders]').then(el => {
        expect(el.text().trim()).to.equal(data['inDeliverySubordersCount']);
    });

    cy.get('[data-cy=counters-delivered-label]').then(el => {
        expect(el.text().trim()).to.equal(data['deliveredLabel']);
    });
    cy.get('[data-cy=counters-delivered-orders]').then(el => {
        expect(el.text().trim()).to.equal(data['deliveredOrdersCount']);
    });
    cy.get('[data-cy=counters-delivered-suborders]').then(el => {
        expect(el.text().trim()).to.equal(data['deliveredSubordersCount']);
    });

    cy.get('[data-cy=counters-missing-table-label]').then(el => {
        expect(el.text().trim()).to.equal(data['missingTableLabel']);
    });
    cy.get('[data-cy=counters-missing-table-orders]').then(el => {
        expect(el.text().trim()).to.equal(data['missingTableOrdersCount']);
    });

    cy.get('[data-cy=counters-pending-payment-label]').then(el => {
        expect(el.text().trim()).to.equal(data['pendingPaymentLabel']);
    });
    cy.get('[data-cy=counters-pending-payment-orders]').then(el => {
        expect(el.text().trim()).to.equal(data['pendingPaymentOrdersCount']);
    });

    cy.get('[data-cy=counters-reset-label]').then(el => {
        expect(el.text().trim()).to.equal('Reset');
    });

    cy.get('[data-cy=counters-all-label]').then(el => {
        expect(el.text().trim()).to.equal('All');
    });
});

Then('I create new order', () => {
    cy.get('[data-cy=orders-grid-new-order-button]').click();
});

Given('I am on fast order page', () => {
    cy.get('[data-cy=fast-order-token]').then(el => {
        // expect(el.text().trim()).to.equal('Token Code');
    });
    cy.wait(3000);
    cy.get('[data-cy=fast-order-token]').then(el => {
        expect(el.text().trim().length).greaterThan(0);
    });
});

When('I open {string} and select {string}', (category, item) => {
    cy.get(`[data-cy=fast-order-menu-category-label-${category.replace(' ', '-')}]`).click();
    cy.get(`[data-cy=fast-order-menu-item-name-${item.replace(' ', '-')}]`).click();
});

Then('I verify the items in cart', (table) => {
    const data = table.rowsHash();
    for (let i = 0; i < Number(data['noOfItems']); i++) {
        cy.get('[data-cy=fast-order-cart-item]').eq(i).get('[data-cy=fast-order-cart-item-name]').then(el => {
            expect(el.text().trim()).to.equal(data['items'].split(',')[i]);
        });
    }
});

Given('Cart is not empty and order type is not selected', () => {
    cy.get('[data-cy=fast-order-total-quantity-button]').then(el => {
        expect(Number(el.text().trim())).to.greaterThan(0);
    });
    cy.get('[data-cy=fast-order-order-type-button]').then(el => {
        expect(el.text().trim()).to.equal("Order Type");
    });
});

When('I place order', () => {
    cy.get('[data-cy=fast-order-place-order]').click();
});

And('I see order type dialog', () => {

});

And('I close order type dialog', () => {
    cy.get('.p-dialog-header-close').click();
});

Then('I see message {string}', (message) => {
    cy.get('[data-cy=fast-order-message]').then(el => {
        expect(el.text().trim()).to.equal(message);
    });
});

Given('Order type button has value {string}', (text) => {
    cy.get('[data-cy=fast-order-order-type-button]').then(el => {
        expect(el.text().trim()).to.equal(text);
    });
});

When('I select order type Dine out with user details', (table) => {
    cy.get('[data-cy=fast-order-order-type-button]').click();
    cy.get(`[data-cy=fast-order-dine-in-button]`).then(el => {
        expect(el.text().trim()).to.equal("Dine In");
    });
    cy.get('[data-cy=fast-order-delivery-button]').then(el => {
        expect(el.text().trim()).to.equal("Delivery");
    });
    cy.get('[data-cy=fast-order-dine-out-button]').then(el => {
        expect(el.text().trim()).to.equal("Pick Up");
    });
    cy.get('[data-cy=fast-order-dine-out-button]').click();
    const data = table.rowsHash();
    cy.get('[data-cy=fast-order-dine-out-name-input]').type(data['name']);
    cy.get('[data-cy=fast-order-dine-out-contact-input]').type(data['contact']);
    cy.get('[data-cy=fast-order-order-type-dialog-save-button]').click();
});

Then('I verify order type: Dine out and user details', (table) => {
    cy.get('[data-cy=fast-order-order-type-button]').then(el => {
        expect(el.text().trim()).to.equal('Pick Up');
    });
    const data = table.rowsHash();
    cy.get('[data-cy=fast-order-dine-out-user-name]').then(el => {
        expect(el.text().trim()).to.equal(data['name']);
    });
    cy.get('[data-cy=fast-order-dine-out-user-contact]').then(el => {
        expect(el.text().trim()).to.equal(data['contact']);
    });
});

Given('Payment Status is {string}', (status) => {
    cy.get('[data-cy=fast-order-payment-status-button]').then(el => {
        expect(el.text().trim()).to.equal(status);
    });
});

When('I select {string} payment mode', (paymentMode) => {
    let ids = {
        'Pay At Counter': 'pay-at-counter'
    }
    cy.get(`[data-cy=fast-order-${ids[paymentMode]}]`).click();
});

Given('{string} payment mode is available', (paymentMode) => {
    let ids = {
        'Pay At Counter': 'pay-at-counter'
    }
    cy.get(`[data-cy=fast-order-${ids[paymentMode]}]`).should('exist');
    cy.get(`[data-cy=fast-order-${ids[paymentMode]}]`).children().eq(2).then(el => {
        expect(el.text().trim()).to.equal(paymentMode);
    });
});

Then('I verify payment status is {string}', (status) => {
    cy.get('[data-cy=fast-order-payment-status-button]').then(el => {
        expect(el.text().trim()).to.equal(status);
    });
});

Given('Place order button has label {string}', (text) => {
    cy.get('[data-cy=fast-order-place-order]').then(el => {
        expect(el.text().trim()).to.equal(text);
    });
});

When('I place order', () => {
    cy.get('[data-cy=fast-order-place-order]').click();
});

Then('I land on orders grid page and verify order', (table) => {
    cy.wait(5000);
    const data = table.rowsHash();
    cy.get('[data-cy=orders-grid-order-row]').eq(0).get('[data-cy=orders-grid-order-status]').children().eq(0).children().eq(1).then(el => {
        expect(el.text().trim()).to.equal(data['status']);
    });
    cy.get('[data-cy=orders-grid-order-row]').eq(0).get('[data-cy=orders-grid-payment-status]').children().eq(0).children().eq(1).then(el => {
        expect(el.text().trim()).to.equal(data['paymentStatus']);
    });
    cy.get('[data-cy=orders-grid-order-row]').eq(0).get('[data-cy=orders-grid-del-qty]').then(el => {
        expect(el.text().trim()).to.equal(data['quantity']);
    });
    cy.get('[data-cy=orders-grid-order-row]').eq(0).get('[data-cy=orders-gid-grand-total-value]').then(el => {
        expect(el.text().trim()).to.equal(data['price']);
    });
    cy.get('[data-cy=orders-grid-order-row]').eq(0).get('[data-cy=orders-open-suborder]').click();
    for (let i = 0; i < Number(data['noOfSuborders']); i++) {
        cy.get('[data-cy=suborders-grid-suborder-row]').eq(i).get('[data-cy=suborder-grid-itemname]').then(el => {
            expect(el.text().trim()).to.equal(data['items'].split(',')[i]);
        });
        cy.get('[data-cy=suborders-grid-suborder-row]').eq(i).get('[data-cy=suborder-grid-quantity]').then(el => {
            expect(el.text().trim()).to.equal(data['itemQuantities'].split(',')[i]);
        });
        cy.get('[data-cy=suborders-grid-suborder-row]').eq(i).get('[data-cy=suborder-grid-price]').then(el => {
            expect(el.text().trim()).to.equal(data['itemPrices'].split(',')[i]);
        });
        cy.get('[data-cy=suborders-grid-suborder-row]').eq(i).get('[data-cy=suborder-grid-prepare-time]').then(el => {
            expect(el.text().trim()).to.equal(data['prepareTime'].split(',')[i].replace(' ', '\u00a0'));
        });
    }
});

Given('Cart item in cart is {string} with price {string} and itemPrice {string}', (item, price, itemPrice) => {
    cy.get('[data-cy=fast-order-cart-item]').eq(0).get('[data-cy=fast-order-cart-item-name]').then(el => {
        expect(el.text().trim()).to.equal(item);
    });
    cy.get('[data-cy=fast-order-cart-item]').eq(0).get('[data-cy=fast-order-cart-item-unit-price]').then(el => {
        expect(el.text().trim()).to.equal(itemPrice);
    });
    cy.get('[data-cy=fast-order-cart-item]').eq(0).get('[data-cy="fast-order-cart-item-price"]').then(el => {
        expect(el.text().trim()).to.equal(price);
    });
});

When('I select customization of type {string} with name {string}', (type, item) => {
    cy.get(`[data-cy=fast-order-customization-item-${item.replace(' ', '-')}-${type}]`).click();
});

When('I select customization of type {string} with name {string} under {string}', (type, chooseFrom, item) => {
    cy.get(`[data-cy=fast-order-customization-item-${item.replace(' ', '-')}-${chooseFrom.replace(' ', '-')}]`).click();
});

And('I verify the total price total be {string}', (totalPrice) => {
    cy.get('[data-cy=fast-order-order-total-button]').then(el => {
        expect(el.text().trim()).to.equal(totalPrice);
    });
});

When('I apply coupon {string}', (couponCode) => {
    cy.get('[data-cy=fast-order-coupon-input]').type(couponCode);
    cy.get('[data-cy=fast-order-apply-coupon-button]').click();
});

Then('I see the warning {string}', (warning) => {
    cy.get('[data-cy=fast-order-coupon-warning]').then(el => {
        let text = el.text().trim();
        expect(text).to.equal(warning);
    });
});

Then('I see coupon applied message {string}', (message) => {
    cy.get('[data-cy=fast-order-applied-coupon-message]').then(el => {
        expect(el.text().trim()).to.equal(message);
    })
});

When('I {string} cart item quantity by {int}', (type, quantity) => {
    for (let i = 0; i < quantity; i++) {
        cy.get('[data-cy=fast-order-cart-item]').eq(0).get('[data-cy=item-quantity-add]').click();
    }
});

And('I verify the total price total be {string}', (price) => {
    cy.get('[data-cy=fast-order-order-total-button]').children().eq(0).then(el => {
        expect(el.text().trim()).to.equal(price);
    })
});

Given('Total Value of cart is {string}', (price) => {
    cy.get('[data-cy=fast-order-order-total-button]').children().eq(0).then(el => {
        expect(el.text().trim()).to.equal(price);
    })
});

When('I search for {string}', (searchText) => {
    cy.get('[data-cy=fast-order-menu-search]').type(searchText);
});

Then('I see {int} items', (noOfItems, table) => {
    if (noOfItems == 0) {
        cy.get('[data-cy=fast-order-product-in-category]').should('not.exist');
    } else {
        const data = table.rowsHash();
        cy.get('[data-cy=fast-order-product-in-category]').should('have.length', 3);
        for (let i = 0; i < noOfItems; i++) {
            cy.get(`[data-cy=fast-order-menu-item-name-${data.items.split(',')[i].replace(' ', '-')}]`).then(el => {
                expect(el.text().trim()).to.equal(data['items'].split(',')[i]);
            });
        }
    }
    cy.get('[data-cy=fast-order-menu-search]').clear();
});

And('I verify the payment details', (table) => {
    const data = table.rowsHash();
    cy.get('[data-cy=fast-order-subtotal]').then(el => {
        expect(el.text().trim()).to.equal(data['subtotal']);
    });
    if (data['tax']) {
        cy.get('[data-cy=fast-order-tax]').then(el => {
            expect(el.text().trim()).to.equal(data['tax']);
        });
    }
    if (data['cgst']) {
        cy.get('[data-cy=fast-order-cgst]').then(el => {
            expect(el.text().trim()).to.equal(data['cgst']);
        });
    }
    if (data['sgst']) {
        cy.get('[data-cy=fast-order-sgst]').then(el => {
            expect(el.text().trim()).to.equal(data['sgst']);
        });
    }
    cy.get('[data-cy=fast-order-tip-spinner-value]').then(el => {
        expect(el.text().trim()).to.equal(data['tipSpinnerValue']);
    });
    cy.get('[data-cy=fast-order-tip-value]').then(el => {
        expect(el.text().trim()).to.equal(data['tip']);
    });
});