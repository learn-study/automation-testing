import { Before, Given, When, Then, And } from "cypress-cucumber-preprocessor/steps";
import { orders } from '../../fixtures/orders-data.json';

const { FirebaseSDK } = require("../../services/cypress-firebase-sdk");
import * as settings from "../../fixtures/settings.json";

let firebase;

Before({ tags: "@callBefore" }, async () => {
    cy.task('initFirebase', settings.firebase_service_account).then(async () => {
        cy.task('deleteOrdersCollection', "M1_Prakash_Parvat_Test").then(async () => {
            cy.wait(3000);
            cy.task('addOrders', { ordersData: orders, merchantId: "M1_Prakash_Parvat_Test" }).then(async () => {
                firebase = new FirebaseSDK("M1_Prakash_Parvat_Test");
                await firebase.getIDToken();
                await firebase.getCustomToken('som.shrivastava@gmail.com', "localhost");
                await firebase.getAccessToken();
                cy.visit(`http://localhost:4200/login?bearer=${firebase.customToken}&email=som.shrivastava@gmail.com&profileURL=https://brandyourself.com/blog/wp-content/uploads/linkedin-profile-picture-tips.png&profileName=Server`)
                window.sessionStorage.setItem(btoa('ADMIN_PROFILE_EMAIL'), "som.shrivastava@gmail.com");
                cy.wait(5000);
                cy.visit(`http://localhost:4200/mIndex/M1_Prakash_Parvat_Test/orders/grid?language=en`);
            });
        });
    });
});

Before(() => {
    cy.viewport(1500, 720);
});

Given('I am on orders grid page', () => {
    cy.url().should('include', 'orders/grid');
});

Then('I verify order at position {int}', (pos, table) => {
    let i = pos - 1;
    const data = table.rowsHash();

    cy.get('[data-cy=orders-grid-order-row]').eq(i).get('[data-cy=orders-grid-order-status]').eq(i).children().eq(0).children().eq(1).then(el => {
        expect(el.text().trim()).to.equal(data['status'].split(',')[i]);
    });
    cy.get('[data-cy=orders-grid-order-row]').eq(i).get('[data-cy=orders-grid-payment-status]').eq(i).children().eq(0).children().eq(1).then(el => {
        expect(el.text().trim()).to.equal(data['paymentStatus'].split(',')[i]);
    });
    cy.get('[data-cy=orders-grid-order-row]').eq(i).get('[data-cy=orders-gid-order-type]').eq(i).children().eq(0).children().eq(1).then(el => {
        expect(el.text().trim()).to.equal(data['orderType']);
    });
    cy.get('[data-cy=orders-grid-order-row]').eq(i).get('[data-cy=orders-grid-del-qty]').eq(i).then(el => {
        expect(el.text().trim()).to.equal(data['totalQuantity'].split(',')[i]);
    });
    cy.get('[data-cy=orders-grid-order-row]').eq(i).get('[data-cy=orders-grid-servers]').eq(i).then(el => {
        expect(el.text().trim()).to.equal(data['servers']);
    });
    cy.get('[data-cy=orders-grid-order-row]').eq(i).get('[data-cy=orders-grid-table-number]').eq(i).then(el => {
        expect(el.text().trim()).to.equal(data['table']);
    });
    if (data['deliveryTime'] && data['deliveryTime'].split(',')[i]) {
        cy.get('[data-cy=orders-grid-order-row]').eq(i).get('[data-cy=orders-grid-delivery-time-value]').eq(i).then(el => {
            expect(el.text().trim()).to.equal(data['deliveryTime'].split(',')[i].replace(' ', '\u00a0'));
        });
    }
    cy.get('[data-cy=orders-grid-order-row]').eq(i).get('[data-cy=orders-gid-grand-total-value]').eq(i).then(el => {
        expect(el.text().trim()).to.equal(data['totalPrice'].split(',')[i]);
    });
    if (data['token'] && data['token'].split(',')[i]) {
        cy.get('[data-cy=orders-grid-order-row]').eq(i).get('[data-cy=orders-grid-token]').eq(i).then(el => {
            expect(el.text().trim()).to.equal(data['token'].split(',')[i]);
        });
    }
    cy.get('[data-cy=orders-open-suborder]').eq(i).click();
    for (let j = 0; j < Number(data['noOfSuborders']); j++) {
        cy.get('[data-cy=suborder-grid-itemname]').eq(j).then(el => {
            expect(el.text().trim()).to.equal(data['items'].split(',')[j]);
        });
        cy.get('[data-cy=suborder-grid-quantity]').eq(j).then(el => {
            expect(el.text().trim()).to.equal(data['itemQuantities'].split(',')[j]);
        });
        cy.get('[data-cy=suborder-grid-price]').eq(j).then(el => {
            expect(el.text().trim()).to.equal(data['itemPrices'].split(',')[j]);
        });
        cy.get('[data-cy=suborder-grid-prepare-time]').eq(j).then(el => {
            expect(el.text().trim()).to.equal(data['suborderPrepareTime'].split(',')[j].replace(' ', '\u00a0'));
        });
        if (data['suborderDeliveryTime'] && data['suborderDeliveryTime'].split(',')[i]) {
            cy.get('[data-cy=suborder-grid-delivery-time]').eq(j).then(el => {
                expect(el.text().trim()).to.equal(data['suborderDeliveryTime'].split(',')[j].replace(' ', '\u00a0'));
            });
        }
        cy.get('[data-cy=suborder-grid-status]').eq(j).children().eq(0).children().eq(1).then(el => {
            expect(el.text().trim()).to.equal(data['suborderStatus'].split(',')[j]);
        });
    }
});

Given('I am on {string} {string} page', (page, type) => {
    cy.url().should('include', `${page}/${type}`);
});

And('I see {int} suborders', (noOfSuborders) => {
    cy.get('[data-cy=prepare-grid-suborder-row]').should('have.length', noOfSuborders);
});

Given('Suborder at position {int} has name {string} and status {string} with label {string}', (pos, name, suborderStatus, statusLabel) => {
    cy.get('[data-cy=prepare-items-name]').eq(pos - 1).then(el => {
        expect(el.text().trim()).to.equal(name);
    });
    cy.get(`[data-cy=prepare-grid-status-button]`).eq(pos - 1).should('exist');
    cy.get(`[data-cy=prepare-grid-status-button]`).eq(pos - 1).children().eq(1).then(el => {
        expect(el.text().trim()).to.equal(statusLabel);
    });
});

When('I change the status of suborder at positions {string} to {string}', (pos, suborderStatus) => {
    let positions = pos.trim().split(',');
    positions.forEach(el => {
        cy.wait(1000);
        cy.get(`[data-cy=prepare-grid-status-button]`).eq(Number(el) - 1).click();
    });
});

And('I change the status of suborder at positions {string} to {string}', (pos, suborderStatus) => {
    let positions = pos.trim().split(',');
    positions.forEach(el => {
        cy.wait(1000);
        cy.get(`[data-cy=prepare-grid-status-button]`).eq(Number(el) - 1).click();
    });
});

Then('I verify the suborders in prepare page', (table) => {
    const data = table.rowsHash();
    for (let i = 0; i < Number(data['noOfSuborders']); i++) {
        cy.get('[data-cy=prepare-items-name]').eq(i).then(el => {
            expect(el.text().trim()).to.equal(data['items'].split(',')[i]);
        });
        cy.get('[data-cy=prepare-items-quantity]').eq(i).then(el => {
            expect(el.text().trim()).to.equal(data['quantities'].split(',')[i]);
        });
        cy.get(`[data-cy=prepare-grid-status-button]`).eq(i).should('exist');
        cy.get(`[data-cy=prepare-grid-status-button]`).eq(i).children().eq(1).then(el => {
            expect(el.text().trim()).to.equal(data['status'].split(',')[i]);
        });
        cy.wait(1000);
        cy.get('[data-cy=prepare-delivery-time]').eq(i).then(el => {
            expect(el.text().trim()).to.equal(data['deliveryTime'].split(',')[i].replace(' ', '\u00a0'));
        });
        cy.get('[data-cy=prepare-prepare-time]').eq(i).then(el => {
            expect(el.text().trim()).to.equal(data['prepareTime'].split(',')[i].replace(' ', '\u00a0'));
        });
        if (data['tables']) {
            cy.get('[data-cy=prepare-table-number]').eq(i).then(el => {
                expect(el.text().trim()).to.equal(data['tables'].split(',')[i]);
            });
        }
        if (data['customizations'] && data['customizations'].split(',')[i]) {
            cy.get('[data-cy=prepare-grid-customizations]').eq(i).then(el => {
                expect(el.text().trim()).to.equal(data['customizations'].split(',')[i]);
            });
        }
        if (data['notes'] && data['notes'].split(',')[i]) {
            cy.get('[data-cy=prepare-grid-suborder-note]').eq(i).then(el => {
                expect(el.text().trim()).to.equal(data['notes'].split(',')[i]);
            });
        }
    }
});

//orders page filter tests
And('I go to {string} page', (page) => {
    cy.get('[data-cy=header-toggle-button]').click();
    cy.get(`[data-cy=header-${page}-page]`).click();
    cy.wait(1000);
    cy.get('[data-cy=header-toggle-button]').click();
});

Then('I go to {string} view page', (view) => {
    cy.get('[data-cy=orders-view-dropdown]').click();
    cy.contains(view).click();
});

And('I see table {string} with servers {string}', (seat, servers) => {
    cy.get(`[data-cy=table-view-seat-button-${seat}]`).should('exist');
    cy.get(`[data-cy=table-view-seat-button-${seat}]`).children().eq(0).then(el => {
        expect(el.text().trim()).to.equal(seat);
    });
    cy.get(`[data-cy=table-view-seat-button-${seat}]`).children().eq(1).then(el => {
        expect(el.text().trim()).to.equal(servers);
    })
});

When('I select seat {string}', (seat) => {
    cy.get(`[data-cy=table-view-seat-button-${seat}]`).click();
});

Given('I am on seats view page', () => {
    cy.url().should('include', 'orders/tables');
    cy.url().should('include', 'type=placeOrder');
});

Given('I am on suborders view page', () => {
    cy.url().should('include', 'orders/suborder');
});

Given('I am on {string} page {string} view', (page, view) => {
    cy.url().should('include', `${page}/${view}`);
})

And('I see suborder at position {int}', (pos, table) => {
    const data = table.rowsHash();
    const i = pos - 1;
    cy.get('[data-cy=suborder-grid-itemname]').eq(i).then(el => {
        expect(el.text().trim()).to.equal(data['item']);
    });
    cy.get('[data-cy=suborder-grid-quantity]').eq(i).then(el => {
        expect(el.text().trim()).to.equal(data['quantity']);
    });
    cy.get('[data-cy=suborder-grid-price]').eq(i).then(el => {
        expect(el.text().trim()).to.equal(data['itemPrice']);
    });
    cy.get('[data-cy=suborder-grid-prepare-time]').eq(i).then(el => {
        expect(el.text().trim()).to.equal(data['prepareTime']);
    });
    cy.get('[data-cy=suborder-grid-status]').eq(i).children().eq(0).children().eq(1).then(el => {
        expect(el.text().trim()).to.equal(data['status']);
    });
});

When('I select Suborder at position {int}', (pos) => {
    cy.get('[data-cy=suborders-grid-open-order]').eq(pos - 1).click();
});

Then('I verify orders', (table) => {
    const data = table.rowsHash();
    cy.get('[data-cy=orders-grid-order-row]').should('have.length', data['noOfOrders']);
    for (let i = 0; i < data['noOfOrders']; i++) {
        cy.get('[data-cy=orders-grid-order-row]').eq(i).get('[data-cy=orders-grid-order-status]').eq(i).children().eq(0).children().eq(1).then(el => {
            expect(el.text().trim()).to.equal(data['status'].split(',')[i]);
        });
        cy.get('[data-cy=orders-grid-order-row]').eq(i).get('[data-cy=orders-grid-payment-status]').eq(i).children().eq(0).children().eq(1).then(el => {
            expect(el.text().trim()).to.equal(data['paymentStatus'].split(',')[i]);
        });
        cy.get('[data-cy=orders-grid-order-row]').eq(i).get('[data-cy=orders-grid-del-qty]').eq(i).then(el => {
            expect(el.text().trim()).to.equal(data['orderQuantities'].split(',')[i]);
        });
        cy.get('[data-cy=orders-grid-order-row]').eq(i).get('[data-cy=orders-gid-grand-total-value]').eq(i).then(el => {
            expect(el.text().trim()).to.equal(data['grandTotals'].split(',')[i]);
        });
        if (data['token'] && data['token'].split(',')[i]) {
            cy.get('[data-cy=orders-grid-order-row]').eq(i).get('[data-cy=orders-grid-token]').eq(i).then(el => {
                expect(el.text().trim()).to.equal(data['token'].split(',')[i]);
            });
        }
    }
});

And('I verify filter label {string}', (filterLabel) => {
    cy.get('[data-cy=orders-grid-filter-by-seat-label]').then(el => {
        expect(el.text().trim()).to.equal(filterLabel);
    });
});

And('I verify token filter label {string}', (filterLabel) => {
    cy.get('[data-cy=orders-grid-filter-by-token]').then(el => {
        expect(el.text().trim()).to.equal(filterLabel);
    });
});

Given('Filter label is {string}', (filterLabel) => {
    cy.get('[data-cy=orders-grid-filter-by-seat-label]').then(el => {
        expect(el.text().trim()).to.equal(filterLabel);
    });
});

Given('Filter by token label is {string}', (filterLabel) => {
    cy.get('[data-cy=orders-grid-filter-by-token]').then(el => {
        expect(el.text().trim()).to.equal(filterLabel);
    });
});

Then('I close filter', () => {
    cy.get('[data-cy=close-filter-button]').click({ force: true });
});

When('I {string} 5 minutes to the suborder at position {int}', (type, pos) => {
    cy.get('[data-cy=suborders-grid-add-delivery-time]').eq(pos - 1).click();
    cy.get('[data-cy=orders-open-suborder]').eq(0).click();
});

When('I change the status to {string} of suborder at positions {string} in orders page', (status, pos) => {
    let positions = pos.trim().split(',');
    positions.forEach(el => {
        cy.wait(3000);
        cy.get('[data-cy=suborder-grid-status]').eq(Number(el) - 1).children().eq(0).click();
        cy.get('p-dropdownitem').contains(status).click();
    });
});

When('I add note: {string} to suborder at position {int}', (note, pos) => {
    cy.get('[data-cy=orders-open-suborder]').eq(0).click();
    cy.get('[data-cy=suborder-grid-notes-icon]').eq(pos - 1).click();
    cy.get('[data-cy=suborder-grid-notes-input]').type(note);
    cy.get('[data-cy=suborder-grid-notes-save]').children().eq(0).click();
    cy.wait(1000);
    cy.get('[data-cy=suborders-grid-customNote]').eq(pos - 1).then(el => {
        expect(el.text().trim()).to.equal(note);
    });
});

When('I select customization: {string} of type: {string} to suborder at position {int}', (customization, type, pos) => {
    cy.get('[data-cy=orders-open-suborder]').eq(0).click();
    cy.get('[data-cy=suborder-grid-customization-icon]').eq(0).click();
    cy.get(`[data-cy=suborders-grid-customization-${customization}-${type}`).click();
    cy.get('[data-cy=suborders-grid-customization-save-button]').children().eq(0).click();
    cy.get('[data-cy=suborder-grid-customizations').eq(0).then(el => {
        expect(el.text().trim()).to.equal(customization + ' (+)');
    });
});

When('I {string} quantity {int} to suborder with quantity {int} at position {int}', (type, changeQty, suborderQty, pos) => {
    cy.get('[data-cy=orders-open-suborder]').eq(0).click();
    cy.get('[data-cy=suborder-grid-quantity]').eq(pos - 1).click();
    cy.get('[data-cy=item-quantity-suborder-quantity]').then(el => {
        expect(el.text().trim()).to.equal(`${suborderQty}`);
    });
    cy.get('[data-cy=item-quantity-remove]').should('exist');
    cy.get('[data-cy=item-quantity-add]').should('exist');
    for (let i = 0; i < changeQty; i++) {
        if (type == 'add') {
            cy.get('[data-cy=item-quantity-add]').click();
        } else {
            cy.get('[data-cy=item-quantity-remove]').click();
        }
    }
    cy.get('[data-cy=suborders-grid-quantity-save-button]').click({ force: true });
    cy.get('[data-cy=orders-open-suborder]').eq(0).click({ force: true });
});

And('I close orders row', () => {
    cy.get('[data-cy=orders-open-suborder]').eq(0).click();
});