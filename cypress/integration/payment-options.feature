Feature: Payment Options Page
    Scenario: Paying With Pay At Counter
        Given I am on payment options page with title: "Mode of Payment"
        When I select payment mode "payAtCounter"
        Then I verify I am back on track order page with title: "Track Order"