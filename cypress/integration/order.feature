Feature: Guest Order Manu Page

  I want to open a guest order menu page

  # @focus
  Scenario: Opening a guest order menu page
    Given I open guest order menu page
    Then I see "Guest Order" in the title
    Then I add "Veggie Sandwich" to cart
    Then I place the order with "Guest" login
    Then I select "DineIn" order type
    And I go the track order

  Scenario: Verifying payment details for the order
    Given I am on payment details page with title: "Make Payment"
    Then I verify payment details
      | grandTotal | ₹31.80          |
      | noOfItems  | 1               |
      | items      | Veggie Sandwich |
      | itemPrice  | ₹30.00          |
      | itemQty    | 1               |
      | totalQty   | 1               |
      | subTotal   | ₹30.00          |
      | SGST       |                 |
      | SGST       |                 |
      | CGST%      |                 |
      | CGST%      |                 |
      | tax%       | 6%              |
      | tax        | ₹1.80           |
      | tip        | ₹0.00           |
    And I go the payment options

  Scenario: Verifying payment options and selecting pay at counter
    Given I am on payment options page with title: "Mode of Payment"
    Then I verify total amount to be "₹31.80" and select "payAtCounter"

  Scenario: Verifying track order page
    Given I am on track order page with title: "Track Order"
    Then I verify the order in track order
      | table          | --              |
      | paymentStatus  | Pay At Counter  |
      | status         | Open            |
      | grandTotal     | ₹31.80          |
      | noOfItems      | 1               |
      | items          | Veggie Sandwich |
      | suborderStatus | Open            |
      | itemPrice      | ₹30.00          |
      | itemQty        | 1               |

  Scenario: Increase item quantity from track order page
    When I add 1 quantity of item at 1
    Then I verify the order in track order
      | table          | --              |
      | paymentStatus  | Pay At Counter  |
      | status         | Open            |
      | grandTotal     | ₹63.60          |
      | noOfItems      | 1               |
      | items          | Veggie Sandwich |
      | suborderStatus | Open            |
      | itemPrice      | ₹60.00          |
      | itemQty        | 2               |

  Scenario: Decrease item quantity from track order page
    When I decrease 1 quantity of item at 1
    Then I verify the order in track order
      | table          | --              |
      | paymentStatus  | Pay At Counter  |
      | status         | Open            |
      | grandTotal     | ₹31.80          |
      | noOfItems      | 1               |
      | items          | Veggie Sandwich |
      | suborderStatus | Open            |
      | itemPrice      | ₹30.00          |
      | itemQty        | 1               |