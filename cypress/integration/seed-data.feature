Feature: Guest Order Seed data

  I want to create the test data using the seed data which is json export of the M1_Prakash_Parvat
    
  Scenario: Crate The Test Merchant using the seed data
    Given I have seed data in json format
    When I create the Test Merchant using seed data
    Then Test Merchant get created in firestore