Feature: Payment Details Page
    Scenario: Verifying order in payment details page
        Given I am on payment details page with title: "Make Payment"
        When I add tip of 5 in the order
        Then I verify the order details in the payment details page
            | grandTotal | ₹39.50          |
            | noOfItems  | 1               |
            | items      | Veggie Sandwich |
            | itemPrice  | ₹30.00          |
            | itemQty    | 1               |
            | totalQty   | 1               |
            | subTotal   | ₹30.00          |
            | SGST       |                 |
            | SGST       |                 |
            | CGST%      |                 |
            | CGST%      |                 |
            | tax%       | 5%              |
            | tax        | ₹1.50           |
            | tip        | ₹5.00           |
        And I go to payment options page
        And I verify order in database