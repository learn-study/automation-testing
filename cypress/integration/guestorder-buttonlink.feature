Feature: Guest Order Buttonlink
#Track order
@callBefore
Scenario:Merchant details
 Given I open guest order menu page
   When I go to merchant details
   Then I am on merchant details page with title: "About Prakash Parvat Test"
Scenario:verifying merchant details
   Given I am on merchant details page with title: "About Prakash Parvat Test"
   When I verify merchant details page
    | Open Hours  | AM |
    | Call  |7477779805|
    | Email |y.customproduct@gmail.com|
    |Address|  Prakash Blvd., USA  |
   Then I go back from merchant details
    And I come to menu page where I can see different category





Scenario: Opening a guest order menu page
    Given I open guest order menu page
    When I see "Guest Order" in the title
    Then I open a category called "Starters"
Scenario:Opening the category
    Given I am on menu page
    #When I see 10 products in the menu page 
    When I open a category called "Starters"
    Then I select a product called "Veggie Sandwich"
Scenario:verifying Track order
    Given I am on the customization page with title: "Veggie Sandwich."
     When I add the product to my cart
        Then I go to header-cart
Scenario: Place order
        Given I am on cart details page with title: "Cart Details"
        When I place the order
        Then I land on the login page
Scenario:login as Guest
        Given I am on login page with title: "Sign In"
        When I place the order with "Guest" login
        Then I select "dinein" order type
Scenario:verifying track order page
      Given I am on track order page with title: "Track Order"
       When I verify in order tracking
         | Payment Status | Pending |
         | Status | In-Approval  |
         | Table | ----   |
         | Number Of Suborders | 1 |





#header cart
Scenario:header cart
   Given I am on track order page with title: "Track Order"
   When I go to menu
   Then I go to header-cart
    And I can see the empty cart
Scenario:checking cart
   Given I can see the empty cart
   When I go to menu
   Then I open a category called "Starters"
   And I select a product called "Veggie Sandwich"
Scenario:Verifying cart
   Given I am on the customization page with title: "Veggie Sandwich."
   When I verify first Products in cart customization
     | Name | Veggie Sandwich. |
       | Price | ₹30.00 |
      | Qty | 1 |
   Then I add the product to my cart


  
#footer cart
Scenario:footer cart
Given I am on menu
When I go to footer cart
Then I can see one product



#language
Scenario:selection of language
   Given I am on menu
   When I go to language
  Then I select hindi language
   And I save the language
Scenario:cancelling the language
   Given I am on menu
   When I go to language
   Then I cancel the language

#phone
#Scenario:checking phone
  #Given I am on menu
 # When I go to phone
