const { Given, When, Then, then } = require('cypress-cucumber-preprocessor/steps');

const { FirebaseSDK } = require("../../services/cypress-firebase-sdk");

const GUEST_URL = "http://localhost:4200";

const ADMIN_URL = "http://localhost:4300";

const PROFILE_URL = "https://brandyourself.com/blog/wp-content/uploads/linkedin-profile-picture-tips.png";

let firebase;

// Website Caching

Then('I clear the cache of my application', () => {
    window.indexedDB.deleteDatabase("firebaseLocalStorageDb");
    cy.wait(5000);
});

// Navigating The Guest Order 

When('Guest: I go to merchant with id: {word}', async (merchantID) => {
    cy.viewport(1400, 660);
    cy.visit(`${GUEST_URL}/go/${merchantID}`);
    firebase = new FirebaseSDK(merchantID);
    await firebase.getIDToken();
    await firebase.getCustomToken();
    await firebase.getAccessToken();
    cy.wait(5000);
    console.clear();
});

// Footer

Then('Guest: I go to menu', () => {
    cy.wait(2000)
    cy.get('[data-cy=footer-menu]').click();
});

Then('Guest: I go to order tracking', () => {
    cy.get('[data-cy=footer-order-tracking]').click();
});

// Then('Guest: I verify Order tracking page empty', () => {
    
// });

Then('Guest: I go back from order tracking page', () => {
  cy.get('[data-cy=order-tracking-back-button]').click();
});

Then('Guest: I go to order history', () => {
    cy.get('[data-cy=footer-order-history]').click();
});

Then('Guest: I go to cart', () => {
    cy.get('[data-cy=footer-cart]').click();
});

// Then('Guest: I verify cart page empty', () => {
    
// });

Then('Guest:I go back from cart page', () =>{
    cy.get('[data-cy=cart-back-menu]').click();
}); 

Then('Guest: I go to about us', () => {
    cy.get('.fa-question-circle').parent().click();
});

// Header

Then('Guest: I go to merchant details', () => {
    cy.get('[data-cy=guest-header-logo]').click()
});

Then('Guest: I go back from merchant details', () => {
    cy.get('[data-cy=guest-merchant-details-back]').click()
});

Then('Guest: I go to language', () => {
    cy.get('[data-cy=guest-header-language]').click()
});
Then('Guest:I select hindi language', () => {
    cy.get('[data-cy=guest-select-hindi-language]').click({multiple:true})
});
Then('Guest: I save the language', () => {
    cy.wait(2000)
    cy.get('[data-cy=Guest-header-language-save]').click()
});

Then('Guest: I cancel the language', () => {
    cy.get('[data-cy=Guest-header-language-cancel]').click()
});

Then('Guest: I go to chat', () => {
    cy.get('[data-cy=guest-header-chat]').click()
});

Then('Guest: I go to phone', () => {
    cy.get('[data-cy=guest-header-phone]').click()
});

// Then('Guest: I go to header-cart', () => {
//  cy.get('[data-cy=guest-header-cart]').click()
// });
Then('Guest: I go to header-cart', () => {
    cy.get('[data-cy=guest-header-cart]').click({ force: true })
   });

// Then('Guest: I verify empty cart ', () => {
//     cy.get('[data-cy=guest-order-empty-cart]');
//    });

// Menu Page

Then('Guest:Menu: I select a category: {string}', async (category) => {
    cy.contains(category).click();
});

Then('Guest: I select a category: {string}', async (category) => {
    cy.get(`[data-cy=menu-category-open-${category}]`).click();
}); 

Then('Guest:Menu: I select a product: {string}',async (name) => {
    cy.contains(name).click();
});

Then('Guest: I select a product: {string} within the category: {string}', (product, category="Dinner") => {
    if(category){
        cy.get(`[data-cy=menu-category-open-${category.replace(' ', '-')}]`).click();
    }
    cy.wait(2000);
    cy.get(`[data-cy=menu-item-name-open-${product.replace(' ', '-')}]`).click();
});

Then('Guest: I search product in menu: {string}', (search) => {
    cy.get('[data-cy=guest-menu-search-input]').type(search);
});

Then('Guest: I clear search box', () => {
    cy.get('[data-cy=guest-menu-search-input]').clear()
});

Then('Guest: I expand category', () => {
    cy.get('[data-cy=menu-category-expand]').click()
});
   
Then('Guest: I close expanded category', () => {
    cy.get('[data-cy=menu-category-close]').click()
});
// Customization Page

Then('Guest:Customization: I select the following customizations: {string}', (customizations) => {
    customizations.split(", ").forEach(customization => {
        cy.contains(customization).parent().siblings().eq(0).click();
    });
});

Then('Guest:Customization: I add a quantity of {int}', (quantity) => {
    for (let i = 1; i <= quantity - 1; i++) {
        cy.get('[data-cy=item-quantity-add]').eq(0).click();
    };
});

Then('Guest:Cart: I add a quantity in first product of {int}', (quantity) => {
    for (let i = 1; i <= quantity - 1; i++) {
        cy.get('[data-cy=item-quantity-add]').eq(0).click();
    };
});

Then('Guest:Cart: I add a quantity in second product of {int}', (quantity) => {
    for (let i = 1; i <= quantity - 1; i++) {
        cy.get('[data-cy=item-quantity-add]').eq(1).click();
    };
});

Then('Guest:Cart: I remove a quantity in first product of {int}', (quantity) => {
    for (let i = 1; i <= quantity - 1; i--) {
        cy.get('[data-cy=item-quantity-add]').eq(0).click();
    };
});

Then('Guest:Cart: I remove a quantity in second product of {int}', (quantity) => {
    for (let i = 1; i <= quantity - 1; i--) {
        cy.get('[data-cy=item-quantity-add]').eq(1).click();
    };
});

Then('Guest:Customization: I remove a quantity of {int}', (quantity) => {
    for (let i = 1; i <= quantity - 1; i++) {
        cy.get('[data-cy=item-quantity-remove]').eq(0).click();
    };
});

Then('Guest:Customization: I add the product to my cart', () => {
    cy.get('[data-cy=customization-add-to-cart]').click();
});

Then('Guest: I verify similar Products in customization', (table) => {
    const data = table.rowsHash();
    cy.wait(2000);
    cy.get('[data-cy=customizations-similar-items-name]').eq(0).then(element => {
        expect(element.text().trim()).to.equal(data['Name']);
    });
    cy.get('[data-cy=customizations-similar-items-price]').eq(0).then(element => {
        expect(element.text().trim()).to.equal(data['Price']);
    });
    cy.get('[data-cy=customizations-similar-items-customize]').eq(0).click();

});


// Cart Page

Then('Guest: I verify first Products in cart customization', (table) => {
    const data = table.rowsHash();
    cy.wait(2000);
    cy.get('[data-cy=guest-customization-name]').eq(0).then(element => {
        expect(element.text().trim()).to.equal(data['Name']);
    });
    cy.get('[data-cy=guest-customization-price]').eq(0).then(element => {
        expect(element.text().trim()).to.equal(data['Price']);
    });
    // cy.get('[data-cy=cart-items-customization]').eq(0).click();

});


Then('Guest: I verify second Products in cart', (table) => {
    const data = table.rowsHash();
    cy.wait(2000);
    cy.get('[data-cy=cart-items-name]').eq(1).then(element => {
        expect(element.text().trim()).to.equal(data['Name']);
    });
    cy.get('[data-cy=cart-items-price]').eq(1).then(element => {
        expect(element.text().trim()).to.equal(data['Price']);
    });
    cy.get('[data-cy=cart-items-customization]').eq(1).click();

});

Then('Guest: I verify Number of Products in Cart', (Number)=> {
cy.get('[data-cy=cart-products]').should('have.length', expected['Number Of Suborders']);
});

Then('Guest:Cart: I add a coupon code: {string}', (code) => {
    cy.get('[data-cy=cart-coupon-input]').type(code);
    cy.get('[data-cy=cart-coupon-add]').click();
});

Then('Guest: I go to customization link', () => {
    cy.get('[data-cy=cart-customization]').click();
})

// Then('Guest: I open the customization', () => {
//     cy.get('[data]').click()
// });

Then('Guest: I save the customization', () => {
    cy.get('[data-cy=cart-customization-save]').click();
});

Then('Guest: I close the customization', () => {
    cy.get('[data-cy=cart-customization-close]').click();
});

Then('Guest: I open the notes', () => {
    cy.get('[data-cy=cart-notes]').click()
});

Then('Guest: I add notes: {string}', (notes) => {
    // cy.get('[data-cy=cart-notes-input]').type('This is notes');
    cy.get('.p-fluid').get('[data-cy=cart-notes-input]').type(notes)
});

Then('Guest: I save the notes',() => {
    cy.get('[data-cy=cart-notes-add]').click();
});

Then('Guest: I close the notes', () => {
    cy.get('[data-cy=cart-notes-close]').click();
});

Then('Guest:Cart: I place an order', () => {
    cy.get('[data-cy=cart-place-order]').click({ force: true });
});

Then ('Guest: I go to back menu', () => {
    cy.get('[data-cy=cart-back-menu]').click();
});


// Login Page 

Then('Guest:Login: I login with {word}', (provider) => {
    cy.wait(1000);
    cy.url()
        .then(url => {
            if (url.includes("login")) {
                const providers = ["Guest", "Google", "Facebook"];
                expect(providers).to.include(provider);
                cy.contains(provider).click();
            }
        })
});

// Order Type Page

Then('Guest:Order-Type: I select an order type: {string}', (type) => {
    cy.wait(1000);
    cy.url()
        .then(url => {
            if (url.includes("orderType")) {
                const types = ["Dine In", "Pick Up", "Delivery"];
                expect(types).to.include(type);
                cy.contains(type).click();
            }
        })
});

// User Details Page

Then('Guest:User-Details: I submit my user details for dine out', (table) => {
    const dineOut = table.rowsHash();
    cy.get('[data-cy=user-details-name]').type(dineOut["Name"]);
    cy.get('[data-cy=user-details-contact]').type(dineOut["Contact"]);
    cy.contains('Submit').click();
});

// Then('Guest:User-Details: I submit my user details for delivery', (table) => {
//     const delivery = table.rowsHash();
//     cy.get('[data-cy=user-details-name]').type(delivery['Name']);
//     cy.get('[data-cy=user-details-contact]').type(delivery['Contact']);
//     cy.get('[data-cy=user-details-address]').type(delivery['Address']);
//     cy.get('[data-cy=user-details-zipcode]').click();
//     cy.contains(delivery['Zipcode']).click();
//     cy.get('[data-cy=user-details-city]').type(delivery['City']);
//     cy.contains('Submit').click();
// });

// Order Tracking Page

Then('Guest:Order-Tracking: I verify in order tracking', async (table) => {
    const expected = table.rowsHash();
    if (expected["Payment Status"]) {
        cy.get('[data-cy=order-tracking-payment-status]').then(element => {
            expect(element.text().trim()).to.equal(expected['Payment Status'])
        });
    }
    if (expect["Status"]) {
        cy.get('[data-cy=order-tracking-status]').then(element => {
            expect(element.text().trim()).to.equal(expected['Status'])
        });
    }
    if (expected["Table"]) {
        cy.get('[data-cy=order-tracking-table]').then(element => {
            expect(element.text().trim()).to.equal(expected['Table'])
        });
    }
    if (expected["Number Of Suborders"]) {
        cy.get('[data-cy=order-tracking-product]').should('have.length', expected['Number Of Suborders']);
    }
});

Then('Guest: I verify Suborders Products in order tracking', (table) => {
    const data = table.rowsHash();
    cy.wait(2000);
    cy.get('[data-cy=order-tracking-name]').eq(0).then(element => {
        expect(element.text().trim()).to.equal(data['Name']);
    });
    cy.get('[data-cy=order-tracking-price]').eq(0).then(element => {
        expect(element.text().trim()).to.equal(data['Price']);
    });
    cy.get('[data-cy=order-tracking-customization]').eq(0).then(element => {
        expect(element.text().trim()).to.equal(data['Customization']);
    });

});

 Then('Guest: I modify the order',() => {
     cy.get('[data-cy=order-tracking-modify-order]').click();
 });

Then('Guest:Order-Tracking: I add a quantity of {int}', (quantity) => {
    for (let i = 1; i <= quantity - 1; i++) {
        cy.get('[data-cy=item-quantity-add]').eq(0).click();
    };
});

Then('Guest:Order-Tracking: I remove a quantity of {int}', (quantity) => {
    for (let i = 1; i <= quantity - 1; i++) {
        cy.get('[data-cy=item-quantity-remove]').eq(0).click();
    };
});

// Order Tracking / Payment Details / Payment Options Page

function payWithPaymentGateway(gateway) {
    switch (gateway) {
        case "RazorPay":
            break;
        case "Stripe":
            break;
        case "PayTM":
            break;
        case "PayPal":
            break;
    }
}

Then('Guest:Payment I make a payment with {string}', (method) => {
    const methods = ["Pay At Counter", "Cash On Delivery", "RazorPay", "Stripe", "PayTM", "PayPal"];
    expect(methods).to.include(method);
    cy.get('[data-cy=order-tracking-make-payment]').click();
    cy.get('[data-cy=payment-details-make-payment]').click();
    if (method == "Pay At Counter" || method == "Cash On Delivery") {
        cy.get('[data-cy=payment-options-input]').eq(0).click();
        cy.get('[data-cy=payment-options-make-payment]').click();
    } else {
        cy.get('[data-cy=payment-options-input]').eq(methods.indexOf(method) - 1).click();
        cy.get('[data-cy=payment-options-make-payment]').click();
        cy.wait(5000);
        // payWithPaymentGateway(method);
        cy.get('button').eq(0).click();
    };
    cy.wait(1000);
});

// Navigating The Admin App

When('Admin: I go to merchant with id: {word} and email: {word}', async (merchantID, email) => {
    cy.viewport(1400, 660);
    firebase = new FirebaseSDK(merchantID);
    await firebase.getIDToken();
    await firebase.getCustomToken(email, "localhost");
    await firebase.getAccessToken();
    cy.visit(`${ADMIN_URL}/login?bearer=${firebase.customToken}&email=${email}&profileURL=${PROFILE_URL}&profileName=${'Server'}}`)
    cy.wait(5000);
    window.sessionStorage.setItem(btoa('ADMIN_PROFILE_EMAIL'), email);
    cy.visit(`${ADMIN_URL}/mIndex/${firebase.merchantID}/merchant-create/organization/settings?language=en`);
});

When('Admin: I go to merchant with id: {word}', async (merchantID) => {
    cy.viewport(1400, 660);
    firebase = new FirebaseSDK(merchantID);
    await firebase.getIDToken();
    await firebase.getCustomToken();
    await firebase.getAccessToken();
    cy.visit(`${ADMIN_URL}/login?bearer=${firebase.customToken}&email=${EMAIL}&profileURL=${PROFILE_URL}&profileName=${PROFILE_NAME}`)
    cy.wait(5000)
    cy.visit(`${ADMIN_URL}/mIndex/${firebase.merchantID}/merchant-create/organization/settings?language=en`);
});

Then('Admin: I go to {word} view of {word} page', (view, page) => {
    cy.wait(2000);
    cy.visit(`${ADMIN_URL}/mIndex/${firebase.merchantID}/${page}/${view}?language=en`);
    cy.wait(5000);
});

Then('Admin:Orders:Grid: I verify my order', (table) => {
    const expected = table.rowsHash();
    Object.keys(expected).forEach(key => {
        ordersGridVerifyValue(expected, key);
    });
});

Then('Admin:Orders:Grid: I verify my {string} order user details', (orderType, table) => {
    const expected = table.rowsHash();
    const orderDetails = returnOrderDetails();
    const order = orderDetails.order;
    const columns = orderDetails.columns;
    order.children().eq(columns["Order Type"].number).find(".fa-user-circle").click();
    if (orderType == 'Dine Out') {
        cy.get(".p-dialog").find("input").eq(0).should("have.value", expected["Name"]);
        cy.get(".p-dialog").find("input").eq(1).should("have.value", expected["Contact"]);
    } else {
        cy.get(".p-dialog").find("input").eq(0).should("have.value", expected["Name"]);
        cy.get(".p-dialog").find("input").eq(1).should("have.value", expected["Contact"]);
        cy.get(".p-dialog").find("input").eq(2).should("have.value", expected["Address"]);
        cy.get(".p-dialog").find("input").eq(4).should("have.value", expected["City"]);
        cy.get(".p-dialog").find("input").eq(3).should("have.value", expected["Zipcode"]);
    };
    cy.contains("Save").click();
});

function returnOrderDetails() {
    const columns = {
        "Table": {
            number: 1,
            type: "text"
        },
        "Qty/Del": {
            number: 2,
            type: "text"
        },
        "Delivery Time": {
            number: 3,
            type: "text"
        },
        "Order Type": {
            number: 4,
            type: "dropdown"
        },
        "Price": {
            number: 6,
            type: "text"
        },
        "Status": {
            number: 8,
            type: "dropdown"
        },
        "Payment": {
            number: 9,
            type: "dropdown"
        }
    };
    const order = cy.get("tr").eq(1);
    return { columns, order };
}

function ordersGridVerifyValue(expected, key) {
    const orderDetails = returnOrderDetails();
    const order = orderDetails.order;
    const columns = orderDetails.columns;
    if (columns[key].type == "text") {
        order.children().eq(columns[key].number).then(element => {
            let text = element.text().trim();
            expect(text).to.equal(expected[key]);
        });
    } else if (columns[key].type == "dropdown") {
        order.children().eq(columns[key].number).find(".p-dropdown-label");
    }
};

Then("Admin:Orders:Grid: I update order status to {string}", (status) => {
    cy.wait(4000)
    cy.get("tr").eq(1).get('[data-cy=orders-grid-order-status]').eq(0).children().eq(0).click();
    cy.contains(status).click();
});



Then('Admin:Orders:Standard: I change the status for suborder with name: {string} to {word}', (name, status) => {
    cy.get('tr').eq(1).children().eq(0).children().eq(0).click();
    cy.contains(name).parent().parent().children().eq(4).children().eq(0).click();
    cy.get('.p-dialog-content').contains("Status").parent().children().eq(1).click().contains(status).click();
    cy.contains("Save").click().click();
});

Then('Admin:Prepare:Standard: I change the status in the prepare page for suborder with name: {string} to: {string}', (name, status) => {
    cy.wait(2000);
    if (status == 'In-Progress') {
        cy.get('[data-cy=prepare-standard-in-progress-button]').eq(0).click();
    } else {
        cy.get('[data-cy=prepare-standard-ready-button]').eq(0).click();
    }
});

Then('Admin:Orders:Standard: I create a new order with order-type: {string} and select table number: {string}', (orderType, tableNumber) => {
    cy.viewport(300, 500);
    cy.get('[data-cy=orders-standard-new-order-button]').click();
    cy.wait(2000);
    cy.get('[data-cy=order-type-dine-in-button]').click();
    cy.wait(2000);
    cy.get(`[data-cy=table-view-seat-button-${tableNumber}]`).click();
});

Then('Admin:Orders:Standard: I create a new order with order-type: {string} and with user details:', (orderType, table) => {
    cy.viewport(300, 500);
    cy.get('[data-cy=orders-standard-new-order-button]').click();
    cy.wait(2000);
    cy.get(`[data-cy=order-type-${orderType.trim() == 'Delivery' ? 'delivery' : 'dine-out'}-button]`).click();
    cy.wait(2000);
    const userDetails = table.rowsHash();
    Object.keys(userDetails).forEach(key => {
        if (key.trim() === 'zipcode') {
            cy.get(`[data-cy=user-details-${key}-dropdown]`).children().eq(0).click();
            cy.contains(userDetails[key].trim()).click();
        } else {
            cy.get(`[data-cy=user-details-${key}-input]`).type(userDetails[key]);
        }
    })
    cy.get('[data-cy=user-details-submit-button]').click();
});

Then('Admin:Orders:Standard: I verify the order in orders page', (table) => {
    const data = table.rowsHash();
    cy.get('.orders-contents').contains('Status').next().then(element => {
        expect(element.text()).to.equal(data['Status']);
    });
    cy.get('.orders-contents').contains('Payment').next().then(element => {
        expect(element.text()).to.equal(data['Payment Status']);
    });
    cy.get('.orders-contents').contains('Table').next().then(element => {
        expect(element.text()).to.equal(data['Table']);
    });
    cy.get('.orders-contents').contains('Value').next().then(element => {
        expect(element.text()).to.equal(data['Total Price']);
    });
    cy.get('.orders-contents').contains('Quantity').next().then(element => {
        expect(element.text()).to.equal(data['Total Quantity']);
    });
    cy.get('.orders-contents').contains('Servers').next().then(element => {
        expect(element.text()).to.equal(data['Servers']);
    });
    cy.get('.orders-contents').contains('Total Price').next().then(element => {
        expect(element.text()).to.equal(data['Grand Total']);
    });
    cy.get('.orders-contents').contains('Tip').next().then(element => {
        expect(element.text()).to.equal(data['Tip']);
    });
});

Then('Admin:Orders:Standard: I close the order from orders page', () => {
    cy.get('.orders-contents').eq(0).children().eq(7).children().eq(0).children().eq(1).children().eq(0).click();
    cy.get('.p-dropdown-items').children().eq(7).click();
    cy.get('.orders-contents').eq(0).parent().children().eq(2).children().eq(0).children().eq(0).children().eq(1).children().eq(1).children().eq(1).click();
});


Then('Admin:Prepare:Standard: I change the status in the prepare page for suborder with name: {string}', (name) => {
    cy.wait(3000);
    cy.contains(name).eq(0).parent().parent().children().eq(1).children().eq(1).children().eq(1).children().eq(0).click();
});

// Orders Standard Page

Then('Admin: I pay for the order by: {string}', (paymentMode) => {
    cy.get('[data-cy=orders-standard-recieve-payment-button]').eq(0).click();
    cy.get('[data-cy=payment-page-payment-button]').click();
    cy.get('[data-cy=payment-options-cash-paid-button').click();
    cy.get('[data-cy=payment-options-payment-button]').click();
});

Then('Admin: I change the status for payment: {string}', () => {
    cy.wait(2000)
    cy.get("tr").eq(1).get('[data-cy=orders-grid-payment-status').eq(0).children().eq(0).click();
    cy.contains('Pay By Cash').click();
    // cy.wait(4000)
    // cy.get("tr").eq(1).get('[data-cy=orders-grid-order-status]').eq(0).children().eq(0).click();
    // cy.contains(status).click();
});
// Verify That The Order Placed Is Accurately Shown In Prepare Page
Then('Admin: I verify suborder in prepare page', (table) => {
    const data = table.rowsHash();
    cy.wait(5000);
    cy.get('[data-cy=prepare-standard-item-name]').eq(0).then(element => {
        expect(element.text().trim()).to.equal(data['Name']);
    });
    cy.get('[data-cy=prepare-standard-quantity-badge]').eq(0).children().eq(0).then(element => {
        expect(element.text().trim()).to.equal(data['Qty']);
    });
    cy.get('[data-cy=prepare-standard-table-number]').eq(0).then(element => {
        expect(element.text().trim()).to.equal(data['Table Number']);
    });
});

Then('Admin: I verfiy order after payment', () => {
    cy.wait(3000);
    cy.get('.orders-contents').contains('Status').next().then(element => {
        console.log(element)
        expect(element.text().trim()).to.equal('Delivered');
    });
    cy.get('.orders-contents').contains('Payment').next().then(element => {
        expect(element.text().trim()).to.equal('Pay At Counter');
    });
})

Then('Admin: I verfiy the suborder after payment', () => {
    cy.get('.suborder-header-text').parent().parent().contains('Status').next().then(element => {
        expect(element.text()).to.equal("Delivered");
    });
})

// Changing The Status In Prepare Page

Then('Admin: I open the order row', () => {
    cy.get('[data-cy=orders-standard-open-row-button]').eq(0).click();
})

Then('Admin: I close the order row', () => {
    cy.wait(3000);
    cy.get('.btn-vsm').eq(0).click();
})

Then('Admin:I open suborder in order', (table) => {
    cy.get('data-cy=orders-open-suborder').eq(0).click();
})

Then('Admin: I change the status in the orders page for order to: {string}', (status => {
    let orderStatus = {
        InCart: 0,
        InApproval: 1,
        Open: 2,
        InProgress: 3,
        Prepared: 4,
        InDelivery: 5,
        Delivered: 6,
        Complete: 7,
        Cancelled: 8
    };
    cy.wait(2000);
    cy.get('[data-cy=orders-grid-order-status]').children().eq(0).click();
    cy.get('.p-dropdown-item').eq(orderStatus[status]).click();
    cy.wait(2000);
    cy.get('[data-cy=suborders-standard-save-button]').click();
    cy.wait(2000);
}))

Then('Admin: I change the status of dropdown in orders page: {string}', (status => {
    let orderStatus = {
        InCart: 0,
        InApproval: 1,
        Open: 2,
        InProgress: 3,
        Prepared: 4,
        InDelivery: 5,
        Delivered: 6,
        Complete: 7,
        Cancelled: 8
    };
    cy.wait(2000);
    cy.get('[data-cy=orders-grid-order-status]').children().eq(0).click();
    cy.get('.p-dropdown-item').eq(orderStatus[status]).click();

}))
// Edit The Organization Or Operations Page Fields
Then('Admin: I edit the {word} inputs of organization or operations page', (type, table) => {
    const organization = table.rowsHash();
    Object.keys(organization).forEach(key => {
        const input = cy.contains(key).parent().children().eq(1);
        if (type == "dropdown") {
            input.click().contains(organization[key]).click();
        } else if (type == "text") {
            input.clear();
            input.type(organization[key]);
        }

    });
});

// Menu Page

// Manage Page

// Products Page

// Export Import Page

// Chat Page

// Order Flow
// TODO: Break Up Order Flow Into Respective Pages

Then('Admin: I select a product: {string} within the category: {string}', (product, category) => {
    cy.wait(2000);
    cy.get(`[data-cy=menu-expand-category-${category.replace(' ', '-')}]`).click();
    cy.wait(2000);
    cy.get(`[data-cy=menu-customization-button-${product.replace(' ', '-')}]`).click();
});

Given('Admin: I am on customizations page', () => {
    const url = cy.url();
    console.log(url, typeof url)
    url.includes('customization');
});

Then('Admin: I add item with quantity: {int}', (quantity) => {
    for (let i = 0; i < quantity - 1; i++) {
        cy.get('[data-cy=item-quantity-product-increase-button]').click();
    }
});

Then('Admin: I add the item to cart', () => {
    cy.get('[data-cy=customizations-update-cart-button]').click();
});

Then('Admin: I place the order', () => {
    cy.wait(1000);
    cy.get('[data-cy=cart-place-order-button]').click();
});

Then('Admin: I go to cart', () => {
    cy.wait(1000);
    cy.get('[data-cy=cart-button-small-devices]').click();
});

Then('Admin: I verify the order in orders page', (table) => {
    const data = table.rowsHash();
    cy.wait(5000);
    // cy.get('[data-cy=orders-standard-del-qty-value]').eq(0).then(element => {
    //     expect(element.text().trim()).to.equal(data['Qty']);
    // });
    cy.get('[data-cy=orders-standard-order-status-value]').eq(0).then(element => {
        expect(element.text().trim()).to.equal(data['Status']);
    });
    cy.get('[data-cy=orders-standard-payment-status-value]').eq(0).then(element => {
        expect(element.text().trim()).to.equal(data['Payment Status']);
    });
    cy.get('[data-cy=orders-standard-table-number-value]').eq(0).then(element => {
        expect(element.text().trim()).to.equal(data['Table']);
    });
    cy.get('[data-cy=orders-standard-grand-total-value]').eq(0).then(element => {
        expect(element.text().substring(1)).to.equal(data['Price']);
    });
    cy.get('[data-cy=orders-standard-order-type-value]').eq(0).then(element => {
        expect(element.text().trim()).to.equal(data['Order Type']);
    });
    // cy.get('[data-cy=orders-standard-delivery-time-value]').eq(0).then(element => {
    //     if(data['Delivery Time']) {
    //         expect(element.text().trim()).to.equal(data['Delivery Time']);
    //     }
    // });
});

Then('Admin: I verify delivery order in orders standard page', (table) => {
    const data = table.rowsHash();
    cy.wait(1000);
    cy.get('[data-cy=orders-standard-del-qty-value]').eq(0).then(element => {
        expect(element.text().trim()).to.equal(data['Qty']);
    });
    cy.get('[data-cy=orders-standard-order-status-value]').eq(0).then(element => {
        expect(element.text().trim()).to.equal(data['Status']);
    });
    cy.get('[data-cy=orders-standard-payment-status-value]').eq(0).then(element => {
        expect(element.text().trim()).to.equal(data['Payment Status']);
    });
    cy.get('[data-cy=orders-standard-address-value]').eq(0).then(element => {
        expect(element.text().trim()).to.equal(data['Address']);
    });
    cy.get('[data-cy=orders-standard-grand-total-value]').eq(0).then(element => {
        expect(element.text().substring(1)).to.equal(data['Price']);
    });
    // cy.get('[data-cy=orders-standard-delivery-time-value]').eq(0).then(element => {
    //     expect(element.text().trim()).to.equal(data['Delivery Time']);
    // });
});

Then('Admin: I verify the order in delivery boy screen', (table) => {
    const data = table.rowsHash();
    cy.wait(1000);
    cy.get('[data-cy=delivery-order-payment-status]').eq(0).then(element => {
        expect(element.text().trim()).to.equal(data['Payment Status']);
    });
    // cy.get('[data-cy=orders-standard-address-value]').eq(0).then(element => {
    //     expect(element.text().trim()).to.equal(data['Address']);
    // });
    cy.get('[data-cy=delivery-order-grand-total]').eq(0).then(element => {
        expect(element.text().substring(1)).to.equal(data['Price']);
    });
    // cy.get('[data-cy=delivery-order-delivery-time]').eq(0).then(element => {
    //     expect(element.text().trim()).to.equal(data['Delivery Time']);
    // });
});

Then('Admin: I verify the suborders in orders page', (table) => {
    const data = table.rowsHash();
    cy.wait(1000);
    cy.get('[data-cy=suborders-standard-name-value]').eq(0).then(element => {
        expect(element.text().trim()).to.equal(data['Name']);
    });
    cy.get('[data-cy=suborders-standard-suborder-status-dropdown]').eq(0).children().eq(0).children().eq(1).then(element => {
        expect(element.text().trim()).to.equal(data['Status']);
    });
    cy.get('[data-cy=suborders-standard-quantity-badge]').eq(0).children().eq(0).then(element => {
        expect(element.text().trim()).to.equal(data['Quantity']);
    });
});

Then('Admin - I get data from database', async () => {
    console.log(orderDetails)
});

Then('Admin: I close the order from orders page', () => {
    cy.wait(6000);
    cy.get('.orders-contents').eq(0).children().eq(7).children().eq(0).children().eq(1).children().eq(0).click();
    cy.get('.p-dropdown-items').children().eq(7).click();

    cy.get('.orders-contents').eq(0).parent().children().eq(2).children().eq(0).children().eq(0).children().eq(1).children().eq(1).children().eq(1).click();
    cy.wait(2000);
});

Then('Admin: I go to: {string}', (page) => {
    cy.get('[data-cy=header-toggle-button]').click();
    cy.wait(1000);
    cy.get(`[data-cy=header-${page}-page]`).click();
    cy.wait(3000);
})

Then('Admin: I open the navbar', () => {
    cy.get('.navbar-toggler').click();
    cy.wait(1000);
})
// Then('Admin:I go to header',() => {
//    cy.get('[data-cy=header-toggle-button]').click();
// });
Then('Admin:I go to header',() => {
  cy.wait(2000)
    cy.get('[data-cy=header-toggle-button]').click();
 });
Then('Admin:I open prepare page',() => {
    cy.get('[data-cy=header-prepare-page]').click();
 });
 Then('Admin:I open orders page',() => {
    cy.get('[data-cy=header-orders-page]').click({force:true});
 });

 Then('Admin:I open products page', () => {
     cy.get('[data-cy=header-products-page]').click();
 });

Then('Admin: I go to: {string}', (page) => {
    cy.contains(page).click();
    cy.wait(3000);
})

Then('Admin: I verify order in prepare', (table) => {
    const data = table.rowsHash();
    cy.wait(2000);
    cy.get('[data-cy=prepare-items-name]').eq(0).then(element => {
        expect(element.text().trim()).to.equal(data['Name']);
    });
    cy.get('[data-cy=prepare-items-status]').eq(0).then(element => {
        expect(element.text().trim()).to.equal(data['Status']);
    });


});

Then('Admin: I reset the screen size', () => {
    cy.wait(2000);
    cy.viewport(1000, 660);
    cy.wait(2000);
});

Then('Admin: I change the payment status to: {string}', (paymentStatus) => {
    cy.get('.orders-contents').eq(0).children().eq(8).children().eq(1).children().eq(0).click();
    cy.wait(1000);
    cy.get('.p-dropdown-items').contains(paymentStatus).click({ force: true });
    // cy.contains("Save").click();
    cy.wait(1000);
    cy.get('.dialogSaveButton').click();
    cy.wait(1000);
    cy.get('.orders-contents').eq(0).parent().children().eq(2).children().eq(0).children().eq(0).children().eq(1).children().eq(1).children().eq(1).click({ force: true });
    cy.wait(2000);
})


// Order Grid page

Then('Admin: I verify the order in orders grid page', (table) => {
    const data = table.rowsHash();
    cy.wait(2000);
    cy.get('[data-cy=orders-grid-del-qty]').eq(0).then(element => {
        expect(element.text().trim()).to.equal(data['Qty']);
    });
    // cy.get('[data-cy=orders-gid-order-type]').eq(0).then(element => {
    //     expect(element.text().trim()).to.equal(data['Order Type']);
    // });
    cy.get('[data-cy=orders-gid-grand-total-value]').eq(0).then(element => {
        expect(element.text().trim()).to.equal(data['Price']);
    });
    cy.get('[data-cy=orders-grid-order-status]').eq(0).then(element => {
        expect(element.text().trim()).to.equal(data['Status']);
    });
    cy.get('[data-cy=orders-grid-payment-status]').eq(0).then(element => {
        expect(element.text().trim()).to.equal(data['Payment Status']);
    });
});

Then('Admin: I verify first suborder in orders grid page', (table) => {
    const data = table.rowsHash();
    cy.wait(2000);
    cy.get('[data-cy=suborder-grid-itemname]').eq(0).then(element => {
        expect(element.text().trim()).to.equal(data['Name']);
    });
    cy.get('[data-cy=suborder-grid-quantity]').eq(0).then(element => {
        expect(element.text().trim()).to.equal(data['Qty']);
    });
    cy.get('[data-cy=suborder-grid-price]').eq(0).then(element => {
        expect(element.text().trim()).to.equal(data['Price']);
    });
    cy.get('[data-cy=suborder-grid-status]').eq(0).then(element => {
        expect(element.text().trim()).to.equal(data['Status']);
    });
    cy.get('[data-cy=suborder-grid-prepare-time]').eq(0).then(element => {
        expect(element.text().trim()).to.equal(data['Prepare Time']);
    });
    // cy.get('[data-cy=suborder-grid-customizations]').eq(0).then(element => {
    //     console.log(element.text())
    //     expect(element.text().trim()).to.equal(data['customizations']);
    // });
});

Then('Admin: I verify second suborder in orders grid page', (table) => {
    const data = table.rowsHash();
    cy.wait(2000);
    cy.get('[data-cy=suborder-grid-item-name]').eq(1).then(element => {
        expect(element.text().trim()).to.equal(data['Name']);
    });
    cy.get('[data-cy=suborder-grid-quantity]').eq(1).then(element => {
        expect(element.text().trim()).to.equal(data['Qty']);
    });
    cy.get('[data-cy=suborder-grid-price]').eq(1).then(element => {
        expect(element.text().trim()).to.equal(data['Price']);
    });
    cy.get('[data-cy=suborder-grid-status]').eq(1).then(element => {
        expect(element.text().trim()).to.equal(data['Status']);
    });
    cy.get('[data-cy=suborder-grid-prepare-time]').eq(1).then(element => {
        expect(element.text().trim()).to.equal(data['Prepare Time']);
    });
    // cy.get('[data-cy=suborder-grid-customizations]').eq(0).then(element => {
    //     expect(element.text().trim()).to.equal(data['customizations']);
    // });
});

Then('Admin: I open suborder of order row', () => {
    cy.get('[data-cy=orders-open-suborder]').eq(0).click();
})

Then('Admin: I open suborder quanity', () => {
    cy.get('[data-cy=suborder-grid-quantity]').eq(0).click();
})

Then('Admin: I add a quantity in first suborder of {int}', (quantity) => {
    for (let i = 1; i <= quantity - 1; i++) {
        cy.get('[data-cy=item-quantity-product-increase-button]').eq(0).click();
    };
});

Then('Admin: I add a quantity in second suborder of {int}', (quantity) => {
    for (let i = 1; i <= quantity - 1; i++) {
        cy.get('[data-cy=item-quantity-product-increase-button]').eq(1).click();
    };
});

Then('Admin: I remove a quantity in first suborder of {int}', (quantity) => {
    for (let i = 1; i <= quantity - 1; i--) {
        cy.get('[data-cy=item-quantity-product-decrease-button]').eq(0).click();
    };
});

Then('Admin: I remove a quantity in second suborder of {int}', (quantity) => {
    for (let i = 1; i <= quantity - 1; i--) {
        cy.get('[data-cy=item-quantity-product-decrease-button]').eq(1).click();
    };
});

Then('Admin:I select suborder status', () => {
    cy.get('[data-cy=suborder-grid-status]').eq(0).click();
})

Then("Admin:I change first suborder status to {string}", (status) => {
    cy.get("tr").eq(1).get('[data-cy=suborder-grid-status]').eq(0).click();
    cy.contains(status).click();
    cy.get(".p-fluid").get('[data-cy=suborder-grid-save-order]').click();
});

Then('Admin:I go to first suborder customization', () => {
    cy.get('[data-cy=suborder-grid-customization-icon]').eq(0).click();
})

Then('Admin:I select checkbox: {string}',(item) => {
     cy.get(`[data-cy=suborder-customization- ${item}]`).click();
});

Then('Admin:I select the following customizations in first suborder: {string}', (customizations) => {
    customizations.split(", ").forEach(customization => {
        cy.contains(customization).parent().siblings().eq(0).click();
    });
});

Then('Admin:Admin:I go to second suborder customization', () => {
    cy.get('[data-cy=suborder-grid-customization-icon]').eq(1).click();
})

Then('Admin:I select the following customizations in second suborder: {string}', (customizations) => {
    customizations.split(", ").forEach(customization => {
        cy.contains(customization).parent().siblings().eq(1).click();
    });
});


Then('Admin: I save the suborder customizations',() => {
    cy.get('[data-cy=suborder-grid-customization save]').click();
});


Then('Admin:I go to suborder notes', () => {
    cy.get('[data-cy=suborder-grid-notes-icon]').eq(1).click();
})

Then('Admin: I add notes in suborder: {string}', (notes) => {
    // cy.get('[data-cy=cart-notes-input]').type('This is notes');
    cy.get('.p-fluid').get('[data-cy=suborder-grid-notes-input]').clear();
    cy.get('.p-fluid').get('[data-cy=suborder-grid-notes-input]').type(notes);
});

Then('Admin: I save the notes',() => {
    cy.get('[data-cy=suborder-grid-notes-save]').click();
});

// Guest User-details-page

Then('Guest: I add name: {string}', (name) => {
    cy.get('.p-fluid').get('[data-cy=user-details-name]').type(name)
});

Then('Guest: I add phone number: {string}', (contact) => {
    cy.get('.p-fluid').get('[data-cy=user-details-contact]').type(contact)
});

Then('Guest: I add address: {string}', (address) => {
    cy.get('.p-fluid').get('[data-cy=user-details-address]').type(address)
});

// Then('Guest: I select a zipcode', () => {
//     cy.get('.p-fluid').get('[data-cy=user-details-zipcode]').eq(0).click();
// });
// Then('Guest: I select a zipcode',() => {
//     cy.get('[data-cy=user-details-zipcode]').click();
// });

Then("Guest: I select a zipcode {string}", (zipcode) => {
    // cy.get('[data-cy=orders-grid-order-status]').eq(0).click();
    cy.get('.p-fluid').get('[data-cy=user-details-zipcode]').children().eq(0).click();
    cy.contains(zipcode).click();
});

Then('Guest: I add city: {string}', (city) => {
    cy.get('.p-fluid').get('[data-cy=user-details-city]').type(city)
});

Then('Guest: I submit user details',() => {
    cy.get('[data-cy=user-details-submit]').click();
});

Then('Admin: I open the header', () => {
    cy.get('[data-cy=header-toggle-button]').click();
});

Then('Admin: I scroll down: {int}', (scrollToY) => {
    cy.scrollTo(0, scrollToY);
});

Then('Admin:Orders:Standard: I assign the order to {string}', (deliveryBoy) => {
    cy.get('[data-cy=orders-standard-delivery-boy-dropdown]').eq(0).children().eq(0).click();
    cy.contains(deliveryBoy).eq(0).click();
    cy.get('[data-cy=suborders-standard-save-button]').eq(0).click();
});

Then('Admin: I accept the delivery order', () => {
    cy.get('[data-cy=orders-standard-accept-delivery-order-button]').eq(0).click();
});

Then('Admin: I view the delivery order', () => {
    cy.get('[data-cy=orders-standard-open-delivery-order-button]').eq(0).click();
});

Then('Admin: I accept the payment by {string} from delivery screen', () => {
    cy.get('[data-cy=delivery-order-payment-button]').click();
    cy.get('[data-cy=payment-page-payment-button]').click();
    cy.get('[data-cy=payment-options-cash-paid-button]').click();
    cy.get('[data-cy=payment-options-payment-button]').click();
});

Then('Admin: I mark the delivery order as Delivered', () => {
    cy.get('[data-cy=delivery-order-delivered-buttton]').click();
});

Then('Admin: I switch to mobile view', () => {
    cy.viewport(300, 500);
});

Then('I scroll to the top', () => {
    cy.wait(2000);
    cy.scrollTo('top');
});


//Fast order and orders grid tests...

Then('Admin: I test the search bar with product {string} and search text {string} in fast order', (product, searchText) => {
    let firstProduct;
    cy.get('[data-cy=fast-order-menu-products').eq(0).get('[data-cy=fast-order-menu-product-name').then(el => {
        firstProduct = el.text().trim();
    });
    cy.get('[data-cy=fast-order-menu-search-input]').type(searchText);
    cy.get('[data-cy=fast-order-menu-products').eq(0).get('[data-cy=fast-order-menu-product-name').then(el => {
        expect(el.text().trim()).to.eq(product);
    });
    cy.get('[data-cy=fast-order-menu-search-input]').clear();
    cy.get('[data-cy=fast-order-menu-products').eq(0).get('[data-cy=fast-order-menu-product-name').then(el => {
        expect(el.text().trim()).to.eq(firstProduct);
    });
});

Then('Admin: I create new order from orders grid', () => {
    cy.get('[data-cy=orders-grid-new-order-button]').click();
});

Then('Admin: I verify the fast order screen top menu', (table) => {
    const data = table.rowsHash();
    cy.get('[data-cy=fast-order-page-top-menu-order-type]').then(el => {
        expect(el.text().trim()).to.equal(data['orderType']);
    });

    // cy.get('[data-cy=fast-order-page-top-menu-grand-total]').then(el => {
    //     expect(el.text().trim()).to.equal(data['grandTotal']);
    // });

    cy.get('[data-cy=fast-order-page-top-menu-total-quantity]').then(el => {
        expect(el.text().trim()).to.equal(data['totalQuantity']);
    });

    cy.get('[data-cy=fast-order-page-top-menu-status]').then(el => {
        expect(el.text().trim()).to.equal(data['status']);
    });

    cy.get('[data-cy=fast-order-page-top-menu-payment-status]').then(el => {
        expect(el.text().trim()).to.equal(data['paymentStatus']);
    });

    // cy.get('[data-cy=fast-order-page-top-menu-token]').then(el => {
    //     if(data['tokenCode']) {
    //         expect(el.text().trim()).to.equal(data['tokenCode']);
    //     } else {
    //         expect(el.text().trim()).to.not.equal('');
    //     }
    // });

    // cy.wait(1500);
    // if(data['tokenCode']) {
    //     cy.get('[data-cy=fast-order-page-top-menu-token]').then(el => {
    //         expect(el.text().trim()).to.not.equal(data['tokenCode']);
    //         expect(el.text().trim()).to.not.equal('');
    //     });
    // }

    if (data['table']) {
        cy.get('[data-cy=fast-order-page-top-menu-table-number]').then(el => {
            expect(el.text().trim()).to.equal(data['table']);
        });
    }
});

Then('Admin: I select category {string} and product {string} With Quantity {int} in fast order screen', (category, name, qty) => {
    cy.get(`[data-cy=fast-order-menu-category-${category.replace(' ', '-')}]`).click();
    cy.get(`[data-cy=fast-order-menu-product-${name.replace(' ', '-')}]`).click();
    for (let i = 1; i < qty; i++) {
        cy.get('[data-cy=fast-order-cart-product]').last().get('[data-cy=item-quantity-add]').click();
    }
});

Then('Admin: I select {string} customization from fast order screen', (item) => {
    cy.get('[data-cy=fast-order-cart-product-img]').last().click();
    cy.get(`[data-cy=fast-order-customization-item-${item.replace(' ', '-')}]`).click();
    cy.get('[data-cy=fast-order-customization-dialog-save-button]').click();
});

Then('Admin: I add note {string} from fast order screen', (note) => {
    cy.wait(3000);
    cy.get('[data-cy=fast-order-cart-product-img]').last().click();
    cy.get('[data-cy=fast-order-notes-edit-field').type(note);
    cy.get('[data-cy=fast-order-customization-dialog-save-button]').click();
});

Then('Admin: I verify the cart item in fast order page', (table) => {
    const data = table.rowsHash();

    cy.get('[data-cy=fast-order-cart-product-name]').last().then(el => {
        expect(el.text().trim()).to.equal(data['name']);
    });

    // cy.get('[data-cy=fast-order-cart-product-price]').last().then(el => {
    //     expect(el.text().trim()).to.equal(data['price']);
    // });

    cy.get('[data-cy=cart-item-quantity]').last().then(el => {
        expect(el.text().trim()).to.equal(data['qty']);
    });

    cy.get('[data-cy=fast-order-cart-product-customizations]').last().then(el => {
        expect(el.text().trim()).to.equal(data['customizations']);
    });

    cy.get('[data-cy=fast-order-cart-product-notes]').last().then(el => {
        expect(el.text().trim()).to.equal('Notes: ' + data['notes']);
    });
});

Then('Admin: I place order from fast order screen', () => {
    cy.get('[data-cy=fast-order-place-order-button]').click();
    cy.wait(3000);
});

Then('Admin: I select order type {string} with table number {string} from fast order page', (orderType, table) => {
    cy.get('[data-cy=fast-order-page-top-menu-order-type]').click();
    cy.get('[data-cy=fast-order-order-type-dine-in-button]').click();
    cy.get('[data-cy=fast-order-order-type-dialog]').contains(table).click();
    cy.wait(1500);
});

Then('Admin: I select order type {string} with user details from fast order page', (orderType, table) => {
    const data = table.rowsHash();
    cy.get('[data-cy=fast-order-page-top-menu-order-type]').click();
    if (orderType == 'Pick Up') {
        cy.get('[data-cy=fast-order-order-type-dine-out-button]').click();
        cy.get('[data-cy=fast-order-order-type-dialog-dine-out-name-input]').type(data['name']);
        cy.get('[data-cy=fast-order-order-type-dialog-dine-out-contact-input]').type(data['contact']);
    } else {
        cy.get('[data-cy=fast-order-order-type-delivery-button]').click();
        cy.get('[data-cy=fast-order-order-type-dialog-delivery-name-input]').type(data['name']);
        cy.get('[data-cy=fast-order-order-type-dialog-delivery-contact-input]').type(data['contact']);
        cy.get('[data-cy=fast-order-order-type-dialog-delivery-address-input]').type(data['address']);
        // cy.get('[data-cy=fast-order-order-type-dialog-delivery-zipcode-dropdown]').type(data['zipcode']);
        cy.get('[data-cy=fast-order-order-type-dialog-delivery-city-input]').type(data['city']);
    }
    cy.get('[data-cy=fast-order-order-type-dialog-save-button]').click();
});

Then('Admin: I verify the order in orders grid', (table) => {
    const data = table.rowsHash();

    cy.get('[data-cy="orders-grid-del-qty"]').eq(0).then(el => {
        expect(el.text().trim()).to.equal(data['totalQuantity']);
    });

    cy.get('[data-cy="orders-grid-token"]').eq(0).then(el => {
        expect(el.text().trim()).not.to.equal('');
    });

    cy.get('[data-cy="orders-grid-order-status"]').eq(0).children().eq(0).children().eq(1).then(el => {
        expect(el.text().trim()).to.equal(data['status']);
    });

    cy.get('[data-cy="orders-grid-payment-status"]').eq(0).children().eq(0).children().eq(1).then(el => {
        expect(el.text().trim()).to.equal(data['paymentStatus']);
    });

    cy.get('[data-cy="orders-grid-order-type"]').eq(0).children().eq(0).children().eq(1).then(el => {
        expect(el.text().trim()).to.equal(data['orderType']);
    });

    // cy.get('[data-cy="orders-grid-del-qty"]').then(el => {
    //     expect(el.text().trim()).to.equal(data['table']);
    // });

    // cy.get('[data-cy="orders-grid-del-qty"]').then(el => {
    //     expect(el.text().trim()).to.equal(data['grandTotal']);
    // });
});

Then('Admin: I open the order rows in orders grid', () => {
    cy.get('[data-cy=orders-open-suborder]').eq(0).click();
});

Then('Admin: I verify suborder {int} in orders grid', (number, table) => {
    const data = table.rowsHash();
    let index = --number;

    cy.get('[data-cy=suborder-grid-itemname]').eq(index).then(el => {
        expect(el.text().trim()).to.equal(data['name']);
    });

    cy.get('[data-cy=suborder-grid-quantity]').eq(index).then(el => {
        expect(el.text().trim()).to.equal(data['qty']);
    });

    // cy.get('[data-cy=suborder-grid-itemname]').eq(index).then(el => {
    //     expect(el.text().trim()).to.equal(data['price']);
    // });

    cy.get('[data-cy=suborder-grid-status]').eq(index).children().eq(0).children().eq(1).then(el => {
        expect(el.text().trim()).to.equal(data['status']);
    });

    cy.get('[data-cy=suborder-grid-notes]').eq(index).then(el => {
        expect(el.text().trim()).to.equal(data['notes']);
    });

    cy.get('[data-cy=suborder-grid-customizations]').eq(index).then(el => {
        expect(el.text().trim()).to.equal(data['customizations']);
    });
});

Then('Admin: I verify the total dialog fast order screen cancel button', () => {
    let subtotal;
    let tips;
    let adjustedAmt;
    let grandTotal;
    cy.get('[data-cy=fast-order-page-top-menu-grand-total]').click();
    cy.get('[data-cy=fast-order-total-dialog-sub-total]').then(el => {
        subtotal = el.text().trim();
    });

    cy.get('[data-cy=fast-order-total-dialog-tips]').then(el => {
        tips = el.text().trim();
    });

    // cy.get('[data-cy=fast-order-total-dialog-adjustedAmt]').then(el => {
    //     adjustedAmt = el.text().trim();
    // });

    cy.get('[data-cy=fast-order-total-dialog-grand-total]').then(el => {
        grandTotal = el.text().trim();
    });

    cy.get('[data-cy=fast-order-total-dialog-tips-add]').click();
    cy.get('[data-cy=fast-order-total-dialog-tips-add]').click();

    cy.get('[data-cy=fast-order-total-dialog-cancel-button]').click();

    cy.get('[data-cy=fast-order-page-top-menu-grand-total]').click();

    cy.get('[data-cy=fast-order-total-dialog-sub-total]').then(el => {
        expect(el.text().trim()).to.equal(subtotal);
    });

    cy.get('[data-cy=fast-order-total-dialog-tips]').then(el => {
        expect(el.text().trim()).to.equal(tips);
    });

    // cy.get('[data-cy=fast-order-total-dialog-adjustedAmt]').then(el => {
    //     expect(el.text().trim()).to.equal(adjustedAmt);
    // });

    cy.get('[data-cy=fast-order-total-dialog-grand-total]').then(el => {
        expect(el.text().trim()).to.equal(grandTotal);
    });

    cy.get('[data-cy=fast-order-total-dialog-cancel-button]').click();
});

Then('Admin: I change the tips and adjustment Amt to {int} and {int}', (tip, adjustedAmt) => {
    cy.get('[data-cy=fast-order-page-top-menu-grand-total]').click();
    for (let i = 0; i < tip; i++) {
        cy.get('[data-cy=fast-order-total-dialog-tips-add]').click();
    }
    cy.get('[data-cy=fast-order-total-dialog-adjustedAmt-input]').type(adjustedAmt);
    cy.get('[data-cy=fast-order-total-dialog-save-button]').click();
});


// login into the app as Manager 

When('Manager: I go to admin page with id: {word} with email: {string}', async (merchantID, email) => {
    cy.viewport(1400, 660);
    firebase = new FirebaseSDK(merchantID);
    await firebase.getIDToken();
    await firebase.getCustomToken(email);
    await firebase.getAccessToken();
    cy.visit(`${ADMIN_URL}/login?bearer=${firebase.customToken}&email=${email}&profileURL=${PROFILE_URL}&profileName=${'Server'}`)
    cy.wait(7000);
    cy.visit(`${ADMIN_URL}/mIndex/${firebase.merchantID}/merchant-create/organization/settings?language=en`);
});

//performing operation in the seat page 

//navigate to the seat page 
When('Admin: I navigate to the seats page', () => {
    cy.get('[data-cy=header-toggle-button]').click();
    cy.get('[data-cy=header-manage-page]').click();
    cy.get('[data-cy=merchant-create-seats-navigation]').click();
})

When('Admin: I navigate to the organizations page', () => {
    cy.get('[data-cy=header-toggle-button]').click();
    cy.get('[data-cy=header-manage-page]').click();
    cy.get('[data-cy=merchant-create-organizations-navigation]').click();
})
//add seat 
Then('Manager: click on the Add seat button', () => {
    cy.get("I[ptooltip='Add']").click()
})
Then('Manager: I enter seat info', (datatable) => {
    datatable.hashes().forEach(element => {
        cy.get('[data-cy= merchant_create_seats_no]').type(element.seatNo)
        cy.get('[data-cy= merchant_create_seats_name]').type(element.details)
        cy.get('[data-cy=merchant_create_seats_seats]').type(element.chairs)
    })
    cy.get('[data-cy=merchant_create_seats_spl_ability]').children().eq(0).click()
    cy.get('[data-cy=merchant_create_seats_status]').children().eq(0).click()
    cy.get('.p-multiselect-trigger').click()
    cy.get('.p-multiselect-trigger-icon').click()
    cy.get('.dialogSaveButton').click()
    cy.wait(5000)


})
//view seats
Then('Manager: I verify the row', (table) => {
    const element = table.rowsHash();
    cy.contains(element.seatNo)
    cy.contains(element.details)
    cy.contains(element.chairs)
    cy.contains(element.specialAbility)
    cy.contains(element.status)
    cy.wait(7000);
});

Then('Admin: I verify the row but cannot perform any operation', () => {
    cy.get('[data-cy=ctable-add]').should('not.exist');
    cy.get('[data-cy=ctable-edit]').should('not.exist');
    cy.get('[data-cy=ctable-delete]').should('not.exist');
});

Then('Admin: I verify that I am on {string} page', (page) => {
    cy.wait(3000);
    cy.get('[data-cy=ctable-title]').then(element => {
        expect(element.text().trim()).to.equal(page);
    });
})
//
Then('Manager: I edit the seat row', (table) => {
    const element = table.rowsHash();
    cy.get('[data-cy=ctable-edit]').eq(0).click();
    cy.get('[data-cy= merchant_create_seats_no] #input').clear().type(element.seatNo)
    cy.get('.dialogSaveButton').click()
})
//Delete the row 
Then('Manager: I delete the row', () => {
    cy.get('[data-cy=ctable-delete]').eq(0).click()
})

//navigate to transaction page
When('Manager:I navigate to the transaction page', () => {
    cy.visit(`${ADMIN_URL}/mIndex/${firebase.merchantID}/merchant-create/transactions?language=en`)
    cy.url().should('eq', `${ADMIN_URL}/mIndex/${firebase.merchantID}/merchant-create/transactions?language=en`)

})
Then('Manager:I verify transaction page', () => {
    cy.contains('Transactions').should('be.visible')
})

//navigate to payment page
When('Manager:I navigate to payment page', () => {
    cy.visit(`${ADMIN_URL}/mIndex/${firebase.merchantID}/merchant-create/paymentlinks?language=en`)
    cy.url().should('eq', `${ADMIN_URL}/mIndex/${firebase.merchantID}/merchant-create/paymentlinks?language=en`)
})
Then('Manager:I verify the payment table', () => {
    cy.contains('Payment Links').should('be.visible')
})
//navigate to the user page
When('Manager: I navigate to the user page', () => {
    cy.visit(`${ADMIN_URL}/mIndex/${firebase.merchantID}/merchant-create/users?language=en`)
    cy.url().should('eq', `${ADMIN_URL}/mIndex/${firebase.merchantID}/merchant-create/users?language=en`)
})
// Then('Manager: I click on Add button', () => {
//     cy.get("[ptooltip='Add']").click()
// })

Then('Manager: I enter user info', (table) => {
    const data = table.rowsHash();
    cy.get('[data-cy= merchant_create_user_name]').type(data.Name)
    cy.get('[data-cy= merchant_create_user_nickname]').type(data.ShortName)
    cy.get('[data-cy=merchant_create_user_contact]').type(data.Contact)
    cy.get('[data-cy=merchant_create_user_email]').type(data.Email)
    cy.get('[data-cy=merchant_create_user_roles]').children().eq(1).children().eq(0).click()
    cy.get('[data-cy=merchant_create_user_status]').children().eq(1).children().eq(0).click()
    cy.get('.dialogSaveButton').click()
    cy.wait(5000)
})

Then('Manager: I verify the user data', (table) => {
    const element = table.rowsHash();
    cy.wait(5000);
    cy.contains(element.Name).scrollIntoView().should('be.visible')
    cy.contains(element.ShortName).should('be.visible')
    cy.contains(element.Contact).should('be.visible')
    cy.contains(element.Email).should('be.visible')
    cy.contains(element.Role).should('be.visible')
    cy.contains(element.Status).should('be.visible')
    cy.wait(5000)
})
Then('Manager: I click on the edit button', () => {
    cy.get('[data-cy=ctable-edit]').eq(0).click()
})

Then('Manager : I update the user data change the name xyz to xy', (table) => {
    const element = table.rowsHash();
    cy.get('[data-cy= merchant_create_user_name] #input').clear().type(element.Name)
    cy.get('.dialogSaveButton').click()
    cy.wait(5000)
    //cy.get('.p-toast-message-text').contains('Row updated').should('be.visible')
})

Then('Manager : I delete the user row', () => {
    cy.get('[data-cy=ctable-delete]').eq(0).click({ force: true })
})


When('Manager: I navigate to the Item Type page', () => {
    cy.visit(`${ADMIN_URL}/mIndex/${firebase.merchantID}/merchant-create/itemtypes?language=en`)
    cy.url().should('eq', `${ADMIN_URL}/mIndex/${firebase.merchantID}/merchant-create/itemtypes?language=en`)
})

Then('Manager: I add the items type info', (table) => {
    const element = table.rowsHash();
    cy.get('[data-cy=merchant_create_items_name]').type(element.Name)
    cy.get('[data-cy=merchant_create_items_status]').children().eq(2).click()
    cy.get('.dialogSaveButton').click()
    cy.wait(7000)
})
Then('Manager : I verify the items type', (table) => {
    const element = table.rowsHash();
    cy.contains(element.Name)
    cy.contains(element.status)
})

Then('Manager : I edit the items type', (table) => {
    const element = table.rowsHash();
    cy.get('[data-cy=ctable-edit]').eq(3).click()
    cy.get('[data-cy=merchant_create_items_name] #input').clear().type(element.Name)
    cy.get('.dialogSaveButton').click()
    cy.wait(3000)
    cy.get('.p-toast-message-text').contains('Updated Row').should('be.visible')
})

Then('Manager: I delete the item type row', () => {
    cy.get('[data-cy=ctable-delete]').eq(0).click()
    cy.get('.p-toast-message-text').contains('Row Deleted').should('be.visible')
})


//navigate to the coupon page 
When('Manager: I navigate to the Coupon page', () => {
    cy.visit(`${ADMIN_URL}/mIndex/${firebase.merchantID}/merchant-create/coupons?language=en`)
    cy.url().should('eq', `${ADMIN_URL}/mIndex/${firebase.merchantID}/merchant-create/coupons?language=en`)
})
Then('Manager: I add the coupons', (table) => {
    const element = table.rowsHash();
    cy.get('[data-cy=merchant_create_coupons_coupon_code]').type(element.Code)
    cy.get('[data-cy=merchant_create_coupons_type]').children().eq(0).click()
    cy.get('[data-cy=merchant_create_coupons_value]').type(element.value)
    cy.get('[data-cy=merchant_create_coupons_minimum_order]').type(element.minimumOrder)
    cy.get('.dialogSaveButton').click()
    cy.wait(5000)
})
Then('Manager: I verify the coupons', (table) => {
    const element = table.rowSHash();
    cy.contains(element.Code).should('be.visible')
    cy.contains(element.Type).should('be.visible')
    cy.contains(element.value).should('be.visible')
    cy.contains(element.minimumOrder).should('be.visible')
    cy.wait(5000)

})
Then('Manager: I edit the coupons', (table) => {
    const element = table.rowHash();
    cy.get('[ng-reflect-index="0"] > [style="width: 7.5rem;"] > .d-flex > .fa-pencil').click()
    cy.get('[data-cy=merchant_create_coupons_coupon_code]').type(element.Code)
    cy.get('.p-toast-message-text').contains('Row updated').should('be.visible')
})
Then('Manager: I delete the row', () => {
    cy.get('[data-cy=ctable-delete]').eq(0).click()
})

//navigate to the ingredient page   

When('Manager: I navigate to ingredients page', () => {
    cy.visit(`${ADMIN_URL}/mIndex/${firebase.merchantID}/merchant-create/ingredients?language=en`)
    cy.url().should('eq', `${ADMIN_URL}/mIndex/${firebase.merchantID}/merchant-create/ingredients?language=en`)
})
Then('Manager: I add the ingredients', (table) => {
    const element = table.rowsHash();
    cy.get(':nth-child(1) > #input').type(element.Name)
    cy.get(':nth-child(2) > #input').type(element.Price)
    cy.get('.dialogSaveButton').click()
    cy.wait(5000)
})
Then('Manager: I verify the ingredients page row', (table) => {
    const element = table.rowsHash()
    cy.contains(element.Name)
    cy.contains(element.Price)
})
Then('Manager: I edit the ingredients page row', (table) => {
    const element = table.rowsHash();
    cy.get('data-cy=ctable-edit]').eq(0).click()
    cy.get(':nth-child(2) > #input').type(element.Price)
    cy.get('.dialogSaveButton').click()
    cy.wait(5000)
})

Then('Manager: I delete the ingredients page row', () => {
    cy.get('[data-cy=ctable-delete]').eq(0).click()
})

When('Manager: I navigate to the pincode page', () => {
    cy.visit(`${ADMIN_URL}/mIndex/${firebase.merchantID}/merchant-create/pincodes?language=en`)
    cy.url().should('eq', `${ADMIN_URL}/mIndex/${firebase.merchantID}/merchant-create/pincodes?language=en`)

})
Then('Manager: I add the pincode', (table) => {
    const element = table.rowsHash();
    cy.get('[data-cy=merchant_create_pin_code] > #input').type(element.pincode)
    cy.get('[data-cy=merchant_create_delivery_time] > #input').type(element.DeliveryTime)
    cy.get('[data-cy=merchant_create_status]').children().eq(2).click()
    cy.get('.dialogSaveButton').click()
    cy.wait(5000)
})
Then('Manager: I verify the pincode page row', (table) => {
    const element = table.rowsHash()
    cy.contains(element.pincode)
    cy.contains(element.DeliveryTime)
    cy.contains(element.status)
})
Then('Manager: I edit the row', (table) => {
    const element = table.rowsHash()
    cy.get('[data-cy=ctable-edit]').eq(0).click()
    cy.get('[data-cy=merchant_create_pin_code] > #input').clear().type(element.DeliveryTime)
    cy.get('.dialogSaveButton').click()
})
Then('Manager: I delete the pincode row', () => {
    cy.get('[data-cy=ctable-delete]').eq(0).click()
})

When('Manager:I navigate to the Paytm page', () => {
    cy.visit(`${ADMIN_URL}/mIndex/${firebase.merchantID}/merchant-create/paytm?language=en`)
    cy.url().should('eq', `${ADMIN_URL}/mIndex/${firebase.merchantID}/merchant-create/paytm?language=en`)
})
Then('Manager:I verify the paytm page', () => {
    cy.get('.orange-title').then(element => {
        expect(element.text().trim()).to.equal('PayTM');
    })
})

When('Manager:I navigate to the Paypal', () => {
    cy.visit(`${ADMIN_URL}/mIndex/${firebase.merchantID}/merchant-create/paypal?language=en`)
    cy.url().should('eq', `${ADMIN_URL}/mIndex/${firebase.merchantID}/merchant-create/paypal?language=en`)
})

Then('Manager:I verify the paypal page', () => {
    cy.get('.orange-title').then(element => {
        expect(element.text().trim()).to.equal('Paypal');
    })
})

When('Manager:i navigate to the stripe page', () => {
    cy.visit(`${ADMIN_URL}/mIndex/${firebase.merchantID}/merchant-create/stripe?language=en`)
    cy.url().should('eq', `${ADMIN_URL}/mIndex/${firebase.merchantID}/merchant-create/stripe?language=en`)
})

Then('Manager: i verify the strip page', () => {
    cy.get('.orange-title').then(element => {
        expect(element.text().trim()).to.equal('Stripe');

    })
})

When('Manager: I navigate to the RazorPay page', () => {
    cy.visit(`${ADMIN_URL}/mIndex/${firebase.merchantID}/merchant-create/razorpay?language=en`)
    cy.url().should('eq', `${ADMIN_URL}/mIndex/${firebase.merchantID}/merchant-create/razorpay?language=en`)
})
Then('Manager: I verify the Razorpay', () => {

    cy.get('.orange-title').then(element => {
        expect(element.text().trim()).to.equal('RazorPay');
    })

})

When('owner: I go to admin page with id: {word} with email: {string}', async (merchantID, email) => {
    cy.viewport(1400, 660);
    firebase = new FirebaseSDK(merchantID);
    await firebase.getIDToken();
    await firebase.getCustomToken(email);
    await firebase.getAccessToken();
    cy.visit(`${ADMIN_URL}/login?bearer=${firebase.customToken}&email=${email}&profileURL=${PROFILE_URL}&profileName=${'Devansh'}`)
    cy.wait(7000);
    cy.visit(`${ADMIN_URL}/mIndex/${firebase.merchantID}/merchant-create/organization/settings?language=en`);
});

Then('owner: I enter hotel info', (datatable) => {
    datatable.hashes().forEach(element => {
        cy.get("INPUT[name='name']").clear().type(element.name)
        cy.get("INPUT[name='email']").clear().type(element.email)
        cy.get("INPUT[name='timeout']").clear().type(element.userTimeOut)
        cy.get("INPUT[name='primarycontact']").clear().type(element.primaryContact)
        cy.get("INPUT[name='secondarycontact']").clear().type(element.secondaryContact)
        cy.get("INPUT[name='secondaryphone']").clear().type(element.secondaryPhone)
        cy.get("INPUT[name='latitude']").clear().type(element.latitude)
        cy.get("INPUT[name='longitude']").clear().type(element.longitude)
        //cy.get("INPUT[name='address1']").clear().type(element.primaryAdd)
        cy.get("INPUT[name='address2']").clear().type(element.secondaryAdd)
        cy.get("INPUT[name='superuseremail']").clear().type(element.superEmail)
        cy.get("TEXTAREA[name='about1']").clear().type(element.description1)
        cy.get("TEXTAREA[name='about2']").clear().type(element.description2)
        cy.get("TEXTAREA[name='about3']").clear().type(element.description3)
        cy.get('.dialogSaveButton').click()
        cy.get('.p-toast-message-text').contains('Success').should('be.visible')
    })
})



//waiter entitlement

When('I navigate to organization page', () => {
    cy.visit(`${ADMIN_URL}/mIndex/${firebase.merchantID}/merchant-create/organization/settings?language=en`)
    cy.url().should('eq', `${ADMIN_URL}/mIndex/${firebase.merchantID}/merchant-create/organization/settings?language=en`)
})
Then('I save the organization page', () => {
    cy.get('.dialogSaveButton').click()
})
Then('I verify the error toast', () => {
    cy.get('.p-toast-message-text').contains('Error').should('be.visible')
})

When('I navigate to operation page', () => {
    cy.visit(`${ADMIN_URL}/mIndex/${firebase.merchantID}/merchant-create/operations?language=en`)
    cy.wait(5000)
    cy.url().should('eq', `${ADMIN_URL}/mIndex/${firebase.merchantID}/merchant-create/operations?language=en`)
})
Then('I save the operation page', () => {
    cy.get('.dialogSaveButton').click()
})
When('I navigate to seat page', () => {
    cy.visit(`${ADMIN_URL}/mIndex/${firebase.merchantID}/merchant-create/seats?language=en`)
    cy.wait(5000)
    cy.url().should('eq', `${ADMIN_URL}/mIndex/${firebase.merchantID}/merchant-create/seats?language=en`)
})
Then('I verify the edit add and delete button', () => {
    cy.get('[data-cy=ctable-delete]').should('not.be.visible')
    cy.get('[data-cy=ctable-edit]').should('not.be.visible')
    cy.get('[data-cy=ctable-add]').should('not.be.visible')
})

When('I navigate to transaction page', () => {
    cy.visit(`${ADMIN_URL}/mIndex/${firebase.merchantID}/merchant-create/transactions?language=en`)
    cy.wait(5000)
    cy.url().should('eq', `${ADMIN_URL}/mIndex/${firebase.merchantID}/merchant-create/transactions?language=en`)
})
Then('I verify the text You do not have the permissions to view this. page', () => {
    cy.contains('You do not have the permissions to view this.').should('be.visible')
})

When('I navigate to payment page', () => {
    cy.visit(`${ADMIN_URL}/mIndex/${firebase.merchantID}/merchant-create/paymentlinks?language=en`)
    cy.wait(5000)
    cy.url().should('eq', `${ADMIN_URL}/mIndex/${firebase.merchantID}/merchant-create/paymentlinks?language=en`)
})
Then('I verify the text You do not have the permissions to view this. page', () => {
    cy.contains('You do not have the permissions to view this.').should('be.visible')
})

When('Admin: I navigate to the users page', () => {
    cy.get('[data-cy=header-toggle-button]').click();
    cy.get('[data-cy=header-manage-page]').click();
    cy.get('[data-cy=merchant-create-users-navigation]').click();
})

When('I navigate to item page', () => {
    cy.visit(`${ADMIN_URL}/mIndex/${firebase.merchantID}/merchant-create/itemtypes?language=en`)
    cy.wait(5000)
    cy.url().should('eq', `${ADMIN_URL}/mIndex/${firebase.merchantID}/merchant-create/itemtypes?language=en`)
})

When('I navigate to coupon', () => {
    cy.visit(`${ADMIN_URL}/mIndex/${firebase.merchantID}/merchant-create/coupons?language=en`)
    cy.wait(5000)
    cy.url().should('eq', `${ADMIN_URL}/mIndex/${firebase.merchantID}/merchant-create/coupons?language=en`)
})

When('I navigate to ingredients page', () => {
    cy.visit(`${ADMIN_URL}/mIndex/${firebase.merchantID}/merchant-create/coupons?language=en`)
    cy.wait(5000)
    cy.url().should('eq', `${ADMIN_URL}/mIndex/${firebase.merchantID}/merchant-create/coupons?language=en`)
})

When('I navigate to pincode page', () => {
    cy.visit(`${ADMIN_URL}/mIndex/${firebase.merchantID}/merchant-create/pincodes?language=en`)
    cy.wait(5000)
    cy.url().should('eq', `${ADMIN_URL}/mIndex/${firebase.merchantID}/merchant-create/pincodes?language=en`)
})

When('I navigate to paypal page', () => {
    cy.visit(`${ADMIN_URL}/mIndex/${firebase.merchantID}/merchant-create/paypal?language=en`)
    cy.wait(5000)
    cy.url().should('eq', `${ADMIN_URL}/mIndex/${firebase.merchantID}/merchant-create/paypal?language=en`)
})

When('I navigate to paytm page', () => {
    cy.visit(`${ADMIN_URL}/mIndex/${firebase.merchantID}/merchant-create/paytm?language=en`)
    cy.wait(5000)
    cy.url().should('eq', `${ADMIN_URL}/mIndex/${firebase.merchantID}/merchant-create/paytm?language=en`)
})

When('I navigate to Razorpay page', () => {
    cy.visit(`${ADMIN_URL}/mIndex/${firebase.merchantID}/merchant-create/razorpay?language=en`)
    cy.wait(5000)
    cy.url().should('eq', `${ADMIN_URL}/mIndex/${firebase.merchantID}/merchant-create/razorpay?language=en`)
})

When('I navigate to stripe page', () => {
    cy.visit(`${ADMIN_URL}/mIndex/${firebase.merchantID}/merchant-create/stripe?language=en`)
    cy.wait(5000)
    cy.url().should('eq', `${ADMIN_URL}/mIndex/${firebase.merchantID}/merchant-create/stripe?language=en`)
})

When('I navigate to API keys page', () => {
    cy.visit(`${ADMIN_URL}/mIndex/${firebase.merchantID}/merchant-create/apikeys?language=en`)
    cy.wait(5000)
    cy.url().should('eq', `${ADMIN_URL}/mIndex/${firebase.merchantID}/merchant-create/apikeys?language=en`)
})

When('I navigate to permissions page', () => {
    cy.visit(`${ADMIN_URL}/mIndex/${firebase.merchantID}/merchant-create/roles?language=en`)
    cy.wait(5000)
    cy.url().should('eq', `${ADMIN_URL}/mIndex/${firebase.merchantID}/merchant-create/roles?language=en`)
})

When('I navigate to prepare page', () => {
    cy.visit(`${ADMIN_URL}/mIndex/${firebase.merchantID}/prepare/grid?language=en`)
    cy.url().should('eq', `${ADMIN_URL}/mIndex/${firebase.merchantID}/prepare/grid?language=en`)
})

Then('I verify the prepare row', (table) => {
    const element = table.rowsHash()
    cy.get('[data-cy=prepare-items-name]').eq(0).contains(element.name).should('be.visible')
    // cy.get('[data-cy=prepare-items-status]').eq(0).contains(element.status).should('be.visible')
})

Then('I check buttons are disabled', (table) => {
    cy.get('[data-cy=prepare-items-status]').parent().should('be.disabled')
    cy.get('.negative-time-adjust').should('not.be.disabled')
    cy.get('.positive-time-adjust').should('not.be.disabled')
})

Then('I check the button are Enabled', (table) => {
    cy.get('[data-cy=prepare-items-status]').parent().should('not.be.disabled')
    cy.get('.negative-time-adjust').should('not.be.disabled')
    cy.get('.positive-time-adjust').should('not.be.disabled')

})

//prepare page testing
Then('Admin: I verify that status button is Active', () => {
    cy.get('[data-cy=prepare-standard-in-progress-button').eq(0).should('not.be.disabled');
});

Then('Admin: I verify the adjust time button is Active', () => {
    cy.get('[data-cy=prepare-standard-open-row-button]').eq(0).click();
    cy.get('[data-cy=prepare-standard-add-5-button]').eq(0).should('not.be.disabled');
    cy.get('[data-cy=prepare-standard-minus-5-button]').eq(0).should('not.be.disabled');
});

Then('Admin: I change the status of the order', () => {
    cy.get('[data-cy=prepare-standard-open-row-button]').eq(0).click();
    cy.get('[data-cy=prepare-standard-open-row-button]').eq(0).then(element => {
        expect(element.text().trim()).to.equal('In Progress');
    });
})

//guest order testing 

When('Guest: I verify the cart that product : {string} is present', (name) => {
    cy.contains(name).should('be.visible')
})

Then('Guest: I remove a quantity of {int}', (quantity) => {
    for (let i = 1; i <= quantity; i++) {
        cy.get('[data-cy=item-quantity-remove]').eq(0).click();
        cy.wait(2000)
    };
});
When('Guest: I verify the cart is empty', () => {
    cy.contains('Empty Cart !!').should('be.visible')
})

Then('Guest: I edit the product', () => {
    cy.get('span > .fa').eq(0).click()
})
Then('Guest: I click on the update cart', () => {
    cy.get('[data-cy=customization-add-to-cart]').click()
})


Then('Guest: I verify item quantity is changed to {int}', (quantity) => {
    cy.get('.item-quantity-suborder').contains(quantity).should('be.visible')
})

Then('Guest: I place order', () => {
    cy.get('[data-cy=cart-place-order]').click()
})

Then('Guest: I click on the add item button', () => {
    cy.get('[data-cy=order-tracking-add-item]').click()
})

Then('Guest:User-Details: I submit my user details for delivery', (table) => {
    const delivery = table.rowsHash();
    cy.get('[data-cy=user-details-name]').type(delivery["Name"]);
    cy.get('[data-cy=user-details-contact]').type(delivery["Contact"]);
    cy.get('[data-cy=user-details-address]').type(delivery["Address"]);
    cy.get('.p-dropdown').click()
    cy.get('.p-dropdown-item').eq(0).click()
    cy.get('.p-dropdown').click()
    cy.get('[data-cy=user-details-city]').type(delivery["city"]);
    cy.contains('Submit').click();
});

// ----------------------------------------------------------------RIYA CHANGES-----------------------------------------------------------------
// describe('image uploading',()=>{
//     it('I select Image',()=> {
//     cy.visit('http://localhost:4200/mIndex/M1_Prakash_Parvat/products/grid?language=en')
//     cy.get('[data-cy=product-grid-edit-arrow]').eq(0).click({multiple:true})
//     const yourFixturePath='product-test-image.jpg';
//     cy.wait(2000)
//     cy.get('[data-cy=product-grid-add-image]').attachFile(yourFixturePath);
//     cy.wait(7000)
//     cy.get('[data-cy=product-grid-save]').click()
//     });
 
    // });



    // describe('image uploading',()=>{
    //     it('I select Image',()=> {
    //     cy.visit('http://localhost:4200/mIndex/M1_Prakash_Parvat/products/grid?language=en')
    //     cy.get('[data-cy=product-grid-edit-arrow]').eq(0).click({multiple:true})
    //     const yourFixturePath='product-test-image.jpg';
    //     cy.wait(2000)
    //     cy.get('[data-cy=product-grid-add-image]').attachFile(yourFixturePath);
    //     cy.wait(7000)
    //     cy.get('[data-cy=product-grid-save]').click()
    //     });
     
    //     });
 Then ('Admin:I Added image: {string}', (name) => {
    cy.get('[data-cy=product-grid-Addclick]').click()
    const yourFixturePath=name;
    cy.wait(2000)
    cy.get('[data-cy=product-grid-add-image]').attachFile(yourFixturePath);
    cy.wait(7000)
        })
Then ('Admin:I added name: {string}', (name) => {
    cy.get('[data-cy=Admin-product-name-input]').type(name)
})
Then ('Admin:I added namehindi: {string}', (namehindi) => {
    cy.get('[data-cy=Admin-product-namehindi-input]').type(namehindi)
})
Then ('Admin:I added namemarathi: {string}', (namemarathi) => {
    cy.get('[data-cy=Admin-product-namemarathi-input]').type(namemarathi)
})
Then ('Admin:I added namegujarati: {string}', (namegujurati) => {
    cy.get('[data-cy=Admin-product-namegujarati-input]').type(namegujurati)
})
Then ('Admin:I added Price: {string}', (Price) => {
    cy.get('[data-cy=Admin-product-Price-input]').type(Price)
})
Then ('Admin:I added Rating: {string}', (Rating) => {
    cy.get('[data-cy=Admin-product-rating-input]').type(Rating)
})
Then ('Admin:I added Prepare Time: {string}', (PrepareTime) => {
    cy.get('[data-cy=Admin-product-PrepareTime-input]').type(PrepareTime)
})
Then ('Admin:I added Offers: {string}', (Offers) => {
    cy.get('[data-cy=Admin-product-Offers-input]').type(Offers)
})
// Then ('Admin:I select Item Type: {string}', (ItemType) => {
//     cy.scrollTo(0,200)
//     cy.get('[data-cy=Admin-product-Itemtype-input]').click()
// })


Then ('Admin:I select Item Type: {string}', (ItemType) => {
    cy.get('[data-cy=Admin-product-Itemtype-input]').children().eq(0).children().eq(2).click();
    cy.wait(2000)
    cy.get('[data-cy=Admin-product-Itemtype-input]').children().eq(0).children().eq(3).contains(ItemType).click();
    cy.get('[data-cy=Admin-product-Itemtype-input]').children().eq(0).children().eq(2).click();
    cy.wait(2000);
})
Then ('Admin:I add the product:{string},')

 Then('Admin:I added Description: {string}', (Description) => {
    cy.get('[data-cy=Admin-product-Description-input]').type(Description)
})
Then('Admin:I added Description in Hindi: {string}', (DescriptionHindi) => {
    cy.get('[data-cy=Admin-product-DescriptionHindi-input]').type(DescriptionHindi)
})
Then('Admin:I added Description in Marathi: {string}', (DescriptionMarathi) => {
    cy.get('[data-cy=Admin-product-DescriptionMarathi-input]').type(DescriptionMarathi)
})
Then('Admin:I added Description in Gujarati: {string}', (DescriptionGujarati) => {
    cy.get('[data-cy=Admin-product-DescriptionGujarati-input]').type(DescriptionGujarati)
})
Then('Admin:I select Veg', () => {
    cy.get('[data-cy=product-grid-veg-button]').click()
})
Then('Admin:I select NonVeg', () => {
    cy.get('[data-cy=product-grid-non-veg-button]').click()
})
Then ('Admin:I checked status', () =>{
    cy.get('[data-cy=product-grid-status]').click()
})
Then('Admin:I save product', () => {
    cy.get('[data-cy=product-grid-save]').click()
})
Then ('Admin:I click on Add' ,() => {
    cy.wait(5000)
    cy.get('[data-cy=product-grid-Addclick]').click()
})
Then ('Admin:I added name: {string}', (name) => {
    cy.get('[data-cy=Admin-product-name-input]').type(name)
})
Then ('Admin:I added namehindi: {string}', (namehindi) => {
    cy.get('[data-cy=Admin-product-namehindi-input]').type(namehindi)
})
Then ('Admin:I added namemarathi: {string}', (namemarathi) => {
    cy.get('[data-cy=Admin-product-namemarathi-input]').type(namemarathi)
})
Then ('Admin:I added namegujarati: {string}', (namegujurati) => {
    cy.get('[data-cy=Admin-product-namegujarati-input]').type(namegujurati)
})
// Then('Admin:I cancel the product: {string}', (cancel) => {
//     cy.get('[data-cy=product-grid-cancel]').type(cancel)
// })
// Then('Admin:I expanded the product', () => {
//     cy.get('[data-cy=product-grid-right-arrow]').eq(0).click()
// })
Then('Admin: I open the first product', () => {
    cy.wait(5000)
    cy.get('[data-cy=product-grid-right-arrow]').eq(0).click()
})
Then('Admin:I click on add button', () => {
    cy.get('[data-cy=Admin-product-addbutton]').eq(0).click()
})
Then('Admin:I Enter Item Name: {string}', (enteritem) => {
    cy.get('[data-cy=product-grid-customization-enteritem]').type(enteritem)
})
// Then('Admin:I enter Add: {string}', (Add) => {
//     cy.get('[data-cy=product-grid-customization-Add]')
// })
// Then('Admin:I click on remove: {string}', (remove) => {
//     cy.get('[data-cy=product-grid-customization-remove]')
// })
// Then('Admin:I click on chooseone: {string}', (chooseone) => {
//     cy.get('[data-cy=product-grid-customization-chooseone]')
// })

// Then('Admin:I click on cancel button: {string}', (cancelbutton) => {
//     cy.get('[data-cy=product-grid-customization-cancelbutton]')
// })

Then ('Admin:I select customization type Add: {string}', (customizationType) => {
    cy.get('[data-cy=customization-table-dropdown]').children().eq(0).children().eq(2).click();
    cy.wait(2000)
    cy.get('[data-cy=customization-table-dialogbox]').contains(customizationType).click()
  
})
//     cy.get('[data-cy=Admin-product-Add]').children().eq(0).click();
//     cy.contains(customizationType).click();
// })



// Then ('Admin:I select customization type Remove: {string}', (customizationType) => {
//     cy.get('[data-cy=Admin-product-Remove]').children().eq(0).children().eq(2).click();
//     cy.wait(2000)
//     cy.get('[data-cy=Admin-product-Remove]').children().eq(0).children().eq(3).contains(customizationType).click();
//     cy.get('[data-cy=Admin-product-Remove]').children().eq(0).children().eq(2).click();
//     cy.wait(5000);
// })
//     cy.get('[data-cy=Admin-product-Remove]').children().eq(1).click();
//     cy.contains(customizationType).click();
// })


// Then ('Admin:I select customization type ChooseOne: {string}', (customizationType) => {
//     cy.get('[data-cy=Admin-product-ChooseOne]').children().eq(0).children().eq(2).click();
//     cy.wait(2000)
//     cy.get('[data-cy=Admin-product-ChooseOne]').children().eq(0).children().eq(3).contains(customizationType).click();
//     cy.get('[data-cy=Admin-product-ChooseOne]').children().eq(0).children().eq(2).click();
//     cy.wait(5000);
// })
// Then 
// Then ('Admin:I select customization type ChooseOne: {string}', (customizationType) => {
//     cy.get('[data-cy=Admin-product-ChooseOne]').children().eq(2).click();
//     cy.contains(customizationType).click();
// })
Then('Admin:I Click on add of customization dialog box', () => {
    cy.get('[data-cy="Admin-product-addbutton"]').eq(1).click()
})
// Then('Admin:I click on Add ', () => {
//     cy.get('[data-cy=product-grid-Addclick]').click()
// })
// Then('Admin:I  Enter Item: {string}', (enteritem) => {
//     cy.get('[data-cy=Admin-product-itemname]')
// })
// Then('Admin:I Enter Item in Choose One: {string}', (enteritem) => {
//     cy.get('[data-cy=Admin-product-customization-item]').click(enteritem)
// })
Then('Admin:I Enter Item Name In Choose One: {string}', (enteritem) => {
    cy.wait(2000)
    cy.get('[data-cy=Admin-product-customization-item]').type(enteritem)
})
Then('Admin:I click on delete Item Name', () => {
    cy.get('[data-cy=customization-table-deleteicon]').click()
})
Then('Admin:I click on input number minus button', () => {
    cy.get('[data-cy="product-grid-input-number"]').children().eq(0).children().eq(1).click()
})

Then('Admin:I click on Addbutton', () => {
    cy.get('[data-cy=product-grid-customization-Addbutton]').click()
})

Then('Admin:I close the first product', () => {
    cy.wait(2000)
    cy.get('[data-cy=product-grid-right-arrow]').eq(0).click()
})
Then('Admin:I edit product', () => {
    cy.get('[data-cy=product-grid-edit-arrow]').eq(0).click({multiple:true})
})
Then('Admin:I delete product', () => {
    cy.get('[data-cy=product-grid-delete-arrow]').eq(0).click({multiple:true})
})
// I verify the product in products page
Then('Admin:I verify the product in products page', (table) => {
    const data = table.rowsHash();
    cy.wait(2000);
    cy.get('[data-cy=product-grid-name]').eq(0).then(element => {
        expect(element.text().trim()).to.equal(data['Name']);
    });
  
    cy.get('[data-cy=product-grid-price]').eq(0).then(element => {
        expect(element.text().trim()).to.equal(data['Price']);
    });
    cy.get('[data-cy=product-grid-rating]').eq(0).then(element => {
        expect(element.text().trim()).to.equal(data['Rating']);
    });
    cy.get('[data-cy=product-grid-preparetime]').eq(0).then(element => {
        expect(element.text().trim()).to.equal(data['Prepare Time']);
    });
    cy.get('[data-cy=product-grid-offeramount]').eq(0).then(element => {
        expect(element.text().trim()).to.equal(data['Offers']);
    });
    cy.get('[data-cy=products-grid-status]').eq(0).then(element => {
        expect(element.text().trim()).to.equal(data['Status']);
    });
    cy.get('[data-cy=products-grid-description]').eq(0).click().then(element => {
        expect(element.text().trim()).to.equal(data['Description']);
    });
    Then ('Admin:I close the dialog box of Description', () =>{
        cy.get('[data-cy=admin-customization-close').click()
    });    
    cy.get('[data-cy=products-grid-itemtypes]').eq(0).then(element => {
        expect(element.text().trim()).to.equal(data['Item Type']);
    });
});
Then('Admin:I delete the product in fast order screen', () => {
    cy.get('[data-cy=item-quantity-remove]').click()
})
Then('Admin:I search product: {string}', (search) => {
    cy.wait(2000)
    cy.get('[data-cy=admin-search-input]').type(search);
});
Then('Admin:I clear search box of product page', () => {
    cy.get('[data-cy=admin-search-input]').clear()
});


// Then('Admin: I verify the Item in products page', (table) => {
//     const data = table.rowsHash();
//     cy.wait(1000);
//     cy.get('[data-cy=suborders-standard-name-value]').eq(0).then(element => {
//         expect(element.text().trim()).to.equal(data['Item']);
//     });
//     cy.get('[data-cy=suborders-standard-suborder-status-dropdown]').eq(0).children().eq(0).children().eq(1).then(element => {
//         expect(element.text().trim()).to.equal(data['Type']);
//     });
//     cy.get('[data-cy=suborders-standard-quantity-badge]').eq(0).children().eq(0).then(element => {
//         expect(element.text().trim()).to.equal(data['Price']);
//     });
// });

Then('Guest:Menu: I select a category: {string}', async (category) => {
    cy.contains(category).click();
});

Then('Guest: I select a category: {string}', async (category) => {
    cy.get(`[data-cy=menu-category-open-${category}]`).click();
}); 
Then('Guest:Menu: I select a product: {string}', (name) => {
    cy.contains(name).click();
});

Then('Guest: I verify the product', (table) => {
    const data = table.rowsHash();
    cy.wait(2000);
    cy.get('[data-cy=guest-customization-name]').eq(0).then(element => {
        expect(element.text().trim()).to.equal(data['Name']);
    });
    cy.get('[data-cy=guest-customization-price]').eq(0).then(element => {
        expect(element.text().trim()).to.equal(data['Price']);
    });
    cy.get('[data-cy=guest-customization-veg]').eq(0).then(element => {
        expect(element.text().trim()).to.equal(data['Veg']);
    });
    cy.get('[data-cy=guest-customization-prepare-time]').eq(0).then(element => {
        expect(element.text().trim()).to.equal(data['Prepare Time']);
    });
    cy.get('[data-cy=guest-customization-rating]').eq(0).then(element => {
        expect(element.text().trim()).to.equal(data['Rating']);
    });
    cy.get('[data-cy=guest-customization-description]').eq(0).then(element => {
        expect(element.text().trim()).to.equal(data['Description']);
    });
});
Then('Guest:product Added to cart', () => {
    cy.get('[data-cy=customization-add-to-cart]').click()
})

Then('Guest: I verify first Products in cart', (table) => {
    const data = table.rowsHash();
    cy.wait(2000);
    cy.get('[data-cy=cart-items-name]').eq(0).then(element => {
        expect(element.text().trim()).to.equal(data['Name']);
    });
    cy.get('[data-cy=cart-items-price]').eq(0).then(element => {
        expect(element.text().trim()).to.equal(data['Price']);
    });
//     cy.get('[data-cy=cart-items-customization]').eq(0).click();

});
Then('Admin:I click on new order', () => {
    cy.get('[data-cy=orders-grid-new-order-button]').click()
})
Then('Admin:I search product in fast order screen: {string}', (search) => {
    cy.wait(2000)
    cy.get('[data-cy=fast-order-menu-search-input]').type(search);
});
// Then('Admin:I click on Appetizers', () => {
//     cy.get('[data-cy=fast-order-appetizers]').click()
// })
Then('Admin:I click on product in fast order screen', () => {
    cy.wait(2000)
    cy.get('[data-cy=fast-order-product]').eq(0).click()
})
Then('Admin:I increase the quantity {int}', (quantity) => {
    for (let i = 1; i <= quantity - 1; i++) {
        cy.get('[data-cy=item-quantity-add]').eq(0).click();
    };
});
// Then('Admin:I click on product', () => {
//     cy.get('[data-cy=fast-order-product]').click()
// })
Then('Admin:I verify the product', (table) => {
    const data = table.rowsHash();
    cy.wait(2000);
    cy.get('[data-cy=fast-order-menu-product-name]').eq(0).then(element => {
        expect(element.text().trim()).to.equal(data['Name']);
    });
    cy.get('[data-cy=fast-order-veg]').eq(0).then(element => {
        expect(element.text().trim()).to.equal(data['Veg']);
    });
    cy.get('[data-cy=fast-order-preparetime]').eq(0).then(element => {
        expect(element.text().trim()).to.equal(data['Prepare Time']);
    });
    cy.get('[data-cy=fast-order-price]').eq(0).then(element => {
        expect(element.text().trim()).to.equal(data['Price']);
    });
});
Then('Admin:I click on product in customization', () => {
    cy.get('[data-cy=fast-order-cart-product-img]').click()
})
Then('Admin:I write notes: {string}', (Notes) => {
    cy.wait(2000)
    cy.get('[data-cy=fast-order-customization-notes]').type(Notes);
});
Then('Admin:I click on customization', () => {
    cy.get('[attr.data-cy]=fast-order-customization-item-' + customization.item.replace(' ', '-') + '-' + chooseFrom.item.replace(' ', '-')).click()
});

Then('Admin:I click on save of customization dialog box', () => {
    cy.get('[data-cy=fast-order-customization-dialog-save-button]').click()
});
Then('Admin:I write notes in customization: {string}', (Notes) => {
    cy.wait(2000)
    cy.get('[data-cy=fast-order-screen-notes]').type(Notes);
});
// describe('image uploading',()=>{
//     it('I select Image',()=> {
//     cy.visit('http://localhost:4200/mIndex/M1_Prakash_Parvat/products/grid?language=en')
//     cy.get('[data-cy=product-grid-edit-arrow]').eq(0).click({multiple:true})
//     const yourFixturePath='product-test-image.jpg';
//     cy.wait(2000)
//     cy.get('[data-cy=product-grid-add-image]').attachFile(yourFixturePath);
//     cy.wait(7000)
//     cy.get('[data-cy=product-grid-save]').click()
//     });
 
//     });

// prepare page


Then('Guest:Menu: I select a product: {string}', (name) => {
    cy.contains(name).click();
});

Then('Guest: I verify first Products in cart', (table) => {
    const data = table.rowsHash();
    cy.wait(2000);
    cy.get('[data-cy=cart-items-name]').eq(0).then(element => {
        expect(element.text().trim()).to.equal(data['Name']);
    });
    cy.get('[data-cy=cart-items-price]').eq(0).then(element => {
        expect(element.text().trim()).to.equal(data['Price']);
    });
    // cy.get('[data-cy=cart-items-customization]').eq(0).click();

});

Then('Guest: I open the customization', () => {
    cy.get('[data-cy=customization-cart-details]').click()
});

Then('Guest: product Added to cart', () => {
    cy.get('[data-cy=customization-add-to-cart]').click()
})
Then('Guest: I go to header-cart', () => {
    cy.get('[data-cy=guest-header-cart]').click()
   });


Then('Guest: I click on notes', () => {
    cy.get('[data-cy=cart-details-notes]').click({force:true})
});

Then('Guest:I write notes: {string}', (Notes) => {
    cy.get('[data-cy=cart-add-notes-cart-details]').type(Notes);
});
Then('Guest: I save notes', () => {
    cy.get('[data-cy=cart-notes-save]').click()
});
Then('Guest:Cart: I place an order', () => {
    cy.get('[data-cy=cart-place-order]').click();
});


Then('Admin:I open prepare page',() => {
    cy.get('[data-cy=header-prepare-page]').click();
 });

 

Then('Admin:Prepare:Standard: I change the status in the prepare page for suborder with name: {string} to: {string}', (name, status) => {
    cy.wait(2000);
    if(status == 'Prepared') {
        cy.get('[data-cy=prepare-standard-prepared-button]').eq(0).click();
    }
    // } else {
    //     cy.get('[data-cy=prepare-standard-ready-button]').eq(0).click();
    // }
});


Then('Admin:I search product in prepare page: {string}', (search) => {
    cy.get('[data-cy=prepare-page-search]').type(search);
});
Then('Admin:I clear searchbox in prepare page', () => {
    cy.get('[data-cy=prepare-page-search]').clear()
});

// Then('Admin:I open suborder in order', () => {
//     cy.wait(2000)
//     cy.get('[data-cy=orders-open-suborder]').eq(0).click();
// })

// Then('Admin: I verify first order in orders page', (table) => {
//     const data = table.rowsHash();
//     cy.wait(2000);
//     cy.get('[data-cy=suborder-grid-item-name]').eq(0).then(element => {
//         expect(element.text().trim()).to.equal(data['Name']);
//     });
//     cy.get('[data-cy=prepare-items-status]').eq(0).then(element => {
//         expect(element.text().trim()).to.equal(data['Status']);
//     });


// });
Then('Admin: I close suborder of order row', () => {
    cy.get('[data-cy=orders-open-suborder]').eq(0).click();
})
Then('Guest:I click on Dinner', () => {
    cy.get('[data-cy=guest-order-dinner-arrow]').click();
});