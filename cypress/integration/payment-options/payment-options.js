import { Given, Before, Then, And, When } from "cypress-cucumber-preprocessor/steps";
import * as settings from "../../fixtures/settings.json";

let orderDetails = {
    "documentId": "2d9n7IGi5vX4ZfNlJ0gp",
    "entryDate": {
        "seconds": 1634924867,
        "nanoseconds": 831000000
    },
    "tokenCode": "AA-44",
    "merchantId": "M1_Prakash_Parvat_Test",
    "adjustedType": "Less",
    "userId": "jlHUP4pHUsNjJUWjBoypxmeFL0b9",
    "adjustedAmt": 0,
    "id": "2d9n7IGi5vX4ZfNlJ0gp",
    "totalQuantity": 1,
    "versionId": 20,
    "delivery": 0,
    "status": 2,
    "date": {
        "_seconds": 1635307755,
        "_nanoseconds": 912000000
    },
    "totalPrice": 30,
    "orderBy": 1,
    "paymentStatus": 0,
    "subOrders": {
        "9OnbBop3svv1KHfFIEpn": {
            "status": 2,
            "itemId": "M1_D4_Veggie_Grilled_Sandwich_Without_Cheese",
            "addedTime": 0,
            "itemPrice": 30,
            "orderId": "2d9n7IGi5vX4ZfNlJ0gn",
            "customNote": "",
            "startTime": 0,
            "entryDate": {
                "nanoseconds": 129000000,
                "seconds": 1635044895
            },
            "itemQuantity": 1,
            "readyTime": 0,
            "itemName": "Veggie Sandwich",
            "customizations": [],
            "date": {
                "seconds": 1634924875,
                "nanoseconds": 804000000
            },
            "acceptedDelay": 0,
            "id": "9OnbBop3svv1KHfFIEpn",
            "tokenCode": "AA-44-01"
        }
    },
    "tableNo": "",
    "tip": 0,
    "orderType": "DineIn"
};

Before(async () => {
    cy.task("initFirebase", settings.firebase_service_account);
    cy.task("setOrder", { id: orderDetails.id, order: orderDetails, merchantId: "M1_Prakash_Parvat_Test" });
    cy.wait(5000);
    cy.visit(`/go/M1_Prakash_Parvat_Test/payment-mode?language=en&order_id=${orderDetails.id}&user_id=${orderDetails.userId}`);
});

Given('I am on payment options page with title: {string}', (title) => {
    cy.get('[data-cy=payment-options-title]').then(el => {
        expect(el.text().trim()).to.equal(title);
    });
});

When('I select payment mode {string}', (mode) => {
    let paymentOptionsIds = {
        payAtCounter: 'payment-options-pay-at-counter'
    };
    cy.get(`[data-cy=${paymentOptionsIds['payAtCounter']}]`).click();
    cy.get('[data-cy=payment-options-make-payment]').click();
});

Then('I verify I am back on track order page with title: {string}', (title) => {
    cy.get('[data-cy=track-order-title]').then(el => {
        expect(el.text().trim()).to.equal(title);
    });
})