Feature: Guest Order Delivery Page











Scenario: Opening a guest order menu page
    Given I open guest order menu page
    When I see "Guest Order" in the title
    Then I open a category called "Starters"
Scenario:Opening the category
    Given I am on menu page
#     When I see 1 products in the menu page 
    Then I open a category called "Starters"
    And I select a product called "Veggie Sandwich"
Scenario: Add to cart
        Given I am on the customization page with title: "Veggie Sandwich."
        When I add the product to my cart
        Then I go to cart page
Scenario: Place order
        Given I am on cart details page with title: "Cart Details"
        When I place the order
        Then I land on the login page
Scenario:login as Guest
        Given I am on login page with title: "Sign In"
        When I place the order with "Guest" login
        Then I select "delivery" order type
Scenario:User-Details 
        Given I am on the user details page with title: "User Details"
        When I submit user details
        Then page failed to submit
 Scenario:User-Details failed for name
        Given I am on the user details page with title: "User Details"
        When I submit my user details for delivery name
          | Name | Riya |
        Then name failed to submit
Scenario:User-Details failed for Contact
     Given I am on the user details page with title: "User Details"
     When I submit my user details
               | Contact | 8766955553 |
     Then contact failed to submit
Scenario:User-Details failed for address
     Given I am on the user details page with title: "User Details"
     When I submit my user details for address
               | Address |Ahmednagar      |
     Then address failed to submit
Scenario:User-Details failed for zipcode
     Given I am on the user details page with title: "User Details"
     When  I submit my user details for zipcode
          | zipcode | 470004      |
   Then zipcode failed to submit
Scenario:User-details failed for city
    Given I am on the user details page with title: "User Details"
    When I clear address
    Then I submit my user details for city
        | city    | Ahmednagar     |
   And city failed to submit
Scenario:user details
   Given I am on the user details page with title: "User Details"
    When I submit my user details for address
               | Address |Ahmednagar      |
   Then I submit user details
 Scenario:track order
      Given I am on the track order page with title: "Track Order"
      When I verify in order tracking
         | Payment Status | Pending |
         | Status | In-Approval |
         | Table | Delivery |
         | Number Of Suborders | 1 |
       Then I modify order
  Scenario:modifying order
        Given I am on cart details page with title: "Cart Details"
        When I modify order by increasing quantity
        Then I go on track order page
         And I add item from track order
 Scenario:placing second order
       When I see 1 products in the menu page 
      Then I open a category called "Starters"
       And I select a product called "Veggie Bowl"
 Scenario: Add second product in cart
       Given I am on the customization page with title: "Veggie Bowl"
         When I add the product to my cart
       Then I go to cart page
       And I place the order
  Scenario:verifying track order page and cancel modify order
     Given I am on track order page with title: "Track Order"
    When I verify in order tracking
         | Payment Status | Pending |
         | Status | Open |
         | Table | Delivery  |
          | Number Of Suborders | 2 |
     Then I modify order
     And I cancel modify order
   Scenario:verifying track order page for second product
    Given I am on track order page with title: "Track Order"
    When I click on pay
    Then I verify payment details for second product
      | grandTotal | ₹92.40          |
      | noOfItems  | 2               |
      | totalQty   | 3               |
       | subTotal   | ₹76.00          |
      | SGST       |                 |
      | SGST       |                 |
       | CGST%      |                 |
       | CGST%      |                 |
      | tax%       | 5%              |
       | tax        | ₹3.80         |
       | tip        | ₹0.00           |
     And I go the payment options
 Scenario:mode of payment
     Given I am on payment options page with title: "Mode of Payment"
     When I verify total amount to be "₹52.90" and select "cashondelivery"
    Then I verify in order tracking
          | Payment Status | Cash On Delivery |
          | Status | Open |
          | Table | Delivery  |
          | Number Of Suborders | 2 |
     And I verify order in database