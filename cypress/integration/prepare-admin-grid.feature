Feature:Prepare page

Scenario:  Add Products To Cart
  Then I clear the cache of my application
 When Guest: I go to merchant with id: M1_Prakash_Parvat
Then Guest:Menu: I select a product: "Pancake"
Then Guest:product Added to cart
Then Guest: I go to header-cart
Then Guest: I verify first Products in cart
 | Name | Pancake |
      | Price | ₹30.00 |
Then Guest: I open the customization
Then Guest: product Added to cart
Then Guest: I go to header-cart
Then Guest: I click on notes
Then Guest:I write notes: "Pancake is Tasty"
Then Guest: I save notes
Then Guest:Cart: I place an order

Scenario:Prepare page
  When Admin: I go to merchant with id: M1_Prakash_Parvat and email: riyapolade@gmail.com
    Then Admin:I go to header
    Then Admin:I open prepare page
    Then Admin: I verify order in prepare
    | Name | Pancake |
     | Qty          | 1          |
      | Table Number |              |
    | Status | Open |
  Then Admin:I search product in prepare page: "Pancake"
   Then Admin:I clear searchbox in prepare page
   
   Scenario:orders page
     When Admin: I go to merchant with id: M1_Prakash_Parvat and email: riyapolade@gmail.com
    Then Admin:I go to header
   Then Admin:I open orders page
    Then Admin: I verify the order in orders grid page
     | Qty | 1 / 0 |
      | Price | ₹31.80 |
     | Status | In-Cart |
     | Payment Status | Pending |
    Then Admin: I open suborder of order row
   Then Admin: I verify first suborder in orders grid page
      | Name | Pancake|
      | Qty | 1 |
      | Price | ₹30.00 |
      | Status | In-Cart |
      | Prepare Time | 16 mins |
  Then Admin: I close suborder of order row