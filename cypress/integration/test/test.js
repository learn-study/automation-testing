const { Given, Before, When, Then, then } = require('cypress-cucumber-preprocessor/steps');
import * as settings from "../../fixtures/settings.json";
import * as merchant from '../../fixtures/M1_Prakash_Parvat_merchant.json'

Before(async () => {
    cy.task("initFirebase", settings.firebase_service_account);
  cy.task("createMerchant", { id: merchant.documentId, doc: merchant.document });
    cy.task("createSubcollection", {
      merchantId: merchant.documentId,
      subCollectionName: "Catalog",
      subcollections: merchant.subcollections,
    }, { timeout: 240000 });

    cy.task("createSubcollection", {
      merchantId: merchant.documentId,
      subCollectionName: "I18N",
      subcollections: merchant.subcollections,
    }, { timeout: 240000 });

    cy.task("createSubcollection", {
      merchantId: merchant.documentId,
      subCollectionName: "api_keys",
      subcollections: merchant.subcollections,
    }, { timeout: 240000 });

    cy.task("createSubcollection", {
      merchantId: merchant.documentId,
      subCollectionName: "counters",
      subcollections: merchant.subcollections,
    }, { timeout: 240000 });

    cy.task("createSubcollection", {
      merchantId: merchant.documentId,
      subCollectionName: "delivery_pincodes",
      subcollections: merchant.subcollections,
    }, { timeout: 240000 });

    cy.task("createSubcollection", {
      merchantId: merchant.documentId,
      subCollectionName: "delivery_boy_details",
      subcollections: merchant.subcollections,
    }, { timeout: 240000 });


    cy.task("createSubcollection", {
      merchantId: merchant.documentId,
      subCollectionName: "itemsType_details",
      subcollections: merchant.subcollections,
    }, { timeout: 240000 });

    cy.task("createSubcollection", {
      merchantId: merchant.documentId,
      subCollectionName: "messaging_config",
      subcollections: merchant.subcollections,
    }, { timeout: 240000 });

    cy.task("createSubcollection", {
      merchantId: merchant.documentId,
      subCollectionName: "notifications_config",
      subcollections: merchant.subcollections,
    }, { timeout: 240000 });

    cy.task("createSubcollection", {
      merchantId: merchant.documentId,
      subCollectionName: "payment_links",
      subcollections: merchant.subcollections,
    }, { timeout: 240000 });

    cy.task("createSubcollection", {
      merchantId: merchant.documentId,
      subCollectionName: "paypal_config",
      subcollections: merchant.subcollections,
    }, { timeout: 240000 });

    cy.task("createSubcollection", {
      merchantId: merchant.documentId,
      subCollectionName: "paytm_config",
      subcollections: merchant.subcollections,
    }, { timeout: 240000 });

    cy.task("createSubcollection", {
      merchantId: merchant.documentId,
      subCollectionName: "stripe_config",
      subcollections: merchant.subcollections,
    }, { timeout: 240000 });

    cy.task("createSubcollection", {
      merchantId: merchant.documentId,
      subCollectionName: "display_positions",
      subcollections: merchant.subcollections,
    }, { timeout: 240000 });

    cy.task("createSubcollection", {
      merchantId: merchant.documentId,
      subCollectionName: "razorpay_config",
      subcollections: merchant.subcollections,
    }, { timeout: 240000 });

    cy.task("createSubcollection", {
      merchantId: merchant.documentId,
      subCollectionName: "seats_details",
      subcollections: merchant.subcollections,
    }, { timeout: 240000 });

    cy.task("createSubcollection", {
      merchantId: merchant.documentId,
      subCollectionName: "table_configs",
      subcollections: merchant.subcollections,
    }, { timeout: 240000 });

    cy.task("createSubcollection", {
      merchantId: merchant.documentId,
      subCollectionName: "users",
      subcollections: merchant.subcollections,
    }, { timeout: 240000 });

    cy.task("createSubcollection", {
      merchantId: merchant.documentId,
      subCollectionName: "merchant_roles",
      subcollections: merchant.subcollections,
    }, { timeout: 240000 });

});

Given('I open app', () => {

});