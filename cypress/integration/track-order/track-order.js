import { Given, Before, Then, And, When } from "cypress-cucumber-preprocessor/steps";
import * as settings from "../../fixtures/settings.json";

let orderDetails = {
    "documentId": "2d9n7IGi5vX4ZfNlJ0gp",
    "entryDate": {
        "seconds": 1634924867,
        "nanoseconds": 831000000
    },
    "tokenCode": "AA-44",
    "merchantId": "M1_Prakash_Parvat_Test",
    "adjustedType": "Less",
    "userId": "jlHUP4pHUsNjJUWjBoypxmeFL0b9",
    "adjustedAmt": 0,
    "id": "2d9n7IGi5vX4ZfNlJ0gp",
    "totalQuantity": 1,
    "versionId": 20,
    "delivery": 0,
    "status": 2,
    "date": {
        "_seconds": 1635307755,
        "_nanoseconds": 912000000
    },
    "totalPrice": 30,
    "orderBy": 1,
    "paymentStatus": 0,
    "subOrders": {
        "9OnbBop3svv1KHfFIEpn": {
            "status": 2,
            "itemId": "M1_D4_Veggie_Grilled_Sandwich_Without_Cheese",
            "addedTime": 0,
            "itemPrice": 30,
            "orderId": "2d9n7IGi5vX4ZfNlJ0gn",
            "customNote": "",
            "startTime": 0,
            "entryDate": {
                "nanoseconds": 129000000,
                "seconds": 1635044895
            },
            "itemQuantity": 1,
            "readyTime": 0,
            "itemName": "Veggie Sandwich",
            "customizations": [],
            "date": {
                "seconds": 1634924875,
                "nanoseconds": 804000000
            },
            "acceptedDelay": 0,
            "id": "9OnbBop3svv1KHfFIEpn",
            "tokenCode": "AA-44-01"
        }
    },
    "tableNo": "T1",
    "tip": 0,
    "orderType": "DineIn"
};

Before({ tags: "@setOrder" }, async () => {
    cy.task("initFirebase", settings.firebase_service_account);
    cy.task("setOrder", { id: orderDetails.id, order: orderDetails, merchantId: "M1_Prakash_Parvat_Test" });
    cy.wait(5000);
    cy.visit(`/go/M1_Prakash_Parvat_Test/track?language=en&order_id=${orderDetails.id}&user_id=${orderDetails.userId}`);
});

Given('I am on track order page with title: {string}', (title) => {
    cy.wait(2000);
    cy.get('[data-cy=track-order-title]').then(el => {
        expect(el.text().trim()).to.equal(title);
    });
});

Then('I verify order on track order page', (table) => {
    let data = table.rowsHash();
    cy.wait(1000);
    cy.get(`[data-cy=track-order-token]`).then(el => {
        expect(el.text().trim().length).to.equal(5);
    });
    cy.get(`[data-cy=order-tracking-grand-total]`).then(el => {
        expect(el.text().trim()).to.equal(data['grandTotal']);
    });
    cy.get(`[data-cy=order-tracking-payment-status]`).then(el => {
        expect(el.text().trim()).to.equal(data['paymentStatus']);
    });
    cy.get(`[data-cy=order-tracking-status]`).then(el => {
        expect(el.text().trim()).to.equal(data['status']);
    });
    // cy.get(`[data-cy=order-tracking-table]`).then(el => {
    //   expect(el.text().trim()).to.equal(data['table']);
    // });
    for (let i = 0; i < +data['noOfItems']; i++) {
        cy.get(`[data-cy=order-tracking-name]`).then(el => {
            let item = data['items'].split(',')[i];
            expect(el.text().trim()).to.equal(item + '.');
        });
        cy.get(`[data-cy=order-tracking-suborder-status]`).children().eq(0).children().eq(0).then(el => {
            let item = data['suborderStatus'].split(',')[i];
            expect(el.text().trim()).to.equal(item);
        });
        cy.get(`[data-cy=order-tracking-price]`).then(el => {
            let price = data['itemPrice'].split(',')[i];
            expect(el.text().trim()).to.equal(price);
        });
        cy.get(`[data-cy=order-tracking-item-qty-badge]`).children().eq(0).then(el => {
            let itemQty = data['itemQty'].split(',')[i];
            expect(el.text().trim()).to.equal(itemQty);
        });
        cy.get('[data-cy=track-order-qty-spinner]').eq(i - 1).children().eq(0).children().eq(0).children().eq(1).then(el => {
            let itemQty = data['itemQty'].split(',')[i];
            expect(el.text().trim()).to.equal(itemQty);
        });
    }
});

Given('Item at {int} is {string} and quantity is {int}', (i, item, itemQty) => {
    cy.get(`[data-cy=order-tracking-name]`).eq(i - 1).then(el => {
        expect(el.text().trim()).to.equal(item + '.');
    });
    cy.get(`[data-cy=order-tracking-item-qty-badge]`).eq(i - 1).children().eq(0).then(el => {
        expect(Number(el.text().trim())).to.equal(itemQty);
    });
    cy.get('[data-cy=track-order-qty-spinner]').eq(i - 1).children().eq(0).children().eq(0).children().eq(1).then(el => {
        expect(Number(el.text().trim())).to.equal(itemQty);
    });
});

When('I increase quantity by {int} of item at {int}', (qty, index) => {
    for (let i = 0; i < qty; i++) {
        cy.get('[data-cy=track-order-qty-spinner]').eq(index - 1).children().eq(0).children().eq(0).children().eq(2).click();
    }
    cy.wait(500);
});

When('I decrease quantity by {int} of item at {int}', (qty, index) => {
    for (let i = 0; i < qty; i++) {
        cy.get('[data-cy=track-order-qty-spinner]').eq(index - 1).children().eq(0).children().eq(0).children().eq(0).click();
    }
    cy.wait(500);
});


And('I verify order in database', () => {
    cy.wait(4000);
    cy.task("getOrderById", { merchantId: orderDetails.merchantId, orderId: orderDetails.id, userId: orderDetails.userId }, { timeout: 5000 }).then(order => {
        expect(order.id).to.equal(orderDetails.id);
        expect(order.tokenCode).to.equal(orderDetails.tokenCode);
        expect(order.totalPrice).to.equal(orderDetails.totalPrice * 2);
        expect(order.totalQuantity).to.equal(orderDetails.totalQuantity + 1);
        expect(order.tip).to.equal(orderDetails.tip);
        expect(order.paymentStatus).to.equal(orderDetails.paymentStatus);
        expect(order.status).to.equal(orderDetails.status);
        expect(order.merchantId).to.equal(orderDetails.merchantId);
        expect(order.adjustedType).to.equal(orderDetails.adjustedType);
        expect(order.adjustedAmt).to.equal(orderDetails.adjustedAmt);
        expect(order.orderType).to.equal(orderDetails.orderType);
    });
})