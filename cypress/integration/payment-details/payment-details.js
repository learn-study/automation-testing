import { Given, Before, Then, And, When } from "cypress-cucumber-preprocessor/steps";
import * as settings from "../../fixtures/settings.json";

let orderDetails = {
    "documentId": "2d9n7IGi5vX4ZfNlJ0gp",
    "entryDate": {
        "seconds": 1634924867,
        "nanoseconds": 831000000
    },
    "tokenCode": "AA-44",
    "merchantId": "M1_Prakash_Parvat_Test",
    "adjustedType": "Less",
    "userId": "jlHUP4pHUsNjJUWjBoypxmeFL0b9",
    "adjustedAmt": 0,
    "id": "2d9n7IGi5vX4ZfNlJ0gp",
    "totalQuantity": 1,
    "versionId": 20,
    "delivery": 0,
    "status": 2,
    "date": {
        "_seconds": 1635307755,
        "_nanoseconds": 912000000
    },
    "totalPrice": 30,
    "orderBy": 1,
    "paymentStatus": 0,
    "subOrders": {
        "9OnbBop3svv1KHfFIEpn": {
            "status": 2,
            "itemId": "M1_D4_Veggie_Grilled_Sandwich_Without_Cheese",
            "addedTime": 0,
            "itemPrice": 30,
            "orderId": "2d9n7IGi5vX4ZfNlJ0gn",
            "customNote": "",
            "startTime": 0,
            "entryDate": {
                "nanoseconds": 129000000,
                "seconds": 1635044895
            },
            "itemQuantity": 1,
            "readyTime": 0,
            "itemName": "Veggie Sandwich",
            "customizations": [],
            "date": {
                "seconds": 1634924875,
                "nanoseconds": 804000000
            },
            "acceptedDelay": 0,
            "id": "9OnbBop3svv1KHfFIEpn",
            "tokenCode": "AA-44-01"
        }
    },
    "tableNo": "",
    "tip": 0,
    "orderType": "DineIn"
};

Before(async () => {
    cy.task("initFirebase", settings.firebase_service_account);
    cy.task("setOrder", { id: orderDetails.id, order: orderDetails, merchantId: "M1_Prakash_Parvat_Test" });
    cy.wait(2000);
    cy.visit(`/go/M1_Prakash_Parvat_Test/payment?language=en&order_id=${orderDetails.id}&user_id=${orderDetails.userId}`);
});

Given('I am on payment details page with title: {string}', (title) => {
    cy.wait(3000);
    cy.get('[data-cy=payment-details-title]').then(el => {
        expect(el.text().trim()).to.equal(title);
    });
});

When('I add tip of {int} in the order', (tip) => {
    for (let i = 0; i < tip; i++) {
        cy.get('[data-cy=payment-details-add-tip]').click();
    }
});

Then('I verify the order details in the payment details page', (table) => {
    let data = table.rowsHash();
    cy.get('[data-cy=payment-details-token]').then(el => {
        expect(el.text().trim().length).to.equal(5);
    });
    cy.get('[data-cy=payment-details-grand-total]').then(el => {
        expect(el.text().trim()).to.equal(data['grandTotal']);
    });
    cy.get('[data-cy=payment-details-total-qty]').then(el => {
        expect(el.text().trim()).to.equal(data['totalQty']);
    });
    cy.get('[data-cy=payment-details-subtotal]').then(el => {
        expect(el.text().trim()).to.equal(data['subTotal']);
    });
    cy.get('[data-cy=payment-details-tip]').then(el => {
        expect(el.text().trim()).to.equal(data['tip']);
    });
    if (data['SGST']) {
        cy.get('[data-cy=payment-details-sgst]').then(el => {
            expect(el.text().trim()).to.equal(data['SGST']);
        });
        cy.get('[data-cy=payment-details-sgst%]').then(el => {
            expect(el.text().trim()).to.equal(`SGST ${data['sgst%']} On ${data['subTotal']}`);
        });
    }
    if (data['CGST']) {
        cy.get('[data-cy=payment-details-cgst%]').then(el => {
            expect(el.text().trim()).to.equal(`CGST ${data['cgst%']} On ${data['subTotal']}`);
        });
        cy.get('[data-cy=payment-details-cgst]').then(el => {
            expect(el.text().trim()).to.equal(data['CGST']);
        });
    }
    if (data['tax']) {
        cy.get('[data-cy=payment-details-tax]').then(el => {
            expect(el.text().trim()).to.equal(data['tax']);
        });
        cy.get('[data-cy=payment-details-tax-label]').then(el => {
            expect(el.text().trim()).to.equal(`Tax ${data['tax%']} On ${data['subTotal']}`);
        });
    }
    for (let i = 0; i < +data['noOfItems']; i++) {
        cy.get('[data-cy=payment-details-item-name]').eq(0).then(el => {
            let item = data['items'].split(',')[i];
            expect(el.text().trim()).to.equal(item + '.');
        });

        cy.get('[data-cy=payment-details-item-price]').eq(0).then(el => {
            let itemPrice = data['itemPrice'].split(',')[i];
            expect(el.text().trim()).to.equal(itemPrice);
        });

        cy.get('[data-cy=payment-details-item-qty]').eq(0).then(el => {
            let itemQty = data['itemQty'].split(',')[i];
            expect(el.text().trim()).to.equal(itemQty);
        });
    }
});

And('I go to payment options page', () => {
    cy.get('[data-cy=payment-details-make-payment]').click();
});

And('I verify order in database', () => {
    cy.wait(4000);
  cy.task("getOrderById", {merchantId: orderDetails.merchantId, orderId: orderDetails.id, userId: orderDetails.userId}, {timeout: 5000}).then(order => {
      expect(order.id).to.equal(orderDetails.id);
      expect(order.tokenCode).to.equal(orderDetails.tokenCode);
      expect(order.totalPrice).to.equal(orderDetails.totalPrice);
      expect(order.totalQuantity).to.equal(orderDetails.totalQuantity);
      expect(order.tip).to.equal(orderDetails.tip + 5);
      expect(order.paymentStatus).to.equal(orderDetails.paymentStatus);
      expect(order.status).to.equal(orderDetails.status);
      expect(order.merchantId).to.equal(orderDetails.merchantId);
      expect(order.adjustedType).to.equal(orderDetails.adjustedType);
      expect(order.adjustedAmt).to.equal(orderDetails.adjustedAmt);
      expect(order.orderType).to.equal(orderDetails.orderType);
  });
})