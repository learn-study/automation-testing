Feature: Fast Order Desktop Dine Out Order

    @callBefore
    Scenario: Open Orders Grid Page
        Given I am on orders grid page
        When I verify counters and order rows
            | inCartLabel               | In Cart         |
            | inApprovalLabel           | In Approval     |
            | openLabel                 | Open            |
            | inProgressLabel           | In Progress     |
            | readyLabel                | Prepared        |
            | inDeliveryLabel           | In Delivery     |
            | deliveredLabel            | Delivered       |
            | missingTableLabel         | No Table        |
            | pendingPaymentLabel       | Pending Payment |
            | inCartOrdersCount         | 0               |
            | inApprovalOrdersCount     | 0               |
            | openOrdersCount           | 0               |
            | inProgressOrdersCount     | 0               |
            | readyOrdersCount          | 0               |
            | inDeliveryOrdersCount     | 0               |
            | deliveredOrdersCount      | 0               |
            | missingTableOrdersCount   | 0               |
            | pendingPaymentOrdersCount | 0               |
            | inCartSubordersCount      | 0               |
            | openSubordersCount        | 0               |
            | inProgressSubordersCount  | 0               |
            | readySubordersCount       | 0               |
            | inDeliverySubordersCount  | 0               |
            | deliveredSubordersCount   | 0               |
        Then I create new order

    Scenario: Search Non-Existing Product In Menu
        When I search for "asdf"
        Then I see 0 items

    Scenario: Search For Active Product In Menu
        When I search for "Paneer"
        Then I see 3 items
            | items | Matar Paneer,Shahi Paneer,Palak Paneer |

    Scenario: Add Item To Cart
        Given I am on fast order page
        When I open "Starters" and select "Veggie Bowl"
        Then I verify the items in cart
            | noOfItems  | 1           |
            | items      | Veggie Bowl |
            | quantities | 1           |
            | prices     | ₹16.00      |
            | itemPrices | ₹16.00      |

    Scenario: Add Customization To Cart Item Of Type "Add"
        Given Cart item in cart is "Veggie Bowl" with price "₹16.00" and itemPrice "₹16.00"
        When I select customization of type "add" with name "Cheese"
        Then I verify the items in cart
            | noOfItems  | 1           |
            | items      | Veggie Bowl |
            | quantities | 1           |
            | prices     | ₹21.00      |
            | itemPrices | ₹21.00      |
        And I verify the total price total be "₹22.26"

    Scenario: Add Customization To Cart Item Of Type "Remove"
        Given Cart item in cart is "Veggie Bowl" with price "₹21.00" and itemPrice "₹21.00"
        When I select customization of type "remove" with name "Avocado"
        Then I verify the items in cart
            | noOfItems  | 1           |
            | items      | Veggie Bowl |
            | quantities | 1           |
            | prices     | ₹20.00      |
            | itemPrices | ₹20.00      |
        And I verify the total price total be "₹21.20"

    Scenario: Add Customization To Cart Item of Type "Choose One"
        Given Cart item in cart is "Veggie Bowl" with price "₹20.00" and itemPrice "₹20.00"
        When I select customization of type "chooseOne" with name "Olive Oil" under "Oil"
        Then I verify the items in cart
            | noOfItems  | 1           |
            | items      | Veggie Bowl |
            | quantities | 1           |
            | prices     | ₹24.00      |
            | itemPrices | ₹24.00      |
        And I verify the total price total be "₹25.44"

    Scenario: Remove Customization To Cart Item Of Type "Add"
        Given Cart item in cart is "Veggie Bowl" with price "₹24.00" and itemPrice "₹24.00"
        When I select customization of type "add" with name "Cheese"
        Then I verify the items in cart
            | noOfItems  | 1           |
            | items      | Veggie Bowl |
            | quantities | 1           |
            | prices     | ₹19.00      |
            | itemPrices | ₹19.00      |
        And I verify the total price total be "₹20.14"

    Scenario: Remove Customization To Cart Item Of Type "Remove"
        Given Cart item in cart is "Veggie Bowl" with price "₹19.00" and itemPrice "₹19.00"
        When I select customization of type "remove" with name "Avocado"
        Then I verify the items in cart
            | noOfItems  | 1           |
            | items      | Veggie Bowl |
            | quantities | 1           |
            | prices     | ₹20.00      |
            | itemPrices | ₹20.00      |
        And I verify the total price total be "₹21.20"

    Scenario: Change Customization Of Cart Item Of Type "Choose One"
        Given Cart item in cart is "Veggie Bowl" with price "₹20.00" and itemPrice "₹20.00"
        When I select customization of type "chooseOne" with name "Light Olive" under "Oil"
        Then I verify the items in cart
            | noOfItems  | 1           |
            | items      | Veggie Bowl |
            | quantities | 1           |
            | prices     | ₹18.00      |
            | itemPrices | ₹18.00      |
        And I verify the total price total be "₹19.08"

    Scenario: Remove Customization To Cart Item of Type "Choose One"
        Given Cart item in cart is "Veggie Bowl" with price "₹18.00" and itemPrice "₹18.00"
        When I select customization of type "chooseOne" with name "Light Olive" under "Oil"
        Then I verify the items in cart
            | noOfItems  | 1           |
            | items      | Veggie Bowl |
            | quantities | 1           |
            | prices     | ₹16.00      |
            | itemPrices | ₹16.00      |
        And I verify the total price total be "₹16.96"

    Scenario: Apply Coupon When Cart Value Is Less Than Min Order Value
        Given Cart item in cart is "Veggie Bowl" with price "₹16.00" and itemPrice "₹16.00"
        When I apply coupon "PercentCoupon"
        Then I see the warning "Need Mininum Order Value ₹100.00"

    Scenario: Change Cart Item Quantity
        Given Cart item in cart is "Veggie Bowl" with price "₹16.00" and itemPrice "₹16.00"
        When I "increase" cart item quantity by 7
        Then I verify the items in cart
            | noOfItems  | 1           |
            | items      | Veggie Bowl |
            | quantities | 8           |
            | prices     | ₹128.00     |
            | itemPrices | ₹16.00      |
        And I verify the total price total be "₹135.68"
        And I verify the payment details
            | subtotal        | ₹128.00 |
            | adjustementAmt  | ₹0.00   |
            | adjustedType    | Less    |
            | tip             | ₹0.00   |
            | tipSpinnerValue | 0       |
            | sgst            |         |
            | cgst            |         |
            | tax             | ₹7.68   |

    Scenario: Apply Coupon When Cart Value Is More Than Min Order Value
        Given Total Value of cart is "₹135.68"
        When I apply coupon "PercentCoupon"
        Then I see coupon applied message "PercentCoupon Applied!"

    Scenario: Place Order When Order Type Is Not Selected
        Given Cart is not empty and order type is not selected
        When I place order
        And I see order type dialog
        And I close order type dialog
        Then I see message "Select Order Type"

    Scenario: Select Order Type
        Given Order type button has value "Order Type"
        When I select order type Dine out with user details
            | name    | Ritul      |
            | contact | 8989898989 |
        Then I verify order type: Dine out and user details
            | name    | Ritul      |
            | contact | 8989898989 |

    Scenario: Place Order When Payment Mode Is Not Selected
        Given Payment Status is "Pending"
        When I place order
        Then I see message "Select Payment Mode"
    
    Scenario: Select Pay At Counter
        Given "Pay At Counter" payment mode is available
        When I select "Pay At Counter" payment mode
        Then I verify payment status is "Pay At Counter"

    Scenario: Place Order
        Given Place order button has label "Place Order"
        When I place order
        Then I land on orders grid page and verify order
            | price          | ₹122.11     |
            | quantity       | 8 / 0       |
            | status         | Open        |
            | paymentStatus  | Pay At Counter     |
            | noOfSuborders  | 1           |
            | items          | Veggie Bowl |
            | itemQuantities | 8           |
            | itemPrices     | ₹16.00     |
            | customizations | --          |
            | notes          | --          |
            | prepareTime    | 32 mins     |
        When I verify counters and order rows
            | inCartLabel               | In Cart         |
            | inApprovalLabel           | In Approval     |
            | openLabel                 | Open            |
            | inProgressLabel           | In Progress     |
            | readyLabel                | Prepared        |
            | inDeliveryLabel           | In Delivery     |
            | deliveredLabel            | Delivered       |
            | missingTableLabel         | No Table        |
            | pendingPaymentLabel       | Pending Payment |
            | inCartOrdersCount         | 0               |
            | inApprovalOrdersCount     | 0               |
            | openOrdersCount           | 1               |
            | inProgressOrdersCount     | 0               |
            | readyOrdersCount          | 0               |
            | inDeliveryOrdersCount     | 0               |
            | deliveredOrdersCount      | 0               |
            | missingTableOrdersCount   | 0               |
            | pendingPaymentOrdersCount | 1               |
            | inCartSubordersCount      | 0               |
            | openSubordersCount        | 8               |
            | inProgressSubordersCount  | 0               |
            | readySubordersCount       | 0               |
            | inDeliverySubordersCount  | 0               |
            | deliveredSubordersCount   | 0               |