import { Given, Before, Then, And, When } from "cypress-cucumber-preprocessor/steps";
import * as merchant from '../../fixtures/M1_Prakash_Parvat_merchant.json'
import * as settings from "../../fixtures/settings.json";
import * as _ from 'lodash';

let orderDetails = {
    "documentId": "2d9n7IGi5vX4ZfNlJ0gp",
    "entryDate": {
        "seconds": 1634924867,
        "nanoseconds": 831000000
    },
    "tokenCode": "AA-44",
    "merchantId": "M1_Prakash_Parvat_Test",
    "adjustedType": "Less",
    "userId": "jlHUP4pHUsNjJUWjBoypxmeFL0b9",
    "adjustedAmt": 0,
    "id": "2d9n7IGi5vX4ZfNlJ0gp",
    "totalQuantity": 1,
    "versionId": 20,
    "delivery": 0,
    "status": 8,
    "date": {
        "seconds": 1635307755,
        "nanoseconds": 912000000
    },
    "totalPrice": 30,
    "orderBy": 1,
    "paymentStatus": 0,
    "subOrders": {
        "9OnbBop3svv1KHfFIEpn": {
            "status": 8,
            "itemId": "M1_D4_Veggie_Grilled_Sandwich_Without_Cheese",
            "addedTime": 0,
            "itemPrice": 30,
            "orderId": "2d9n7IGi5vX4ZfNlJ0gn",
            "customNote": "",
            "startTime": 0,
            "entryDate": {
                "nanoseconds": 129000000,
                "seconds": 1635044895
            },
            "itemQuantity": 1,
            "readyTime": 0,
            "itemName": "Veggie Sandwich",
            "customizations": [],
            "date": {
                "seconds": 1634924875,
                "nanoseconds": 804000000
            },
            "acceptedDelay": 0,
            "id": "9OnbBop3svv1KHfFIEpn",
            "tokenCode": "AA-44-01"
        }
    },
    "tableNo": "",
    "tip": 0,
    "orderType": "DineIn"
};

Before({ tags: "@callBefore" },async () => {
    cy.task("initFirebase", settings.firebase_service_account);
    cy.task("setOrder", { id: orderDetails.id, order: orderDetails, merchantId: "M1_Prakash_Parvat_Test" });
    cy.wait(2000);
    cy.visit(`/go/M1_Prakash_Parvat_Test/history?language=en&user_id=${orderDetails.userId}`);
});
Given('I open guest order menu page', () => {
    cy.visit('/mIndex/M1_Prakash_Parvat_Test?language=en');
    cy.contains('Prakash', { timeout: 30000 });
    cy.url().should('include', '/M1_Prakash_Parvat_Test');
  });
  Given('I am on menu page', () => {
    cy.visit('/mIndex/M1_Prakash_Parvat_Test?language=en');
    cy.contains('Prakash', { timeout: 30000 });
    cy.url().should('include', '/M1_Prakash_Parvat_Test');
  });
  When('I open a category called {string}', async (category) => {
    cy.contains(category).click();
});
Given('I am on menu page', () => {
    cy.visit('/mIndex/M1_Prakash_Parvat_Test?language=en');
    cy.contains('Prakash', { timeout: 30000 });
    cy.url().should('include', '/M1_Prakash_Parvat_Test');
  });
  Then('I go to order history', () => {
    cy.get('[data-cy=footer-order-history]').click();
});
  Then('I select {string} order type', (orderType) => {
    let orderTypeIds = {
      dinein: 'dine-in-button'
    }
    cy.get(`[data-cy=${orderTypeIds[orderType]}]`).click();
  });
  ('I place the order', () => {
    cy.get('[data-cy=cart-place-order]').click({ force: true });
  });
  When('I verify in order tracking', async (table) => {
    const expected = table.rowsHash();
    if (expected["Payment Status"]) {
        cy.get('[data-cy=order-tracking-payment-status]').then(element => {
            expect(element.text().trim()).to.equal(expected['Payment Status'])
        });
    }
    if (expect["Status"]) {
        cy.get('[data-cy=order-tracking-status]').then(element => {
            expect(element.text().trim()).to.equal(expected['Status'])
        });
    }
    if (expected["Table"]) {
        cy.get('[data-cy=order-tracking-table]').then(element => {
            expect(element.text().trim()).to.equal(expected['Table'])
        });
    }
    if (expected["Number Of Suborders"]) {
        cy.get('[data-cy=order-tracking-product]').should('have.length', expected['Number Of Suborders']);
    }
  });
  Given('I am on track order page with title: {string}', (title) => {
    cy.get('[data-cy=track-order-title]').then(el => {
      expect(el.text().trim()).to.equal(title);
    });
  });
  When('I place the order with {string} login', (login) => {
    let loginButtonIds = {
      Guest: 'guest-login-with-guest'
    };
    cy.wait(2000);
    cy.get('[data-cy=guest-header-cart]').click({ force: true });
    cy.get('[data-cy=cart-place-order]').click();
    cy.wait(3000);
    cy.get(`[data-cy=${loginButtonIds[login]}]`).click();
  });

  Then('I land on the login page', async () => {
    cy.url().should('include', '/login');
    cy.get('[data-cy=guest-login-option]').should('have.length', 2);
});
  Then('I open a category called {string}', async (category) => {
    cy.contains(category).click();
  });
  Given('I am on login page with title: {string}', (title) => {
    cy.wait(2000);
    cy.get('[data-cy=login-sign-in]').then(el => {
      expect(el.text().trim()).to.equal(title);
    });
  });
  When('I add the product to my cart', () => {
    cy.get('[data-cy=customization-add-to-cart]').click();
    });
  When('I see {int} products in the menu page', async (number) => {
    cy.get('[data-cy=menu-product]').should('have.length', number);
  });
  And('I select a product called {string}', async (name) => {
    cy.contains(name).click();
    });
    Then('I am on cart details page with title: {string}', () => {
        cy.wait(2000);
        cy.get('[data-cy=order-cart-details]').click() ;
        });
        Given('I am on the customization page with title: {string}', (title) => {
          cy.wait(2000);
          cy.get('[data-cy=guest-customization-product-name]').then(el => {
            expect(el.text().trim()).to.equal(title);
          });
        });
        Then('I land on the login page', async () => {
          cy.url().should('include', '/login');
          cy.get('[data-cy=guest-login-option]').should('have.length', 2);
        });
        And('I land on the login page', async () => {
          cy.url().should('include', '/login');
          cy.get('[data-cy=guest-login-option]').should('have.length', 2);
        });
      And('I write notes: {string}', (Notes) => {
        cy.wait(2000)
        cy.get('[data-cy=cart-add-notes-cart-details]').type(Notes);
    });
    Then('I place the order', () => {
      cy.get('[data-cy=cart-place-order]').click({ force: true });
    });
      Given('I am on cart details page with title: {string}', () => {
        cy.wait(2000);
        cy.get('[data-cy=order-cart-details]').click({force: true}) ;
        });
    Then('I go to cart page', () => {
        cy.get('[data-cy=footer-cart]').click();
        });
        When('I reorder my order', () => {
          cy.get('[data-cy=reorder-button]').click();
          });
          Then('I click on notes', () => {
            cy.get('[data-cy=cart-details-notes]').click();
            });
        And('I click on customization', () => {
            cy.get('[data-cy=customization-cart-details]').click();
            });
    Given('I am on the customization page with title: {string}', (title) => {
        cy.wait(2000);
        cy.get('[data-cy=guest-customization-product-name]').then(el => {
          expect(el.text().trim()).to.equal(title);
        });
      });
      And('I verify payment details', (table) => {
        let data = table.rowsHash();
        cy.get('[data-cy=payment-details-token]').then(el => {
          expect(el.text().trim().length).to.equal(5);
        });
        cy.get('[data-cy=payment-details-grand-total]').then(el => {
          expect(el.text().trim()).to.equal(data['grandTotal']);
        });
        cy.get('[data-cy=payment-details-total-qty]').then(el => {
          expect(el.text().trim()).to.equal(data['totalQty']);
        });
        cy.get('[data-cy=payment-details-subtotal]').then(el => {
          expect(el.text().trim()).to.equal(data['subTotal']);
        });
        cy.get('[data-cy=payment-details-tip]').then(el => {
          expect(el.text().trim()).to.equal(data['tip']);
        });
        if (data['SGST']) {
          cy.get('[data-cy=payment-details-sgst]').then(el => {
            expect(el.text().trim()).to.equal(data['SGST']);
          });
          cy.get('[data-cy=payment-details-sgst%]').then(el => {
            expect(el.text().trim()).to.equal(`SGST ${data['sgst%']} On ${data['subTotal']}`);
          });
        }
        if (data['CGST']) {
          cy.get('[data-cy=payment-details-cgst%]').then(el => {
            expect(el.text().trim()).to.equal(`CGST ${data['cgst%']} On ${data['subTotal']}`);
          });
          cy.get('[data-cy=payment-details-cgst]').then(el => {
            expect(el.text().trim()).to.equal(data['CGST']);
          });
        }
        if (data['tax']) {
          cy.get('[data-cy=payment-details-tax]').then(el => {
            expect(el.text().trim()).to.equal(data['tax']);
          });
          cy.get('[data-cy=payment-details-tax-label]').then(el => {
            expect(el.text().trim()).to.equal(`Tax ${data['tax%']} On ${data['subTotal']}`);
          });
        }
        for (let i = 0; i < +data['noOfItems']; i++) {
          cy.get('[data-cy=payment-details-item-name]').eq(0).then(el => {
            let item = data['items'].split(',')[i];
            expect(el.text().trim()).to.equal(item + '.');
          });
      
          cy.get('[data-cy=payment-details-item-price]').eq(0).then(el => {
            let itemPrice = data['itemPrice'].split(',')[i];
            expect(el.text().trim()).to.equal(itemPrice);
          });
      
          cy.get('[data-cy=payment-details-item-qty]').eq(0).then(el => {
            let itemQty = data['itemQty'].split(',')[i];
            expect(el.text().trim()).to.equal(itemQty);
          });
        }
      });
    Then('I click on pay', () => {
        cy.get('[data-cy=order-tracking-make-payment]').click();
      });
      When('I update cart', () => {
        cy.get('[data-cy=customization-add-to-cart]').click();
      });

      When('I save the notes', () => {
        cy.get('[data-cy=cart-notes-save]').click();
      });
      When('I go the payment options', () => {
        cy.get('[data-cy=payment-details-make-payment]').click();
      });
      Given('I am on payment options page with title: {string}', (title) => {
        cy.get('[data-cy=payment-details-title]').then(el => {
          expect(el.text().trim()).to.equal(title);
        });
      });
      
      
      Then('I verify total amount to be {string} and select {string}', (total, type) => {
        let paymentOptionsIds = {
          payAtCounter: 'payment-options-pay-at-counter'
        };
        // // cy.get(`[data-cy=payment-options-grand-total]`).then(el => {
        // //   expect(el.text().trim()).to.equal(total);
        // // });
        cy.get(`[data-cy=${paymentOptionsIds['payAtCounter']}]`).click();
        cy.get('[data-cy=payment-options-make-payment]').click();
      });
      Given('I am on payment details page with title: {string}', (title) => {
        cy.get('[data-cy=payment-details-title]').then(el => {
          expect(el.text().trim()).to.equal(title);
        });
      });
      Then('I am on payment details page with title: {string}', (title) => {
        cy.get('[data-cy=payment-details-title]').then(el => {
          expect(el.text().trim()).to.equal(title);
        });
      });
      Given('I am on payment details page with title: {string}', (title) => {
        cy.get('[data-cy=payment-details-title]').then(el => {
          expect(el.text().trim()).to.equal(title);
        });
      });



      Given('I am on order history', () => {
        
    });