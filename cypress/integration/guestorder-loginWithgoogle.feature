   Feature: Google DineIn for customizations, notes and quantity
   Scenario: Add Products To Cart
    Then I clear the cache of my application
    When Guest: I go to merchant with id: Demo
    Then Guest: I select a product: "Manchow Soup" within the category: ""
    Then Guest:Customization: I add a quantity of 1
    Then Guest:Customization: I select the following customizations: "Chicken"
    Then Guest:Customization: I add the product to my cart
    Then Guest: I go to cart
    Then Guest: I verify first Products in cart
      | Name | Manchow Soup |
      | Price | ₹14.00 |
    Then Guest: I go to cart
    Then Guest: I open the notes
    Then Guest: I add notes: 'This is notes'
    Then Guest: I save the notes

  #  Scenario: Verify In-Cart Order In Admin
  #    When Admin: I go to merchant with id: Demo with email: "mangeshlahamge41@gmail.com"
  #    Then Admin:I go to header
  #    Then Admin:I open orders page
  #    Then Admin: I verify the order in orders grid page
  #     | Qty | 1 / 0 |
  #     | Order Type | |
  #     | Price | ₹14.28 |
  #     | Status | In-Cart |
  #     | Payment Status | Pending |
  #    Then Admin: I open suborder of order row
  #    Then Admin: I verify first suborder in orders grid page
  #     | Name | Manchow Soup|
  #     | Qty | 1 |
  #     | Price | ₹14.00 |
  #     | Status | In-Cart |
  #     | Prepare Time | 22 mins |
  #    Then Admin: I open suborder of order row 

  #  Scenario: Add New Products With Different Customization
  #   When Guest: I go to merchant with id: Demo
  #   Then Guest: I select a product: "Manchow Soup" within the category: ""
  #   Then Guest:Customization: I add a quantity of 1
  #   Then Guest:Customization: I select the following customizations: "Noodles"
  #   Then Guest: I verify similar Products in customization
  #   | Name | Manchow Soup |
  #   | Price | ₹14.00 |
  #   | Qty | 1 |
  #   Then Guest:Customization: I add the product to my cart
  #   Then Guest: I go to cart
  #   Then Guest: I go to back menu
  #   Then Guest: I select a product: "Manchow Soup" within the category: ""
  #   Then Guest:Customization: I add a quantity of 2
  #   Then Guest:Customization: I select the following customizations: "Noodles"
  #   Then Guest:Customization: I add the product to my cart
  #   Then Guest: I go to cart
  #   Then Guest: I verify first Products in cart
  #     | Name | Manchow Soup |
  #     | Price | ₹8.00 |
  #     | Qty | 2 |
  #     Then Guest: I go to cart
  #     Then Guest: I verify second Products in cart
  #     | Name | Manchow Soup |
  #     | Price | ₹14.00 |
  #     | Qty | 1 |

  # Scenario: Verify In-Cart Order In Admin
  #    When Admin: I go to merchant with id: Demo with email: "mangeshlahamge41@gmail.com"
  #    Then Admin:I go to header
  #    Then Admin:I open orders page
  #    Then Admin: I verify the order in orders grid page
  #     | Qty | 3 / 0 |
  #     | Order Type | |
  #     | Price | ₹22.44  |
  #     | Status | In-Cart |
  #     | Payment Status | Pending |
  #    Then Admin: I open suborder of order row
  #    Then Admin: I verify first suborder in orders grid page
  #     | Name | Manchow Soup|
  #     | Qty | 2 |
  #     | Price | ₹4.00 |
  #     | Status | In-Cart |
  #     | Prepare Time | 22 mins |
  #     | Customizations | Noodles (-) |
  #    Then Admin: I verify second suborder in orders grid page
  #     | Name | Manchow Soup|
  #     | Qty | 1 |
  #     | Price | ₹14.00 |
  #     | Status | In-Cart |
  #     | Prepare Time | 22 mins |
  #     | Customizations | Noodles (-) |

  #   Scenario: add and remove product quantity in cart
  #    When Guest: I go to merchant with id: Demo
  #    Then Guest: I go to cart
  #    Then Guest: I go to menu
  #    Then Guest: I go to cart
  #    Then Guest:Cart: I add a quantity in first product of 2
  #    Then Guest:Cart: I remove a quantity in second product of 1

  #   Scenario: Verify In-Cart Order In Admin
  #    When Admin: I go to merchant with id: Demo with email: "mangeshlahamge41@gmail.com"
  #    Then Admin:I go to header
  #    Then Admin:I open orders page
  #    Then Admin: I verify the order in orders grid page
  #     | Qty | 4 / 0 |
  #     | Order Type | |
  #     | Price | ₹26.52  |
  #     | Status | In-Cart |
  #     | Payment Status | Pending |
  #    Then Admin: I open suborder of order row
  #    Then Admin: I verify first suborder in orders grid page
  #     | Name | Manchow Soup|
  #     | Qty | 3 |
  #     | Price | ₹4.00 |
  #     | Status | In-Cart |
  #     | Prepare Time | 22 mins |
  #   # | Customizations | Noodles (-) |
  #    Then Admin: I verify second suborder in orders grid page
  #     | Name | Manchow Soup|
  #     | Qty | 1 |
  #     | Price | ₹14.00 |
  #     | Status | In-Cart |
  #     | Prepare Time | 22 mins |

    # | Customizations | Chicken (+) |
  #     # Discuss with ritul -------------

  #      Then Admin: I open suborder quanity
  #      Then Admin: I remove a quantity in first suborder of 1
  #      Then Admin: I add a quantity in second suborder of 1

  #     # --------------------


  #     Then Admin:I go to first suborder customization
  #     Then Admin:I select the following customizations in first suborder:"Chiken, Spices, Noodles "
  #     Then Admin: I save the suborder customizations

  #     Then Admin:I go to second suborder customization
  #     Then Admin:I select the following customizations in second suborder:"Noodles, Shredded Garlic"
  #     Then Admin: I save the suborder customizations

      # Then Admin:I go to suborder notes
      # Then Admin: I add notes in suborder: 'Notes modifyed'
      # Then Admin: I save the notes


  #     # Discuss with ritul InPLace Dropdown -------------
  #     # Then Admin:I select suborder status
  #     #  Then Admin:I change first suborder status to Open
  #     # -------------

  Scenario: Place Order As Guest Through Dine In
    When Guest: I go to merchant with id: Demo
    Then Guest: I go to cart
    Then Guest: I go to menu
    Then Guest: I go to cart
    Then Guest: I go to menu
    Then Guest: I go to cart
    #  After Add customization and quantity --------------
    #  Then Guest: I verify first Products in cart
    #   | Name | Manchow Soup |
    #   | Price | ₹8.00 |
    #   | Qty | 2 |
    #   Then Guest: I go to cart
    #   Then Guest: I verify second Products in cart
    #   | Name | Manchow Soup |
    #   | Price | ₹14.00 |
    #   | Qty | 1 |
    Then Guest:Cart: I add a coupon code: "first25"
    Then Guest:Cart: I place an order
    Then Guest:Login: I login with Google