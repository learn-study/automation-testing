Feature: Fast Order Desktop Dine In Order

    @callBefore
    Scenario: Open Orders Grid Page
        Given I am on orders grid page
        When I verify counters and order rows
            | inCartLabel               | In Cart         |
            | inApprovalLabel           | In Approval     |
            | openLabel                 | Open            |
            | inProgressLabel           | In Progress     |
            | readyLabel                | Prepared        |
            | inDeliveryLabel           | In Delivery     |
            | deliveredLabel            | Delivered       |
            | missingTableLabel         | No Table        |
            | pendingPaymentLabel       | Pending Payment |
            | inCartOrdersCount         | 1               |
            | inApprovalOrdersCount     | 1               |
            | openOrdersCount           | 1               |
            | inProgressOrdersCount     | 1               |
            | readyOrdersCount          | 1               |
            | inDeliveryOrdersCount     | 1               |
            | deliveredOrdersCount      | 1               |
            | missingTableOrdersCount   | 1               |
            | pendingPaymentOrdersCount | 6               |
            | inCartSubordersCount      | 3               |
            | inApprovalSubordersCount  | 4               |
            | openSubordersCount        | 5               |
            | inProgressSubordersCount  | 5               |
            | readySubordersCount       | 2               |
            | inDeliverySubordersCount  | 6               |
            | deliveredSubordersCount   | 3               |

    Scenario: In Cart Counter Filter
        Given No of orders visible are 7
        When I filter orders by status "In-Cart"
        Then I verify orders
            | noOfOrders | 1       |
            | status     | In-Cart |

    Scenario: Remove In Cart Filter
        Given The orders are filtered by "In-Cart" status and label is "Filtered by - In-Cart"
        When I close filter
        Then I verify orders
            | noOfOrders | 7 |

    Scenario: In Cart Counter Filter
        Given No of orders visible are 7
        When I filter orders by status "Open"
        Then I verify orders
            | noOfOrders | 1    |
            | status     | Open |

    Scenario: Remove In Cart Filter
        Given The orders are filtered by "Open" status and label is "Filtered by - Open"
        When I close filter
        Then I verify orders
            | noOfOrders | 7 |

    Scenario: In Cart Counter Filter
        Given No of orders visible are 7
        When I filter orders by status "In-Progress"
        Then I verify orders
            | noOfOrders | 1           |
            | status     | In-Progress |

    Scenario: Remove In Cart Filter
        Given The orders are filtered by "In-Progress" status and label is "Filtered by - In-Progress"
        When I close filter
        Then I verify orders
            | noOfOrders | 7 |

    Scenario: In Cart Counter Filter
        Given No of orders visible are 7
        When I filter orders by status "Prepared"
        Then I verify orders
            | noOfOrders | 1       |
            | status     | Prepared |

    Scenario: Remove In Cart Filter
        Given The orders are filtered by "Prepared" status and label is "Filtered by - Prepared"
        When I close filter
        Then I verify orders
            | noOfOrders | 7 |

    Scenario: In Cart Counter Filter
        Given No of orders visible are 7
        When I filter orders by status "In-Delivery"
        Then I verify orders
            | noOfOrders | 1           |
            | status     | In-Delivery |

    Scenario: Remove In Cart Filter
        Given The orders are filtered by "In-Delivery" status and label is "Filtered by - In-Delivery"
        When I close filter
        Then I verify orders
            | noOfOrders | 7 |

    Scenario: In Cart Counter Filter
        Given No of orders visible are 7
        When I filter orders by status "Delivered"
        Then I verify orders
            | noOfOrders | 1         |
            | status     | Delivered |

    Scenario: Remove In Cart Filter
        Given The orders are filtered by "Delivered" status and label is "Filtered by - Delivered"
        When I close filter
        Then I verify orders
            | noOfOrders | 7 |