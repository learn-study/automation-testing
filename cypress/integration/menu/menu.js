import { Given, When, Then } from "cypress-cucumber-preprocessor/steps";
import * as settings from "../../fixtures/settings.json";

Then('I clear the cache of my application', () => {
    window.indexedDB.deleteDatabase("firebaseLocalStorageDb");
    cy.wait(5000);
    cy.task("initFirebase", settings.firebase_service_account);
});

Given('I open guest order menu page and see {int} product', async (number) => {
    cy.viewport(1400, 660);
    cy.visit('localhost:4201/mIndex/M1_Prakash_Parvat_Test?language=en');    
    cy.contains('Prakash', { timeout: 30000 });
    cy.url().should('include', '/M1_Prakash_Parvat_Test');
    cy.get('[data-cy=menu-product]').should('have.length', number);
});

When('I open a category called {string}', async (category) => {
    cy.contains(category).click();
});

Then('I see {int} products in the menu page', async (number) => {
    cy.get('[data-cy=menu-product]').should('have.length', number);
});

When('I select a product called {string}', async (name) => {
    cy.contains(name).click();
});

Then('I land on the customization page for {string}', async (name) => {
    cy.url().should('include', '/customization');
    cy.get('[data-cy=guest-customization-product-name]').then(element => {
        expect(element.text().trim()).to.equal(name);
    });
});

Then('The total quantity on the customization page is {int}', async (expectedTotalQuantity) => {
    cy.get('[data-cy=item-quantity-number]').then(element => {
        expect(Number(element.text().trim())).to.equal(expectedTotalQuantity);
    });
}); 

Then('I add a quantity of {int} in the customization page', (quantity) => {
    for (let i = 0; i <= quantity - 1; i++) {
        cy.get('[data-cy=item-quantity-add]').eq(0).click();
    };
});

Then('I remove a quantity of {int} in the customization page', (quantity) => {
    for (let i = 0; i <= quantity - 1; i++) {
        cy.get('[data-cy=item-quantity-remove]').eq(0).click();
    };
});

Then('I add customizations in the customizations page', async (table) => {
    const customizations = table.rowsHash();
    for (let customization in customizations) {
        cy.contains(customization).click();
        cy.get('[data-cy=guest-customization-price]').then(element => {
            expect(element.text().trim()).to.equal(customizations[customization]);
        });
    };
});

Then('I remove customizations in the customizations page', async (table) => {
    const customizations = table.rowsHash();
    for (let customization in customizations) {
        cy.contains(customization).click();
        cy.get('[data-cy=guest-customization-price]').then(element => {
            expect(element.text().trim()).to.equal(customizations[customization]);
        });
    };
});

Then('I add the product to my cart', () => {
    cy.get('[data-cy=customization-add-to-cart]').click();
});

Then('I go to cart page', () => {
    cy.get('[data-cy=footer-cart]').click();
});

Then('I see {int} suborder in the cart page', async (expectedSuborderQuantity) => {
    cy.get('[data-cy=guest-cart-suborder]').should('have.length', expectedSuborderQuantity);
});

Then('I see the grand total is {string} and {int} in database', async (expectedGrandTotal, expectedDatabaseGrandTotal) => {
    cy.get('[data-cy=cart-total]').then(element => {
        expect(element.text().trim()).to.equal(expectedGrandTotal);
    });
    cy.url().then(url => {
        const orderId = url.split("order_id=")[1].split("&")[0];
        cy.task("getOrderById", { orderId: orderId, merchantId: "M1_Prakash_Parvat_Test" }, { timeout: 5000 }).then(order => {
            const cartTotal = 0;
            for (let suborderId in order.subOrders) {
                if (order.subOrders[suborderId].status === 0) {
                    cartTotal += (order.subOrders[suborderId].itemPrice * order.subOrders[suborderId].itemQuantity);
                };
            };
            expect(cartTotal).to.equal(expectedDatabaseGrandTotal);
        });
    });
});

When('I place the order', () => {
    cy.get('[data-cy=cart-place-order]').click({ force: true });
});

Then('I land on the login page', async () => {
    cy.url().should('include', '/login');
    cy.get('[data-cy=guest-login-option]').should('have.length', 3);
});