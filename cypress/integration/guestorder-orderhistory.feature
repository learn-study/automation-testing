Feature: Guest Order Order History


@callBefore
Scenario:order history
   Given I am on order history
   When I reorder my order
   Then I am on cart details page with title: "Cart Details"
   And I click on customization
Scenario:update cart details
     Given I am on the customization page with title: "Veggie Sandwich."
    When I update cart
    Then I click on notes
    And I write notes: "Its yummy"
Scenario:cart details
   When I save the notes
   Then I place the order
   And I land on the login page
Scenario:login as Guest
        Given I am on login page with title: "Sign In"
        When I place the order with "Guest" login
        Then I select "dinein" order type
Scenario:verifying track order page
      Given I am on track order page with title: "Track Order"
       When I verify in order tracking
         | Payment Status | Pending |
         | Status | In-Approval  |
         | Table | ----   |
         | Number Of Suborders | 1 |
       Then I click on pay
       And I verify payment details
        | grandTotal | ₹31.80          |
        | noOfItems  | 1               |
        | items      | Veggie Sandwich |
        | itemPrice  | ₹30.00          |
        | itemQty    | 1               |
        | totalQty   | 1               |
        | subTotal   | ₹30.00          |
        | SGST       |                 |
        | SGST       |                 |
        | CGST%      |                 |
        | CGST%      |                 |
        | tax%       | 6%              |
        | tax        | ₹1.80           |
        | tip        | ₹0.00           |


   Scenario:payment
       Given I am on payment options page with title: "Make Payment"
      When I go the payment options
      Then I verify total amount to be "₹31.80" and select "payAtCounter"
       And I verify in order tracking  
         | Payment Status | Pay At Counter |
         | Status | Open |
         | Table | ----   |
         | Number Of Suborders | 2 |
       And I verify order in database 