Feature: Fast order screen.
    Scenario: Dine In Order With Customization Notes and Two Items.

        When Admin: I go to merchant with id: M1_Prakash_Parvat and email: ritul.shrivastava2142@gmail.com
        Then Admin: I go to grid view of orders page
        Then Admin: I create new order from orders grid
        Then Admin: I verify the fast order screen top menu
            | orderType     | Order Type |
            | grantTotal    | 0.00       |
            | totalQuantity | 0          |
            | status        | In-Cart    |
            | tokenCode     | Token Code |
            | paymentStatus | Pending    |
        Then Admin: I test the search bar with product "Veggie Bowl" and search text "veggie bowl" in fast order
        Then Admin: I select category "Starters" and product "Veggie Bowl" With Quantity 2 in fast order screen
        Then Admin: I select "Cheese" customization from fast order screen
        Then Admin: I add note "less spicy" from fast order screen
        Then Admin: I verify the cart item in fast order page
            | name           | Veggie Bowl |
            | qty            | 2           |
            | price          | 19          |
            | customizations | Cheese(+)   |
            | notes          | less spicy  |
        # Then Admin: I verify the total dialog fast order screen
        #     | subtotal    | 0.00 |
        #     | tips        | 0.00 |
        #     | adjustedAmt | 0.00 |
        #     | grandTotal  | 0.00 |
        Then Admin: I verify the total dialog fast order screen cancel button
        Then Admin: I change the tips and adjustment Amt to 5 and 5
        # Then Admin: I verify the total dialog fast order screen
        #     | subtotal      | 0.00       |
        #     | tips          | 0.00       |
        #     | adjustedAmt   | 0.00       |
        #     | grandTotal    | 0.00       |

        Then Admin: I verify the fast order screen top menu
            | orderType     | Order Type |
            | grantTotal    | 0.00       |
            | totalQuantity | 2          |
            | status        | In-Cart    |
            | tokenCode     |            |
            | paymentStatus | Pending    |

        Then Admin: I select order type "Dine In" with table number "R-25" from fast order page

        Then Admin: I verify the fast order screen top menu
            | orderType     | Dine In |
            | grantTotal    | 0.00    |
            | totalQuantity | 2       |
            | status        | In-Cart |
            | tokenCode     |         |
            | paymentStatus | Pending |
            | table         | R-25    |

        Then Admin: I place order from fast order screen
        Then Admin: I verify the order in orders grid
            | tokenCode     |         |
            | grandTotal    | 0.00    |
            | totalQuantity | 2 / 0   |
            | status        | Open    |
            | paymentStatus | Pending |
            | orderType     | Dine In |
            | table         | R-25    |

        Then Admin: I open the order rows in orders grid
        Then Admin: I verify suborder 1 in orders grid
            | name           | Veggie Bowl |
            | qty            | 2           |
            | status         | Open        |
            | price          | 21          |
            | notes          | less spicy  |
            | customizations | Cheese (+)  |
