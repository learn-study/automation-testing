Feature: Guest Order Dinein Page
@callBefore
Scenario: Opening a guest order menu page
    Given I open guest order menu page
    When I see "Guest Order" in the title
    Then I open a category called "Starters"
Scenario:Opening the category
    Given I am on menu page
    #When I see 10 products in the menu page 
    When I open a category called "Starters"
    Then I select a product called "Veggie Sandwich"
Scenario: Add to cart
       Given I am on the customization page with title: "Veggie Sandwich."
        When I add the product to my cart
        Then I go to cart page
Scenario: Place orders
        Given I am on cart details page with title: "Cart Details"
        When I place the order
        Then I land on the login page
Scenario:login as Guest
        Given I am on login page with title: "Sign In"
        When I place the order with "Guest" login
        Then I go on Order Type page
Scenario:back button of order type page
        Given I am on order type page with title
        When I go back from order type page
        Then I again place the order 
        And I select "dinein" order type
Scenario:verifying track order page
      Given I am on track order page with title: "Track Order"
       When I verify in order tracking
         | Payment Status | Pending |
         | Status | In-Approval  |
         | Table | ----   |
         | Number Of Suborders | 1 |
         |   grandTotal | ₹31.80        |
      Then I can see the Live Update title
     # And I verify grand total
      | ₹31.80  |
Scenario:increasing the quantity
     Given I am on track order page with title: "Track Order"
     When I add 1 quantity of item at 1
     Then I decrease 1 quantity of item at 1
      And verify the image with quantity
Scenario:Notes
     Given I am on track order page with title: "Track Order"
     When I click on Notes
     Then dialog box appears with add notes to item
     And I can see the title Notes
Scenario:Closing Notes
     Given I am on track order page with title: "Track Order"  
     When I close the notes   
     Then Notes dialog box disappears and I am on track order page
Scenario:Writing notes
     Given I am on track order page with title: "Track Order" 
     When I click on Notes 
     Then I write notes: "Sandwitch is tasty"
     And I save the notes
Scenario:Cancelling Notes
    Given I am on track order page with title: "Track Order"
    When I click on Notes
    Then I cancel notes
Scenario:customization
     Given I am on track order page with title: "Track Order"
     When I click on customization icon
     Then Dialog box appears with different ingredient
     And I can see the title Customized Ingredients
Scenario:customized closing
     Given I am on track order page with title: "Track Order"
        When I close the customization dialog box 
        Then I am on track order page
Scenario:selecting Ingredients
 Given I am on track order page with title: "Track Order"
 When I click on customization icon
 #Then I add customizations in the customizations page
            | Cheese | ₹5.00 |
             | Butter    | ₹10.00 |
            | Panner | ₹12.00 |
   And I save the customized ingredient
Scenario:cancelling customized ingredient
    Given I am on track order page with title: "Track Order"
    When I click on customization icon
    Then I cancel the customized ingredient
Scenario:placing second order
    #Given I am on menu page
   Given I add item from track order
    When I open a category called "Starters"
    Then I select a product called "Veggie Bowl"
Scenario:second order
    Given I am on the customization page with title: "Veggie Bowl"
    When I add the product to my cart
   Then I go to cart page
   And I place the order
Scenario:verifying track order page and cancel modify order
    Given I am on track order page with title: "Track Order"
    When I verify in order tracking
         | Payment Status | Pending |
         | Status | Open |
         | Table | ----   |
         | Number Of Suborders | 2 |
Scenario:verifying track order page for second product
   Given I am on track order page with title: "Track Order"
   When I click on pay
   Then I verify payment details for second product
      | grandTotal | ₹48.76          |
      | noOfItems  | 2               |
      | totalQty   | 2               |
      | subTotal   | ₹46.00          |
      | SGST       |                 |
      | SGST       |                 |
      | CGST%      |                 |
      | CGST%      |                 |
      | tax%       | 6%              |
      | tax        | ₹2.76           |
      | tip        | ₹0.00           |
    #And I go to the payment options
Scenario:back button on make payment page
     When I am on Make payment page
     Then I go back on track order page
     And I am on track order page with title: "Track Order"
Scenario:Again coming on make payment page
     Given I am on track order page with title: "Track Order"
     When I click on pay
     Then I am on Make payment page
Scenario:checking cancel button
      When I cancel the payment
      Then I am on track order page with title: "Track Order"
      And I click on pay
Scenario:Make payment
       #When I go back to track order
       #Then I click on pay
       When I go to the payment options
Scenario:mode of payment
    Given I am on payment options page with title: "Mode of Payment"
    When I verify total amount to be "₹52.90" and select "payAtCounter"
    Then I verify in order tracking  
         | Payment Status | Pay At Counter |
         | Status | Open |
         | Table | ----   |
         | Number Of Suborders | 2 |
     And I verify order in database     