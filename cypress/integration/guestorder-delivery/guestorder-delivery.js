import { Given, Before, Then, And, When } from "cypress-cucumber-preprocessor/steps";
import * as merchant from '../../fixtures/M1_Prakash_Parvat_merchant.json'
import * as settings from "../../fixtures/settings.json";
import * as _ from 'lodash';
let userId = ""
Given('I open guest order menu page', () => {
  cy.task("initFirebase", settings.firebase_service_account);
  cy.visit('/mIndex/M1_Prakash_Parvat_Test?language=en');
  cy.contains('Prakash', { timeout: 30000 });
  cy.url().should('include', '/M1_Prakash_Parvat_Test');
  let url = cy.url().toString();
  userId = url.substr(url.indexOf('user_id=') + 8, 28);
});
Given('I am on menu page', () => {
  // cy.visit('/mIndex/M1_Prakash_Parvat_Test?language=en');

  cy.contains('Prakash', { timeout: 30000 });
  cy.url().should('include', '/M1_Prakash_Parvat_Test');
});
// Given('I am on the customization page', () => {
//   cy.visit('/mIndex/M1_Prakash_Parvat_Test?language=en');
//   cy.contains('Prakash', { timeout: 30000 });
//   cy.url().should('include', '/M1_Prakash_Parvat_Test');
// });
// Given('I am on cart details page', () => {
//   cy.visit('/mIndex/M1_Prakash_Parvat_Test?language=en');
//   cy.contains('Prakash', { timeout: 30000 });
//   cy.url().should('include', '/M1_Prakash_Parvat_Test');
// });
// Given('I see three option', () => {
//   cy.visit('/mIndex/M1_Prakash_Parvat_Test?language=en');
//   cy.contains('Prakash', { timeout: 30000 });
//   cy.url().should('include', '/M1_Prakash_Parvat_Test');
// });
// Given('I am on the user details page', () => {
//   cy.visit('/mIndex/M1_Prakash_Parvat_Test?language=en');
//   cy.contains('Prakash', { timeout: 30000 });
//   cy.url().should('include', '/M1_Prakash_Parvat_Test');
// });
When('I open a category called {string}', async (category) => {
  cy.contains(category).click();
});
And('I select a product called {string}', async (name) => {
  cy.wait(4000);
  cy.contains(name).click();
});
When('I see {int} products in the menu page', async (number) => {
  cy.get('[data-cy=menu-product]').should('have.length', number);
});
Then('I add {string} to cart', (item) => {
  cy.contains(item).click();
  cy.scrollTo(0, 500);
  cy.wait(2000);
  cy.get('[data-cy=customization-add-to-cart]').click();
  cy.wait(2000);
});

When('I place the order with {string} login', (login) => {
  let loginButtonIds = {
    Guest: 'guest-login-with-guest'
  };
  cy.wait(2000);
  cy.get('[data-cy=guest-header-cart]').click({ force: true });
  cy.get('[data-cy=cart-place-order]').click();
  cy.wait(3000);
  cy.get(`[data-cy=${loginButtonIds[login]}]`).click();
});

Then('I select {string} order type', (orderType) => {
  let orderTypeIds = {
    delivery: 'delivery-button'
  }
  cy.get(`[data-cy=${orderTypeIds[orderType]}]`).click();
});
When('I submit my user details for delivery name', (table) => {
  const delivery = table.rowsHash();
  cy.get('[data-cy=user-details-name]').type(delivery["Name"]);
  // cy.get('[data-cy=user-details-contact]').type(delivery["Contact"]);
  // cy.get('[data-cy=user-details-address]').type(delivery["Address"]);
  // cy.get('.p-dropdown').click()
  // cy.get('.p-dropdown-item').eq(0).click()
  // cy.get('.p-dropdown').click()
  // cy.get('[data-cy=user-details-city]').type(delivery["city"]);
  cy.contains('Submit').click();
});
When('I submit my user details for delivery', (table) => {
  const delivery = table.rowsHash();
  cy.get('[data-cy=user-details-name]').type(delivery["Name"]);
  cy.get('[data-cy=user-details-contact]').type(delivery["Contact"]);
  cy.get('[data-cy=user-details-address]').type(delivery["Address"]);
  cy.get('.p-dropdown').click()
  cy.get('.p-dropdown-item').eq(0).click()
  cy.get('.p-dropdown').click()
  cy.get('[data-cy=user-details-city]').type(delivery["city"]);
  cy.contains('Submit').click();
});
// When('I submit user details', (table) => {
//   const delivery = table.rowsHash();
//   // cy.get('[data-cy=user-details-name]').type(delivery["Name"]);
//   // cy.get('[data-cy=user-details-contact]').type(delivery["Contact"]);
//   // cy.get('[data-cy=user-details-address]').type(delivery["Address"]);
//   // cy.get('.p-dropdown').click()
//   // cy.get('.p-dropdown-item').eq(0).click()
//   // cy.get('.p-dropdown').click()
//   // cy.get('[data-cy=user-details-city]').type(delivery["city"]);
//   cy.contains('Submit').click();
// });
When('I submit my user details for zipcode', (table) => {
  const delivery = table.rowsHash();
  // cy.get('[data-cy=user-details-name]').type(delivery["Name"]);
  // cy.get('[data-cy=user-details-contact]').type(delivery["Contact"]);
  // cy.get('[data-cy=user-details-address]').type(delivery["Address"]);
  cy.get('.p-dropdown').click()
  cy.get('.p-dropdown-item').eq(0).click()
  cy.get('.p-dropdown').click()
  // cy.get('[data-cy=user-details-city]').type(delivery["city"]);
  cy.contains('Submit').click();
});
When('I submit my user details', (table) => {
  const delivery = table.rowsHash();
  // cy.get('[data-cy=user-details-name]').type(delivery["Name"]);
  cy.get('[data-cy=user-details-contact]').type(delivery["Contact"]);
  // cy.get('[data-cy=user-details-address]').type(delivery["Address"]);
  // cy.get('.p-dropdown').click()
  // cy.get('.p-dropdown-item').eq(0).click()
  // cy.get('.p-dropdown').click()
  // cy.get('[data-cy=user-details-city]').type(delivery["city"]);
  cy.contains('Submit').click();
});

When('I submit my user details for address', (table) => {
  const delivery = table.rowsHash();
  // cy.get('[data-cy=user-details-name]').type(delivery["Name"]);
  // cy.get('[data-cy=user-details-contact]').type(delivery["Contact"]);
  cy.get('[data-cy=user-details-address]').type(delivery["Address"]);
  // cy.get('.p-dropdown').click()
  // cy.get('.p-dropdown-item').eq(0).click()
  // cy.get('.p-dropdown').click()
  // cy.get('[data-cy=user-details-city]').type(delivery["city"]);
  cy.contains('Submit').click();
});
Then('I submit my user details for city', (table) => {
  const delivery = table.rowsHash();
  // cy.get('[data-cy=user-details-name]').type(delivery["Name"]);
  // cy.get('[data-cy=user-details-contact]').type(delivery["Contact"]);
  // cy.get('[data-cy=user-details-address]').type(delivery["Address"]);
  // cy.get('.p-dropdown').click()
  // cy.get('.p-dropdown-item').eq(0).click()
  // cy.get('.p-dropdown').click()
  cy.get('[data-cy=user-details-city]').type(delivery["city"]);
  cy.contains('Submit').click();
});
And('I verify in order tracking', async (table) => {
  const expected = table.rowsHash();
  if (expected["Payment Status"]) {
    cy.get('[data-cy=order-tracking-payment-status]').then(element => {
      expect(element.text().trim()).to.equal(expected['Payment Status'])
    });
  }
  Then('I add a quantity of {int} in the cart order page', (quantity) => {
    for (let i = 0; i <= quantity - 1; i++) {
      cy.get('[data-cy=item-quantity-add]').eq(0).click();
    };
  });
  if (expect["Status"]) {
    cy.get('[data-cy=order-tracking-status]').then(element => {
      expect(element.text().trim()).to.equal(expected['Status'])
    });
  }
  if (expected["Table"]) {
    cy.get('[data-cy=order-tracking-table]').then(element => {
      expect(element.text().trim()).to.equal(expected['Table'])
    });
  }
  if (expected["Number Of Suborders"]) {
    cy.get('[data-cy=order-tracking-product]').should('have.length', expected['Number Of Suborders']);
  }
});
When('I submit user details', () => {
  cy.get('[data-cy=user-details-submit]').click();
});
Then('I go to cart page by clicking on cart', () => {
  cy.get('[data-cy=guest-header-cart]').click();
});
And('I go to the track order', () => {
  cy.get('[data-cy=order-tracking-make-payment]').click();
});
Then('I submit user details', () => {
  cy.get('[data-cy=user-details-submit]').click();
});
Then('I submit user details', () => {
  cy.get('[data-cy=user-details-submit]').click();
});
Given('I am on payment details page with title: {string}', (title) => {
  cy.get('[data-cy=payment-details-title]').then(el => {
    expect(el.text().trim()).to.equal(title);
  });
});
Given('I am on the customization page with title: {string}', (title) => {
  cy.wait(2000);
  cy.get('[data-cy=guest-customization-product-name]').then(el => {
    expect(el.text().trim()).to.equal(title);
  });
});
Given('I am on cart details page with title: {string}', (title) => {
  cy.wait(2000);
  cy.get('[data-cy=order-cart-details]').then(el => {
    expect(el.text().trim()).to.equal(title);
  });
});
Given('I am on the user details page with title: {string}', (title) => {
  cy.wait(2000);
  cy.get('[data-cy=order-users-details]').then(el => {
    expect(el.text().trim()).to.equal(title);
  });
});
Given('I am on the track order page with title: {string}', (title) => {
  cy.wait(2000);
  cy.get('[data-cy=track-order-title]').then(el => {
    expect(el.text().trim()).to.equal(title);
  });
});
Given('I am on login page with title: {string}', (title) => {
  cy.wait(2000);
  cy.get('[data-cy=login-sign-in]').then(el => {
    expect(el.text().trim()).to.equal(title);
  });
});
Then('I verify in order tracking', async (table) => {
  const expected = table.rowsHash();
  if (expected["Payment Status"]) {
    cy.get('[data-cy=order-tracking-payment-status]').then(element => {
      expect(element.text().trim()).to.equal(expected['Payment Status'])
    });
  }
  if (expect["Status"]) {
    cy.get('[data-cy=order-tracking-status]').then(element => {
      expect(element.text().trim()).to.equal(expected['Status'])
    });
  }
  if (expected["Table"]) {
    cy.get('[data-cy=order-tracking-table]').then(element => {
      expect(element.text().trim()).to.equal(expected['Table'])
    });
  }
  if (expected["Number Of Suborders"]) {
    cy.get('[data-cy=order-tracking-product]').should('have.length', expected['Number Of Suborders']);
  }
});
And('I clear address', () => {
  cy.get('[data-cy=user-details-address]').clear()
});
Then('name failed to submit', () => {
  cy.get('[data-cy=invalid-contact]').click();
});
Then('page failed to submit', () => {
  cy.get('[data-cy=invalid-contact]').click();
});
Then('contact failed to submit', () => {
  cy.get('[data-cy=invalid-address]').click();
});
Then('address failed to submit', () => {
  cy.get('[data-cy=invalid-zipcode]').click();
});
Then('I verify payment details for second product', (table) => {
  let data = table.rowsHash();
  cy.get('[data-cy=payment-details-token]').then(el => {
    expect(el.text().trim().length).to.equal(5);
  });
  And('I go the payment options', () => {
    cy.get('[data-cy=payment-details-make-payment]').click();
  });
  cy.get('[data-cy=payment-details-grand-total]').then(el => {
    expect(el.text().trim()).to.equal(data['grandTotal']);
  });
  cy.get('[data-cy=payment-details-total-qty]').then(el => {
    expect(el.text().trim()).to.equal(data['totalQty']);
  });
  cy.get('[data-cy=payment-details-subtotal]').then(el => {
    expect(el.text().trim()).to.equal(data['subTotal']);
  });
  cy.get('[data-cy=payment-details-tip]').then(el => {
    expect(el.text().trim()).to.equal(data['tip']);
  });
  if (data['SGST']) {
    cy.get('[data-cy=payment-details-sgst]').then(el => {
      expect(el.text().trim()).to.equal(data['SGST']);
    });
    cy.get('[data-cy=payment-details-sgst%]').then(el => {
      expect(el.text().trim()).to.equal(`SGST ${data['sgst%']} On ${data['subTotal']}`);
    });
  }
  if (data['CGST']) {
    cy.get('[data-cy=payment-details-cgst%]').then(el => {
      expect(el.text().trim()).to.equal(`CGST ${data['cgst%']} On ${data['subTotal']}`);
    });
    cy.get('[data-cy=payment-details-cgst]').then(el => {
      expect(el.text().trim()).to.equal(data['CGST']);
    });
  }
  if (data['tax']) {
    cy.get('[data-cy=payment-details-tax]').then(el => {
      expect(el.text().trim()).to.equal(data['tax']);
    });
    cy.get('[data-cy=payment-details-tax-label]').then(el => {
      expect(el.text().trim()).to.equal(`Tax ${data['tax%']} On ${data['subTotal']}`);
    });
    // }
    // for (let i = 0; i < +data['noOfItems']; i++) {
    //   cy.get('[data-cy=payment-details-item-name]').eq(0).then(el => {
    //     let item = data['items'].split(',')[i];
    //     expect(el.text().trim()).to.equal(item + '.');
    //   });

    // cy.get('[data-cy=payment-details-item-price]').eq(0).then(el => {
    //   let itemPrice = data['itemPrice'].split(',');
    //   expect(el.text().trim()).to.equal(itemPrice);
    // });

    // cy.get('[data-cy=payment-details-item-qty]').eq(0).then(el => {
    //   let itemQty = data['itemQty'].split(',')[i];
    //   expect(el.text().trim()).to.equal(itemQty);
    // });
  }
});
Then('zipcode failed to submit', () => {
  cy.get('[data-cy=invalid-city]').click();
});
And('city failed to submit', () => {
  cy.get('[data-cy=invalid-address]').click();
});
And('I click on pay', () => {
  cy.get('[data-cy=order-tracking-make-payment]').click();
});
And('I cancel modify order', () => {
  cy.get('[data-cy=cart-cancel]').click();
});
Then('I modify order', () => {
  cy.get('[data-cy=order-tracking-modify-order]').click();
});
When('I modify order by increasing quantity', () => {
  cy.get('[data-cy=item-quantity-add]').click();
});
Then('I modify order', () => {
  cy.get('[data-cy=order-tracking-modify-order]').click();
});
When('I verify payment details', (table) => {
  let data = table.rowsHash();
  cy.get('[data-cy=payment-details-token]').then(el => {
    expect(el.text().trim().length).to.equal(5);
  });
  cy.get('[data-cy=payment-details-grand-total]').then(el => {
    expect(el.text().trim()).to.equal(data['grandTotal']);
  });
  cy.get('[data-cy=payment-details-total-qty]').then(el => {
    expect(el.text().trim()).to.equal(data['totalQty']);
  });
  cy.get('[data-cy=payment-details-subtotal]').then(el => {
    expect(el.text().trim()).to.equal(data['subTotal']);
  });
  cy.get('[data-cy=payment-details-tip]').then(el => {
    expect(el.text().trim()).to.equal(data['tip']);
  });
  if (data['SGST']) {
    cy.get('[data-cy=payment-details-sgst]').then(el => {
      expect(el.text().trim()).to.equal(data['SGST']);
    });
    cy.get('[data-cy=payment-details-sgst%]').then(el => {
      expect(el.text().trim()).to.equal(`SGST ${data['sgst%']} On ${data['subTotal']}`);
    });
  }
  if (data['CGST']) {
    cy.get('[data-cy=payment-details-cgst%]').then(el => {
      expect(el.text().trim()).to.equal(`CGST ${data['cgst%']} On ${data['subTotal']}`);
    });
    cy.get('[data-cy=payment-details-cgst]').then(el => {
      expect(el.text().trim()).to.equal(data['CGST']);
    });
  }
  if (data['tax']) {
    cy.get('[data-cy=payment-details-tax]').then(el => {
      expect(el.text().trim()).to.equal(data['tax']);
    });
    cy.get('[data-cy=payment-details-tax-label]').then(el => {
      expect(el.text().trim()).to.equal(`Tax ${data['tax%']} On ${data['subTotal']}`);
    });
  }
  for (let i = 0; i < +data['noOfItems']; i++) {
    cy.get('[data-cy=payment-details-item-name]').eq(0).then(el => {
      let item = data['items'].split(',')[i];
      expect(el.text().trim()).to.equal(item + '.');
    });

    cy.get('[data-cy=payment-details-item-price]').eq(0).then(el => {
      let itemPrice = data['itemPrice'].split(',')[i];
      expect(el.text().trim()).to.equal(itemPrice);
    });

    cy.get('[data-cy=payment-details-item-qty]').eq(0).then(el => {
      let itemQty = data['itemQty'].split(',')[i];
      expect(el.text().trim()).to.equal(itemQty);
    });
  }
});
Then('I go to cart page', () => {
  cy.get('[data-cy=footer-cart]').click();
});

Then('I go the payment options', () => {
  cy.get('[data-cy=payment-details-make-payment]').click();
});

Given('I am on payment options page with title: {string}', (title) => {
  cy.get('[data-cy=payment-options-title]').then(el => {
    expect(el.text().trim()).to.equal(title);
  });
});

When('I verify total amount to be {string} and select {string}', (total, type) => {
  let paymentOptionsIds = {
    CashOndelivery: 'payment-options-cash-on-delivery'
  };
  // // cy.get(`[data-cy=payment-options-grand-total]`).then(el => {
  // //   expect(el.text().trim()).to.equal(total);
  // // });
  cy.get(`[data-cy=${paymentOptionsIds['CashOndelivery']}]`).click();
  cy.get('[data-cy=payment-options-make-payment]').click();
});


Given('I am on track order page with title: {string}', (title) => {
  cy.get('[data-cy=track-order-title]').then(el => {
    expect(el.text().trim()).to.equal(title);
  });
});

When('I verify the order in track order', (table) => {
  let data = table.rowsHash();
  cy.wait(1000);
  cy.get(`[data-cy=track-order-token]`).then(el => {
    expect(el.text().trim().length).to.equal(5);
  });
  cy.get(`[data-cy=order-tracking-grand-total]`).then(el => {
    expect(el.text().trim()).to.equal(data['grandTotal']);
  });
  cy.get(`[data-cy=order-tracking-payment-status]`).then(el => {
    expect(el.text().trim()).to.equal(data['paymentStatus']);
  });
  cy.get(`[data-cy=order-tracking-status]`).then(el => {
    expect(el.text().trim()).to.equal(data['status']);
  });
  // cy.get(`[data-cy=order-tracking-table]`).then(el => {
  //   expect(el.text().trim()).to.equal(data['table']);
  // });
  for (let i = 0; i < +data['noOfItems']; i++) {
    cy.get(`[data-cy=order-tracking-name]`).then(el => {
      let item = data['items'].split(',')[i];
      expect(el.text().trim()).to.equal(item + '.');
    });
    cy.get(`[data-cy=order-tracking-suborder-status]`).children().eq(0).children().eq(0).then(el => {
      let item = data['suborderStatus'].split(',')[i];
      expect(el.text().trim()).to.equal(item);
    });
    cy.get(`[data-cy=order-tracking-price]`).then(el => {
      let price = data['itemPrice'].split(',')[i];
      expect(el.text().trim()).to.equal(price);
    });
    cy.get(`[data-cy=order-tracking-item-qty-badge]`).children().eq(0).then(el => {
      let itemQty = data['itemQty'].split(',')[i];
      expect(el.text().trim()).to.equal(itemQty);
    });
    cy.get('[data-cy=track-order-qty-spinner]').eq(i - 1).children().eq(0).children().eq(0).children().eq(1).then(el => {
      let itemQty = data['itemQty'].split(',')[i];
      expect(el.text().trim()).to.equal(itemQty);
    });
  }
});
When('I place the order', () => {
  cy.get('[data-cy=cart-place-order]').click({ force: true });
});
And('I place the order', () => {
  cy.get('[data-cy=cart-place-order]').click({ force: true });
});
Then('I land on the login page', async () => {
  cy.url().should('include', '/login');
  cy.get('[data-cy=guest-login-option]').should('have.length', 2);
});
When('I add the product to my cart', () => {
  cy.wait(5000);
  cy.get('[data-cy=customization-add-to-cart]').click();
});

Then('I go to cart page', () => {
  cy.get('[data-cy=footer-cart]').click();
});
Then('I go on track order page', () => {
  cy.get('[data-cy=cart-place-order]').click();
});
And('I add item from track order', () => {
  cy.get('[data-cy=order-tracking-add-item]').click();
});
// Then('I land on track order page', async () => {
//   cy.url().should('include', '/login');
//   cy.get('[data-cy=guest-login-option]').should('have.length', 2);
// });
And('I verify order in database', () => {
  cy.wait(4000);
  cy.url().then(url => {
    let startI = url.indexOf("order_id=") + 9;
    let endI = url.indexOf("&");
    let id = `${url.substring(startI, endI)}`; 
    // console.log('.............................',startI, endI, id);
    cy.task("getOrderById", { merchantId: "M1_Prakash_Parvat_Test", orderId: id, url: typeof url }).then(order => {
      expect(order.status).to.equal(1);
      expect(order.paymentStatus).to.equal(8);
      expect(order.table).to.equal();
      expect(order.numberofsuborders).to.equal();
      expect(order.grandTotal).to.equal();
      expect(order.noOfItems).to.equal();
      expect(order.totalQuantity).to.equal(3);
      expect(order.subTotal).to.equal();
      expect(order.tip).to.equal(0);
      expect(order.Contact).to.equal();
      expect(order.city).to.equal( );
      expect(order.Address).to.equal();
      expect(order.zipcode).to.equal();
      expect(order.userDetails.name).to.equal('Riya');
    })
  })
});
import { Given, Before, Then, And, When } from "cypress-cucumber-preprocessor/steps";
import * as merchant from '../../fixtures/M1_Prakash_Parvat_merchant.json'
import * as settings from "../../fixtures/settings.json";
import * as _ from 'lodash';
let userId = ""
Given('I open guest order menu page', () => {
  cy.task("initFirebase", settings.firebase_service_account);
  cy.visit('/mIndex/M1_Prakash_Parvat_Test?language=en');
  cy.contains('Prakash', { timeout: 30000 });
  cy.url().should('include', '/M1_Prakash_Parvat_Test');
  let url = cy.url().toString();
  userId = url.substr(url.indexOf('user_id=') + 8, 28);
});
Given('I am on menu page', () => {
  // cy.visit('/mIndex/M1_Prakash_Parvat_Test?language=en');

  cy.contains('Prakash', { timeout: 30000 });
  cy.url().should('include', '/M1_Prakash_Parvat_Test');
});
// Given('I am on the customization page', () => {
//   cy.visit('/mIndex/M1_Prakash_Parvat_Test?language=en');
//   cy.contains('Prakash', { timeout: 30000 });
//   cy.url().should('include', '/M1_Prakash_Parvat_Test');
// });
// Given('I am on cart details page', () => {
//   cy.visit('/mIndex/M1_Prakash_Parvat_Test?language=en');
//   cy.contains('Prakash', { timeout: 30000 });
//   cy.url().should('include', '/M1_Prakash_Parvat_Test');
// });
// Given('I see three option', () => {
//   cy.visit('/mIndex/M1_Prakash_Parvat_Test?language=en');
//   cy.contains('Prakash', { timeout: 30000 });
//   cy.url().should('include', '/M1_Prakash_Parvat_Test');
// });
// Given('I am on the user details page', () => {
//   cy.visit('/mIndex/M1_Prakash_Parvat_Test?language=en');
//   cy.contains('Prakash', { timeout: 30000 });
//   cy.url().should('include', '/M1_Prakash_Parvat_Test');
// });
When('I open a category called {string}', async (category) => {
  cy.contains(category).click();
});
And('I select a product called {string}', async (name) => {
  cy.wait(4000);
  cy.contains(name).click();
});
When('I see {int} products in the menu page', async (number) => {
  cy.get('[data-cy=menu-product]').should('have.length', number);
});
Then('I add {string} to cart', (item) => {
  cy.contains(item).click();
  cy.scrollTo(0, 500);
  cy.wait(2000);
  cy.get('[data-cy=customization-add-to-cart]').click();
  cy.wait(2000);
});

When('I place the order with {string} login', (login) => {
  let loginButtonIds = {
    Guest: 'guest-login-with-guest'
  };
  cy.wait(2000);
  cy.get('[data-cy=guest-header-cart]').click({ force: true });
  cy.get('[data-cy=cart-place-order]').click();
  cy.wait(3000);
  cy.get(`[data-cy=${loginButtonIds[login]}]`).click();
});

Then('I select {string} order type', (orderType) => {
  let orderTypeIds = {
    delivery: 'delivery-button'
  }
  cy.get(`[data-cy=${orderTypeIds[orderType]}]`).click();
});
When('I submit my user details for delivery name', (table) => {
  const delivery = table.rowsHash();
  cy.get('[data-cy=user-details-name]').type(delivery["Name"]);
  // cy.get('[data-cy=user-details-contact]').type(delivery["Contact"]);
  // cy.get('[data-cy=user-details-address]').type(delivery["Address"]);
  // cy.get('.p-dropdown').click()
  // cy.get('.p-dropdown-item').eq(0).click()
  // cy.get('.p-dropdown').click()
  // cy.get('[data-cy=user-details-city]').type(delivery["city"]);
  cy.contains('Submit').click();
});
When('I submit my user details for delivery', (table) => {
  const delivery = table.rowsHash();
  cy.get('[data-cy=user-details-name]').type(delivery["Name"]);
  cy.get('[data-cy=user-details-contact]').type(delivery["Contact"]);
  cy.get('[data-cy=user-details-address]').type(delivery["Address"]);
  cy.get('.p-dropdown').click()
  cy.get('.p-dropdown-item').eq(0).click()
  cy.get('.p-dropdown').click()
  cy.get('[data-cy=user-details-city]').type(delivery["city"]);
  cy.contains('Submit').click();
});
// When('I submit user details', (table) => {
//   const delivery = table.rowsHash();
//   // cy.get('[data-cy=user-details-name]').type(delivery["Name"]);
//   // cy.get('[data-cy=user-details-contact]').type(delivery["Contact"]);
//   // cy.get('[data-cy=user-details-address]').type(delivery["Address"]);
//   // cy.get('.p-dropdown').click()
//   // cy.get('.p-dropdown-item').eq(0).click()
//   // cy.get('.p-dropdown').click()
//   // cy.get('[data-cy=user-details-city]').type(delivery["city"]);
//   cy.contains('Submit').click();
// });
When('I submit my user details for zipcode', (table) => {
  const delivery = table.rowsHash();
  // cy.get('[data-cy=user-details-name]').type(delivery["Name"]);
  // cy.get('[data-cy=user-details-contact]').type(delivery["Contact"]);
  // cy.get('[data-cy=user-details-address]').type(delivery["Address"]);
  cy.get('.p-dropdown').click()
  cy.get('.p-dropdown-item').eq(0).click()
  cy.get('.p-dropdown').click()
  // cy.get('[data-cy=user-details-city]').type(delivery["city"]);
  cy.contains('Submit').click();
});
When('I submit my user details', (table) => {
  const delivery = table.rowsHash();
  // cy.get('[data-cy=user-details-name]').type(delivery["Name"]);
  cy.get('[data-cy=user-details-contact]').type(delivery["Contact"]);
  // cy.get('[data-cy=user-details-address]').type(delivery["Address"]);
  // cy.get('.p-dropdown').click()
  // cy.get('.p-dropdown-item').eq(0).click()
  // cy.get('.p-dropdown').click()
  // cy.get('[data-cy=user-details-city]').type(delivery["city"]);
  cy.contains('Submit').click();
});

When('I submit my user details for address', (table) => {
  const delivery = table.rowsHash();
  // cy.get('[data-cy=user-details-name]').type(delivery["Name"]);
  // cy.get('[data-cy=user-details-contact]').type(delivery["Contact"]);
  cy.get('[data-cy=user-details-address]').type(delivery["Address"]);
  // cy.get('.p-dropdown').click()
  // cy.get('.p-dropdown-item').eq(0).click()
  // cy.get('.p-dropdown').click()
  // cy.get('[data-cy=user-details-city]').type(delivery["city"]);
  cy.contains('Submit').click();
});
Then('I submit my user details for city', (table) => {
  const delivery = table.rowsHash();
  // cy.get('[data-cy=user-details-name]').type(delivery["Name"]);
  // cy.get('[data-cy=user-details-contact]').type(delivery["Contact"]);
  // cy.get('[data-cy=user-details-address]').type(delivery["Address"]);
  // cy.get('.p-dropdown').click()
  // cy.get('.p-dropdown-item').eq(0).click()
  // cy.get('.p-dropdown').click()
  cy.get('[data-cy=user-details-city]').type(delivery["city"]);
  cy.contains('Submit').click();
});
And('I verify in order tracking', async (table) => {
  const expected = table.rowsHash();
  if (expected["Payment Status"]) {
    cy.get('[data-cy=order-tracking-payment-status]').then(element => {
      expect(element.text().trim()).to.equal(expected['Payment Status'])
    });
  }
  Then('I add a quantity of {int} in the cart order page', (quantity) => {
    for (let i = 0; i <= quantity - 1; i++) {
      cy.get('[data-cy=item-quantity-add]').eq(0).click();
    };
  });
  if (expect["Status"]) {
    cy.get('[data-cy=order-tracking-status]').then(element => {
      expect(element.text().trim()).to.equal(expected['Status'])
    });
  }
  if (expected["Table"]) {
    cy.get('[data-cy=order-tracking-table]').then(element => {
      expect(element.text().trim()).to.equal(expected['Table'])
    });
  }
  if (expected["Number Of Suborders"]) {
    cy.get('[data-cy=order-tracking-product]').should('have.length', expected['Number Of Suborders']);
  }
});
When('I submit user details', () => {
  cy.get('[data-cy=user-details-submit]').click();
});
Then('I go to cart page by clicking on cart', () => {
  cy.get('[data-cy=guest-header-cart]').click();
});
And('I go to the track order', () => {
  cy.get('[data-cy=order-tracking-make-payment]').click();
});
Then('I submit user details', () => {
  cy.get('[data-cy=user-details-submit]').click();
});
Then('I submit user details', () => {
  cy.get('[data-cy=user-details-submit]').click();
});
Given('I am on payment details page with title: {string}', (title) => {
  cy.get('[data-cy=payment-details-title]').then(el => {
    expect(el.text().trim()).to.equal(title);
  });
});
Given('I am on the customization page with title: {string}', (title) => {
  cy.wait(2000);
  cy.get('[data-cy=guest-customization-product-name]').then(el => {
    expect(el.text().trim()).to.equal(title);
  });
});
Given('I am on cart details page with title: {string}', (title) => {
  cy.wait(2000);
  cy.get('[data-cy=order-cart-details]').then(el => {
    expect(el.text().trim()).to.equal(title);
  });
});
Given('I am on the user details page with title: {string}', (title) => {
  cy.wait(2000);
  cy.get('[data-cy=order-users-details]').then(el => {
    expect(el.text().trim()).to.equal(title);
  });
});
Given('I am on the track order page with title: {string}', (title) => {
  cy.wait(2000);
  cy.get('[data-cy=track-order-title]').then(el => {
    expect(el.text().trim()).to.equal(title);
  });
});
Given('I am on login page with title: {string}', (title) => {
  cy.wait(2000);
  cy.get('[data-cy=login-sign-in]').then(el => {
    expect(el.text().trim()).to.equal(title);
  });
});
Then('I verify in order tracking', async (table) => {
  const expected = table.rowsHash();
  if (expected["Payment Status"]) {
    cy.get('[data-cy=order-tracking-payment-status]').then(element => {
      expect(element.text().trim()).to.equal(expected['Payment Status'])
    });
  }
  if (expect["Status"]) {
    cy.get('[data-cy=order-tracking-status]').then(element => {
      expect(element.text().trim()).to.equal(expected['Status'])
    });
  }
  if (expected["Table"]) {
    cy.get('[data-cy=order-tracking-table]').then(element => {
      expect(element.text().trim()).to.equal(expected['Table'])
    });
  }
  if (expected["Number Of Suborders"]) {
    cy.get('[data-cy=order-tracking-product]').should('have.length', expected['Number Of Suborders']);
  }
});
And('I clear address', () => {
  cy.get('[data-cy=user-details-address]').clear()
});
Then('name failed to submit', () => {
  cy.get('[data-cy=invalid-contact]').click();
});
Then('page failed to submit', () => {
  cy.get('[data-cy=invalid-contact]').click();
});
Then('contact failed to submit', () => {
  cy.get('[data-cy=invalid-address]').click();
});
Then('address failed to submit', () => {
  cy.get('[data-cy=invalid-zipcode]').click();
});
Then('I verify payment details for second product', (table) => {
  let data = table.rowsHash();
  cy.get('[data-cy=payment-details-token]').then(el => {
    expect(el.text().trim().length).to.equal(5);
  });
  And('I go the payment options', () => {
    cy.get('[data-cy=payment-details-make-payment]').click();
  });
  cy.get('[data-cy=payment-details-grand-total]').then(el => {
    expect(el.text().trim()).to.equal(data['grandTotal']);
  });
  cy.get('[data-cy=payment-details-total-qty]').then(el => {
    expect(el.text().trim()).to.equal(data['totalQty']);
  });
  cy.get('[data-cy=payment-details-subtotal]').then(el => {
    expect(el.text().trim()).to.equal(data['subTotal']);
  });
  cy.get('[data-cy=payment-details-tip]').then(el => {
    expect(el.text().trim()).to.equal(data['tip']);
  });
  if (data['SGST']) {
    cy.get('[data-cy=payment-details-sgst]').then(el => {
      expect(el.text().trim()).to.equal(data['SGST']);
    });
    cy.get('[data-cy=payment-details-sgst%]').then(el => {
      expect(el.text().trim()).to.equal(`SGST ${data['sgst%']} On ${data['subTotal']}`);
    });
  }
  if (data['CGST']) {
    cy.get('[data-cy=payment-details-cgst%]').then(el => {
      expect(el.text().trim()).to.equal(`CGST ${data['cgst%']} On ${data['subTotal']}`);
    });
    cy.get('[data-cy=payment-details-cgst]').then(el => {
      expect(el.text().trim()).to.equal(data['CGST']);
    });
  }
  if (data['tax']) {
    cy.get('[data-cy=payment-details-tax]').then(el => {
      expect(el.text().trim()).to.equal(data['tax']);
    });
    cy.get('[data-cy=payment-details-tax-label]').then(el => {
      expect(el.text().trim()).to.equal(`Tax ${data['tax%']} On ${data['subTotal']}`);
    });
    // }
    // for (let i = 0; i < +data['noOfItems']; i++) {
    //   cy.get('[data-cy=payment-details-item-name]').eq(0).then(el => {
    //     let item = data['items'].split(',')[i];
    //     expect(el.text().trim()).to.equal(item + '.');
    //   });

    // cy.get('[data-cy=payment-details-item-price]').eq(0).then(el => {
    //   let itemPrice = data['itemPrice'].split(',');
    //   expect(el.text().trim()).to.equal(itemPrice);
    // });

    // cy.get('[data-cy=payment-details-item-qty]').eq(0).then(el => {
    //   let itemQty = data['itemQty'].split(',')[i];
    //   expect(el.text().trim()).to.equal(itemQty);
    // });
  }
});
Then('zipcode failed to submit', () => {
  cy.get('[data-cy=invalid-city]').click();
});
And('city failed to submit', () => {
  cy.get('[data-cy=invalid-address]').click();
});
And('I click on pay', () => {
  cy.get('[data-cy=order-tracking-make-payment]').click();
});
And('I cancel modify order', () => {
  cy.get('[data-cy=cart-cancel]').click();
});
Then('I modify order', () => {
  cy.get('[data-cy=order-tracking-modify-order]').click();
});
When('I modify order by increasing quantity', () => {
  cy.get('[data-cy=item-quantity-add]').click();
});
Then('I modify order', () => {
  cy.get('[data-cy=order-tracking-modify-order]').click();
});
When('I verify payment details', (table) => {
  let data = table.rowsHash();
  cy.get('[data-cy=payment-details-token]').then(el => {
    expect(el.text().trim().length).to.equal(5);
  });
  cy.get('[data-cy=payment-details-grand-total]').then(el => {
    expect(el.text().trim()).to.equal(data['grandTotal']);
  });
  cy.get('[data-cy=payment-details-total-qty]').then(el => {
    expect(el.text().trim()).to.equal(data['totalQty']);
  });
  cy.get('[data-cy=payment-details-subtotal]').then(el => {
    expect(el.text().trim()).to.equal(data['subTotal']);
  });
  cy.get('[data-cy=payment-details-tip]').then(el => {
    expect(el.text().trim()).to.equal(data['tip']);
  });
  if (data['SGST']) {
    cy.get('[data-cy=payment-details-sgst]').then(el => {
      expect(el.text().trim()).to.equal(data['SGST']);
    });
    cy.get('[data-cy=payment-details-sgst%]').then(el => {
      expect(el.text().trim()).to.equal(`SGST ${data['sgst%']} On ${data['subTotal']}`);
    });
  }
  if (data['CGST']) {
    cy.get('[data-cy=payment-details-cgst%]').then(el => {
      expect(el.text().trim()).to.equal(`CGST ${data['cgst%']} On ${data['subTotal']}`);
    });
    cy.get('[data-cy=payment-details-cgst]').then(el => {
      expect(el.text().trim()).to.equal(data['CGST']);
    });
  }
  if (data['tax']) {
    cy.get('[data-cy=payment-details-tax]').then(el => {
      expect(el.text().trim()).to.equal(data['tax']);
    });
    cy.get('[data-cy=payment-details-tax-label]').then(el => {
      expect(el.text().trim()).to.equal(`Tax ${data['tax%']} On ${data['subTotal']}`);
    });
  }
  for (let i = 0; i < +data['noOfItems']; i++) {
    cy.get('[data-cy=payment-details-item-name]').eq(0).then(el => {
      let item = data['items'].split(',')[i];
      expect(el.text().trim()).to.equal(item + '.');
    });

    cy.get('[data-cy=payment-details-item-price]').eq(0).then(el => {
      let itemPrice = data['itemPrice'].split(',')[i];
      expect(el.text().trim()).to.equal(itemPrice);
    });

    cy.get('[data-cy=payment-details-item-qty]').eq(0).then(el => {
      let itemQty = data['itemQty'].split(',')[i];
      expect(el.text().trim()).to.equal(itemQty);
    });
  }
});
Then('I go to cart page', () => {
  cy.get('[data-cy=footer-cart]').click();
});

Then('I go the payment options', () => {
  cy.get('[data-cy=payment-details-make-payment]').click();
});

Given('I am on payment options page with title: {string}', (title) => {
  cy.get('[data-cy=payment-options-title]').then(el => {
    expect(el.text().trim()).to.equal(title);
  });
});

When('I verify total amount to be {string} and select {string}', (total, type) => {
  let paymentOptionsIds = {
    CashOndelivery: 'payment-options-cash-on-delivery'
  };
  // // cy.get(`[data-cy=payment-options-grand-total]`).then(el => {
  // //   expect(el.text().trim()).to.equal(total);
  // // });
  cy.get(`[data-cy=${paymentOptionsIds['CashOndelivery']}]`).click();
  cy.get('[data-cy=payment-options-make-payment]').click();
});


Given('I am on track order page with title: {string}', (title) => {
  cy.get('[data-cy=track-order-title]').then(el => {
    expect(el.text().trim()).to.equal(title);
  });
});

When('I verify the order in track order', (table) => {
  let data = table.rowsHash();
  cy.wait(1000);
  cy.get(`[data-cy=track-order-token]`).then(el => {
    expect(el.text().trim().length).to.equal(5);
  });
  cy.get(`[data-cy=order-tracking-grand-total]`).then(el => {
    expect(el.text().trim()).to.equal(data['grandTotal']);
  });
  cy.get(`[data-cy=order-tracking-payment-status]`).then(el => {
    expect(el.text().trim()).to.equal(data['paymentStatus']);
  });
  cy.get(`[data-cy=order-tracking-status]`).then(el => {
    expect(el.text().trim()).to.equal(data['status']);
  });
  // cy.get(`[data-cy=order-tracking-table]`).then(el => {
  //   expect(el.text().trim()).to.equal(data['table']);
  // });
  for (let i = 0; i < +data['noOfItems']; i++) {
    cy.get(`[data-cy=order-tracking-name]`).then(el => {
      let item = data['items'].split(',')[i];
      expect(el.text().trim()).to.equal(item + '.');
    });
    cy.get(`[data-cy=order-tracking-suborder-status]`).children().eq(0).children().eq(0).then(el => {
      let item = data['suborderStatus'].split(',')[i];
      expect(el.text().trim()).to.equal(item);
    });
    cy.get(`[data-cy=order-tracking-price]`).then(el => {
      let price = data['itemPrice'].split(',')[i];
      expect(el.text().trim()).to.equal(price);
    });
    cy.get(`[data-cy=order-tracking-item-qty-badge]`).children().eq(0).then(el => {
      let itemQty = data['itemQty'].split(',')[i];
      expect(el.text().trim()).to.equal(itemQty);
    });
    cy.get('[data-cy=track-order-qty-spinner]').eq(i - 1).children().eq(0).children().eq(0).children().eq(1).then(el => {
      let itemQty = data['itemQty'].split(',')[i];
      expect(el.text().trim()).to.equal(itemQty);
    });
  }
});
When('I place the order', () => {
  cy.get('[data-cy=cart-place-order]').click({ force: true });
});
And('I place the order', () => {
  cy.get('[data-cy=cart-place-order]').click({ force: true });
});
Then('I land on the login page', async () => {
  cy.url().should('include', '/login');
  cy.get('[data-cy=guest-login-option]').should('have.length', 2);
});
When('I add the product to my cart', () => {
  cy.wait(5000);
  cy.get('[data-cy=customization-add-to-cart]').click();
});

Then('I go to cart page', () => {
  cy.get('[data-cy=footer-cart]').click();
});
Then('I go on track order page', () => {
  cy.get('[data-cy=cart-place-order]').click();
});
And('I add item from track order', () => {
  cy.get('[data-cy=order-tracking-add-item]').click();
});
// Then('I land on track order page', async () => {
//   cy.url().should('include', '/login');
//   cy.get('[data-cy=guest-login-option]').should('have.length', 2);
// });
And('I verify order in database', () => {
  cy.wait(4000);
  cy.url().then(url => {
    let startI = url.indexOf("order_id=") + 9;
    let endI = url.indexOf("&");
    let id = `${url.substring(startI, endI)}`; 
    // console.log('.............................',startI, endI, id);
    cy.task("getOrderById", { merchantId: "M1_Prakash_Parvat_Test", orderId: id, url: typeof url }).then(order => {
      expect(order.status).to.equal(1);
      expect(order.paymentStatus).to.equal(8);
      expect(order.table).to.equal();
      expect(order.numberofsuborders).to.equal();
      expect(order.grandTotal).to.equal();
      expect(order.noOfItems).to.equal();
      expect(order.totalQuantity).to.equal(3);
      expect(order.subTotal).to.equal();
      expect(order.tip).to.equal(0);
      expect(order.Contact).to.equal();
      expect(order.city).to.equal( );
      expect(order.Address).to.equal();
      expect(order.zipcode).to.equal();
      expect(order.userDetails.name).to.equal('Riya');
    })
  })
});
