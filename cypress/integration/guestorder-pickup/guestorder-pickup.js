import { Given, Before, Then, And, When } from "cypress-cucumber-preprocessor/steps";
import * as merchant from '../../fixtures/M1_Prakash_Parvat_merchant.json'
import * as settings from "../../fixtures/settings.json";
import * as _ from 'lodash';


Given('I open guest order menu page', () => {
    cy.visit('http://localhost:4200/go/M1_Prakash_Parvat_Test?language');
    cy.contains('Prakash', { timeout: 30000 });
    cy.url().should('include', '/M1_Prakash_Parvat_Test');
  });
  Then('I go on Order Type page', () => {
    cy.get('[data-cy=order-type-header]').click();
  });
  Given('I am on order type page with title', () => {
    cy.get('[data-cy=order-type-header]').click();
  });
  When('I go back from order type page', () => {
    cy.get('[data-cy=order-type-back-button]').click();
    });
  Then('I clear the cache of my application', () => {
    window.indexedDB.deleteDatabase("firebaseLocalStorageDb");
    cy.wait(5000);
});
When('I see {int} products in the menu page', async (number) => {
  cy.get('[data-cy=menu-product]').should('have.length', number);
});
Given('I am on the customization page with title: {string}', (title) => {
  cy.wait(2000);
  cy.get('[data-cy=guest-customization-product-name]').then(el => {
    expect(el.text().trim()).to.equal(title);
  });
});
Given('I am on cart details page with title: {string}', (title) => {
  cy.wait(2000);
  cy.get('[data-cy=order-cart-details]').then(el => {
    expect(el.text().trim()).to.equal(title);
  });
});
Then('I again place the order', () => {
  cy.get('[data-cy=cart-place-order]').click({ force: true });
});
Given('I am on the user details page with title: {string}', (title) => {
  cy.wait(2000);
  cy.get('[data-cy=order-users-details]').then(el => {
    expect(el.text().trim()).to.equal(title);
  });
});
  When('I add {string} to cart', (item) => {
    cy.contains(item).click();
    cy.scrollTo(0, 500);
    cy.wait(1000);
    cy.get('[data-cy=customization-add-to-cart]').click();
    cy.wait(1000);
  });
  Given('I am on menu page', () => {
    cy.visit('http://localhost:4200/go/M1_Prakash_Parvat_Test?language');
    cy.contains('Prakash', { timeout: 30000 });
    cy.url().should('include', '/M1_Prakash_Parvat_Test');
  });
  Then('I place the order with {string} login', (login) => {
    let loginButtonIds = {
      Guest: 'guest-login-with-guest'
    };
    cy.wait(1000);
    cy.get('[data-cy=guest-header-cart]').click({ force: true });
    cy.get('[data-cy=cart-place-order]').click();
    cy.wait(3000);
    cy.get(`[data-cy=${loginButtonIds[login]}]`).click();
  });

  Then('I select {string} order type', (orderType) => {
    let orderTypeIds = {
      pickup: 'pick-up-button'
    }
    cy.get(`[data-cy=${orderTypeIds[orderType]}]`).click();
  });
  And('I select {string} order type', (orderType) => {
    let orderTypeIds = {
      pickup: 'pick-up-button'
    }
    cy.get(`[data-cy=${orderTypeIds[orderType]}]`).click();
  });
  // Then('I select {string} order type', (orderType) => {
  //   let orderTypeIds = {
  //     delivery: 'delivery-button'
  //   }
  //   cy.get(`[data-cy=${orderTypeIds[orderType]}]`).click();
  // });
  Given('I am on payment options page with title: {string}', (title) => {
    cy.get('[data-cy=payment-options-title]').then(el => {
      expect(el.text().trim()).to.equal(title);
    });
  });
  // When('I verify total amount to be {string} and select {string}', (total, type) => {
  //   let paymentOptionsIds = {
  //     cashondelivery: 'payment-options-cash-on-delivery'
  //   };
  //   // // cy.get(`[data-cy=payment-options-grand-total]`).then(el => {
  //   // //   expect(el.text().trim()).to.equal(total);
  //   // // });
  //   cy.get(`[data-cy=${paymentOptionsIds['cashondelivery']}]`).click();
  //   cy.get('[data-cy=payment-options-make-payment]').click();
  // });

  Given('I am on payment details page with title: {string}', (title) => {
    cy.get('[data-cy=payment-details-title]').then(el => {
      expect(el.text().trim()).to.equal(title);
    });
  });
  Then('I submit my user details for dine out', (table) => {
    const dineOut = table.rowsHash();
    cy.get('[data-cy=user-details-name]').type(dineOut["Name"]);
    cy.get('[data-cy=user-details-contact]').type(dineOut["Contact"]);
    cy.contains('Submit').click();
});
Then('I verify payment details for second product', (table) => {
  let data = table.rowsHash();
  cy.get('[data-cy=payment-details-token]').then(el => {
    expect(el.text().trim().length).to.equal(5);
  });
  cy.get('[data-cy=payment-details-grand-total]').then(el => {
    expect(el.text().trim()).to.equal(data['grandTotal']);
  });
  cy.get('[data-cy=payment-details-total-qty]').then(el => {
    expect(el.text().trim()).to.equal(data['totalQty']);
  });
  cy.get('[data-cy=payment-details-subtotal]').then(el => {
    expect(el.text().trim()).to.equal(data['subTotal']);
  });
  cy.get('[data-cy=payment-details-tip]').then(el => {
    expect(el.text().trim()).to.equal(data['tip']);
  });
  if (data['SGST']) {
    cy.get('[data-cy=payment-details-sgst]').then(el => {
      expect(el.text().trim()).to.equal(data['SGST']);
    });
    cy.get('[data-cy=payment-details-sgst%]').then(el => {
      expect(el.text().trim()).to.equal(`SGST ${data['sgst%']} On ${data['subTotal']}`);
    });
  }
  if (data['CGST']) {
    cy.get('[data-cy=payment-details-cgst%]').then(el => {
      expect(el.text().trim()).to.equal(`CGST ${data['cgst%']} On ${data['subTotal']}`);
    });
    cy.get('[data-cy=payment-details-cgst]').then(el => {
      expect(el.text().trim()).to.equal(data['CGST']);
    });
  }
  if (data['tax']) {
    cy.get('[data-cy=payment-details-tax]').then(el => {
      expect(el.text().trim()).to.equal(data['tax']);
    });
    cy.get('[data-cy=payment-details-tax-label]').then(el => {
      expect(el.text().trim()).to.equal(`Tax ${data['tax%']} On ${data['subTotal']}`);
    });
  // }
  // for (let i = 0; i < +data['noOfItems']; i++) {
  //   cy.get('[data-cy=payment-details-item-name]').eq(0).then(el => {
  //     let item = data['items'].split(',')[i];
  //     expect(el.text().trim()).to.equal(item + '.');
  //   });

    // cy.get('[data-cy=payment-details-item-price]').eq(0).then(el => {
    //   let itemPrice = data['itemPrice'].split(',');
    //   expect(el.text().trim()).to.equal(itemPrice);
    // });

    // cy.get('[data-cy=payment-details-item-qty]').eq(0).then(el => {
    //   let itemQty = data['itemQty'].split(',')[i];
    //   expect(el.text().trim()).to.equal(itemQty);
    // });
  }
});
When('I verify total amount to be {string} and select {string}', (total, type) => {
  let paymentOptionsIds = {
    payAtCounter: 'payment-options-pay-at-counter'
  };
  // // cy.get(`[data-cy=payment-options-grand-total]`).then(el => {
  // //   expect(el.text().trim()).to.equal(total);
  // // });
  cy.get(`[data-cy=${paymentOptionsIds['payAtCounter']}]`).click();
  cy.get('[data-cy=payment-options-make-payment]').click();
});
And('I click on pay', () => {
  cy.get('[data-cy=order-tracking-make-payment]').click();
});
When('I go to the payment options', () => {
  cy.get('[data-cy=payment-details-make-payment]').click();
});
And('I go to the track order', () => {
  cy.get('[data-cy=order-tracking-make-payment]').click();
});
And('I add item from track order', () => {
  cy.get('[data-cy=order-tracking-add-item]').click();
});
And('I cancel modify order', () => {
  cy.get('[data-cy=cart-cancel]').click();
});
Then('I modify order', () => {
  cy.get('[data-cy=order-tracking-modify-order]').click();
});
When('I submit my user details for pickup', (table) => {
  const dineOut = table.rowsHash();
  cy.get('[data-cy=user-details-name]').type(dineOut["Name"]);
  cy.get('[data-cy=user-details-contact]').type(dineOut["Contact"]);
  cy.contains('Submit').click({ force: true });
});
When('I submit my user details', (table) => {
  const dineOut = table.rowsHash();
  // cy.get('[data-cy=user-details-name]').type(dineOut["Name"]);
  cy.get('[data-cy=user-details-contact]').type(dineOut["Contact"]);
  cy.contains('Submit').click();
});
And('I clear the contact', () => {
  cy.get('[data-cy=user-details-contact]').clear()
});
And('I clear the name', () => {
  cy.get('[data-cy=user-details-name]').clear()
});
When('I submit my user details for name', (table) => {
  const dineOut = table.rowsHash();
  cy.get('[data-cy=user-details-name]').type(dineOut["Name"]);
  // cy.get('[data-cy=user-details-contact]').type(dineOut["Contact"]);
  cy.contains('Submit').click();
});
Then('I verify in order tracking', async (table) => {
  const expected = table.rowsHash();
  if (expected["Payment Status"]) {
      cy.get('[data-cy=order-tracking-payment-status]').then(element => {
          expect(element.text().trim()).to.equal(expected['Payment Status'])
      });
  }
  if (expect["Status"]) {
      cy.get('[data-cy=order-tracking-status]').then(element => {
          expect(element.text().trim()).to.equal(expected['Status'])
      });
  }
  if (expected["Table"]) {
      cy.get('[data-cy=order-tracking-table]').then(element => {
          expect(element.text().trim()).to.equal(expected['Table'])
      });
  }
  if (expected["Number Of Suborders"]) {
      cy.get('[data-cy=order-tracking-product]').should('have.length', expected['Number Of Suborders']);
  }
  if (expected["grandTotal"]) {
    cy.get('[data-cy=order-tracking-grand-total]').then(el => {
      expect(el.text().trim()).to.equal(expected['grandTotal'])
    });
  }
});
When('I cancel the payment', () => {
  cy.get('[data-cy=payment-details-cancel-payment]').click();
});
And('I go the track order', () => {
  cy.get('[data-cy=order-tracking-make-payment]').click();
});

Given('I am on payment details page with title: {string}', (title) => {
  cy.get('[data-cy=payment-details-title]').then(el => {
    expect(el.text().trim()).to.equal(title);
  });
});


Then('I verify payment details', (table) => {
  let data = table.rowsHash();
  cy.get('[data-cy=payment-details-token]').then(el => {
    expect(el.text().trim().length).to.equal(5);
  });
  cy.get('[data-cy=payment-details-grand-total]').then(el => {
    expect(el.text().trim()).to.equal(data['grandTotal']);
  });
  cy.get('[data-cy=payment-details-total-qty]').then(el => {
    expect(el.text().trim()).to.equal(data['totalQty']);
  });
  cy.get('[data-cy=payment-details-subtotal]').then(el => {
    expect(el.text().trim()).to.equal(data['subTotal']);
  });
  cy.get('[data-cy=payment-details-tip]').then(el => {
    expect(el.text().trim()).to.equal(data['tip']);
  });
  if (data['SGST']) {
    cy.get('[data-cy=payment-details-sgst]').then(el => {
      expect(el.text().trim()).to.equal(data['SGST']);
    });
    cy.get('[data-cy=payment-details-sgst%]').then(el => {
      expect(el.text().trim()).to.equal(`SGST ${data['sgst%']} On ${data['subTotal']}`);
    });
  }
  if (data['CGST']) {
    cy.get('[data-cy=payment-details-cgst%]').then(el => {
      expect(el.text().trim()).to.equal(`CGST ${data['cgst%']} On ${data['subTotal']}`);
    });
    cy.get('[data-cy=payment-details-cgst]').then(el => {
      expect(el.text().trim()).to.equal(data['CGST']);
    });
  }
  if (data['tax']) {
    cy.get('[data-cy=payment-details-tax]').then(el => {
      expect(el.text().trim()).to.equal(data['tax']);
    });
    cy.get('[data-cy=payment-details-tax-label]').then(el => {
      expect(el.text().trim()).to.equal(`Tax ${data['tax%']} On ${data['subTotal']}`);
    });
  }
  for (let i = 0; i < +data['noOfItems']; i++) {
    cy.get('[data-cy=payment-details-item-name]').eq(0).then(el => {
      let item = data['items'].split(',')[i];
      expect(el.text().trim()).to.equal(item + '.');
    });

    cy.get('[data-cy=payment-details-item-price]').eq(0).then(el => {
      let itemPrice = data['itemPrice'].split(',')[i];
      expect(el.text().trim()).to.equal(itemPrice);
    });

    cy.get('[data-cy=payment-details-item-qty]').eq(0).then(el => {
      let itemQty = data['itemQty'].split(',')[i];
      expect(el.text().trim()).to.equal(itemQty);
    });
  }
});
When('I open a category called {string}', async (category) => {
  cy.contains(category).click();
});

Then('I see {int} products in the menu page', async (number) => {
  cy.get('[data-cy=menu-product]').should('have.length', number);
});
And('I go the payment options', () => {
  cy.get('[data-cy=payment-details-make-payment]').click();
});
Then('I am on Order type page', () => {
  cy.get('[data-cy=order-type-header]').click();
});
When('I go back from track order page', () => {
  cy.get('[data-cy=order-tracking-back-button]').click();
});
And('I can see the Live Update title', () => {
  cy.get('[data-cy=live-update-title]').click();
});
Then('the page failed to submit', () => {
  cy.get('[data-cy=invalid-message]').click();
});
Then('contact page failed to submit', () => {
  cy.get('[data-cy=invalid-contact]').click();
});
Given('I am on payment options page with title: {string}', (title) => {
  cy.get('[data-cy=payment-options-title]').then(el => {
    expect(el.text().trim()).to.equal(title);
  });
});
Then('I verify total amount to be {string} and select {string}', (total, type) => {
  let paymentOptionsIds = {
    payAtCounter: 'payment-options-pay-at-counter'
  };
  // // cy.get(`[data-cy=payment-options-grand-total]`).then(el => {
  // //   expect(el.text().trim()).to.equal(total);
  // // });
  cy.get(`[data-cy=${paymentOptionsIds['payAtCounter']}]`).click();
  cy.get('[data-cy=payment-options-make-payment]').click();
});
Then('I make a payment with {string}', (method) => {
  const methods = ["Pay At Counter", "Cash On Delivery", "RazorPay", "Stripe", "PayTM", "PayPal"];
  expect(methods).to.include(method);
  cy.get('[data-cy=order-tracking-make-payment]').click();
  cy.get('[data-cy=payment-details-make-payment]').click();
  if (method == "Pay At Counter" || method == "Cash On Delivery") {
      cy.get('[data-cy=payment-options-input]').eq(0).click();
      cy.get('[data-cy=payment-options-make-payment]').click();
  } else {
      cy.get('[data-cy=payment-options-input]').eq(methods.indexOf(method) - 1).click();
      cy.get('[data-cy=payment-options-make-payment]').click();
      cy.wait(5000);
      // payWithPaymentGateway(method);
      cy.get('button').eq(0).click();
  };
  cy.wait(1000);
});
When('I verify total amount to be {string} and select {string}', (total, type) => {
  let paymentOptionsIds = {
    payAtCounter: 'payment-options-pay-at-counter'
  };
  // // cy.get(`[data-cy=payment-options-grand-total]`).then(el => {
  // //   expect(el.text().trim()).to.equal(total);
  // // });
  cy.get(`[data-cy=${paymentOptionsIds['payAtCounter']}]`).click();
  cy.get('[data-cy=payment-options-make-payment]').click();
});
Then('I am on track order page with title: {string}', (title) => {
  cy.get('[data-cy=track-order-title]').then(el => {
    expect(el.text().trim()).to.equal(title);
  });
});
Given('I am on track order page with title: {string}', (title) => {
  cy.get('[data-cy=track-order-title]').then(el => {
    expect(el.text().trim()).to.equal(title);
  });
});
And('verify the image with quantity', () => {
  cy.get('[data-cy=order-tracking-item-qty-badge]').click();
});
When('I close the notes', () => {
  cy.get('[data-cy=notes-close]').click();
});
Then('Notes dialog box disappears and I am on track order page', () => {
  cy.get('[data-cy=track-order-title]').click();
});
When('I click on Notes', () => {
  cy.get('[data-cy=suborder-grid-notes-icon]').click();
});
Then('I write notes: {string}', (Notes) => {
  cy.get('[data-cy=customization-notes]').type(Notes);
});
And('I save the notes', () => {
  cy.get('[data-cy=notes-save]').click();
});
And('I save the customized ingredient', () => {
  cy.get('[data-cy=customization-save-cancel]').click();
});
Then('I cancel the customized ingredient', () => {
  cy.get('[data-cy=customization-save-button]').click();
});
When('I click on Notes', () => {
  cy.get('[data-cy=suborder-grid-notes-icon]').click();
});
Then('I cancel notes', () => {
  cy.get('[data-cy=notes-cancel]').click();
});
When('I click on customization icon', () => {
  cy.get('[data-cy=suborder-grid-customization-icon]').click();
});
When('I click on Notes', () => {
  cy.get('[data-cy=suborder-grid-notes-icon]').click();
});
Then('dialog box appears with add notes to item', () => {
  cy.get('[data-cy=customization-add-notes]').click();
});
And('I can see the title Customized Ingredients', () => {
  cy.get('[data-cy=customization-ingredients-title]').click();
});
Then('I am on track order page', () => {
  cy.get('[data-cy=track-order-title]').click();
});
And('I can see the title Notes', () => {
  cy.get('[data-cy=title-notes]').click();
});
When('I close the customization dialog box', () => {
  cy.get('[data-cy=custmoziation-close]').click();
});
When('I add {int} quantity of item at {int}', () => {
  cy.get('[data-cy=item-quantity-add]').click();
cy.wait(5000);
});
Then('Dialog box appears with different ingredient', () => {
  cy.get('[data-cy=customization-ingredients-title]').click();
});
Then('I decrease {int} quantity of item at {int}', () => {
// for (let i = 0; i < qty; i++) {
  cy.get('[data-cy=item-quantity-remove]').click();
// }
});
Then('I verify the order in track order', (table) => {
  let data = table.rowsHash();
  cy.wait(1000);
  cy.get(`[data-cy=track-order-token]`).then(el => {
    expect(el.text().trim().length).to.equal(5);
  });
  cy.get(`[data-cy=order-tracking-grand-total]`).then(el => {
    expect(el.text().trim()).to.equal(data['grandTotal']);
  });
  cy.get(`[data-cy=order-tracking-payment-status]`).then(el => {
    expect(el.text().trim()).to.equal(data['paymentStatus']);
  });
  cy.get(`[data-cy=order-tracking-status]`).then(el => {
    expect(el.text().trim()).to.equal(data['status']);
  });
  // cy.get(`[data-cy=order-tracking-table]`).then(el => {
  //   expect(el.text().trim()).to.equal(data['table']);
  // });
  for (let i = 0; i < +data['noOfItems']; i++) {
    cy.get(`[data-cy=order-tracking-name]`).then(el => {
      let item = data['items'].split(',')[i];
      expect(el.text().trim()).to.equal(item + '.');
    });
    // cy.get(`[data-cy=order-tracking-suborder-status]`).children().eq(0).children().eq(0).then(el => {
    //   let item = data['suborderStatus'].split(',')[i];
    //   expect(el.text().trim()).to.equal(item);
    // });
    cy.get(`[data-cy=order-tracking-price]`).then(el => {
      let price = data['itemPrice'].split(',')[i];
      expect(el.text().trim()).to.equal(price);
    });
    cy.get(`[data-cy=order-tracking-item-qty-badge]`).children().eq(0).then(el => {
      let itemQty = data['itemQty'].split(',')[i];
      expect(el.text().trim()).to.equal(itemQty);
    });
    cy.get('[data-cy=track-order-qty-spinner]').eq(i - 1).children().eq(0).children().eq(0).children().eq(1).then(el => {
      let itemQty = data['itemQty'].split(',')[i];
      expect(el.text().trim()).to.equal(itemQty);
    });
  }
});
When('I place the order', () => {
  cy.get('[data-cy=cart-place-order]').click({ force: true });
});
Then('I place the order', () => {
  cy.get('[data-cy=cart-place-order]').click({ force: true });
});

Then('I land on the login page', async () => {
  cy.url().should('include', '/login');
  cy.get('[data-cy=guest-login-option]').should('have.length', 2);
});
Then('I add the product to my cart', () => {
cy.get('[data-cy=customization-add-to-cart]').click();
});
When('I add the product to my cart', () => {
  cy.wait(5000);
  cy.get('[data-cy=customization-add-to-cart]').click();
});
Then('I go to cart page', () => {
cy.get('[data-cy=footer-cart]').click();
});
Then('I am login page I go back', () => {
  cy.get('[data-cy=login-sign-in]').click();
  });
  When('I go back on order type from users details page', () => {
    cy.get('[data-cy=back-button-user-detail]').click();
    });
When('I open a category called {string}', async (category) => {
  cy.contains(category).click();
});
When('I select a product called {string}', async (name) => {
  cy.wait(2000);
cy.contains(name).click();
});
Given('I am on login page with title: {string}', (title) => {
  cy.wait(2000);
  cy.get('[data-cy=login-sign-in]').then(el => {
    expect(el.text().trim()).to.equal(title);
  });
});
Then('I select a product called {string}', async (name) => {
  cy.wait(2000);
  cy.contains(name).click();
  });
And('I verify order in database', () => {
  cy.wait(4000);
  cy.url().then(url => {
    let startI = url.indexOf("order_id=") + 9;
    let endI = url.indexOf("&");
    let id = `${url.substring(startI, endI)}`; 
    // console.log('.............................',startI, endI, id);
    cy.task("getOrderById", { merchantId: "M1_Prakash_Parvat_Test", orderId: id, url: typeof url }).then(order => {
      expect(order.status).to.equal(1);
      expect(order.paymentStatus).to.equal(7);
      expect(order.table).to.equal();
      expect(order.numberofsuborders).to.equal();
      expect(order.grandTotal).to.equal();
      expect(order.noOfItems).to.equal();
      expect(order.totalQuantity).to.equal(2);
      expect(order.subTotal).to.equal();
      expect(order.tip).to.equal(0);
      expect(order.Contact).to.equal();
      expect(order.userDetails.name).to.equal('Riya');
    })
  })
});


