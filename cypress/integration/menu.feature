Feature: Guest Order Menu Page
    Scenario: Open category
        Then I clear the cache of my application
        Given I open guest order menu page and see 1 product
        When I open a category called "Starters"
        Then I see 10 products in the menu page
    Scenario: Select product
        When I select a product called "Veggie Bowl"
        Then I land on the customization page for "Veggie Bowl"
    Scenario: Increment quantity
        When I add a quantity of 5 in the customization page
        Then The total quantity on the customization page is 6
    Scenario: Decrement quantity
        When I remove a quantity of 5 in the customization page
        Then The total quantity on the customization page is 1
    Scenario: Add customizations
        When I add customizations in the customizations page
            | abc    | ₹13.00 |
            | Garlic | ₹13.00 |
            | Cheese | ₹18.00 |
    Scenario: Remove customizations
        When I remove customizations in the customizations page
            | Cheese | ₹13.00 |
            | Garlic | ₹13.00 |
            | abc    | ₹16.00 |
    Scenario: Add to cart
        When I add the product to my cart
        Then I go to cart page
        And I see 1 suborder in the cart page
        And I see the grand total is "₹16.00" and 16 in database
    Scenario: Place order
        When I place the order
        Then I land on the login page
