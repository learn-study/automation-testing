import { Given, Before, Then, And, When } from "cypress-cucumber-preprocessor/steps";
import * as merchant from '../../fixtures/M1_Prakash_Parvat_merchant.json'
import * as settings from "../../fixtures/settings.json";
import * as _ from 'lodash';

// this will get called before each scenario
Before(async () => {
  cy.task("initFirebase", settings.firebase_service_account);
  cy.task("createMerchant", { id: merchant.documentId, doc: merchant.document });
  //   cy.task("createSubcollection", {
  //     merchantId: merchant.documentId,
  //     subCollectionName: "Catalog",
  //     subcollections: merchant.subcollections,
  //   }, { timeout: 240000 });

  //   cy.task("createSubcollection", {
  //     merchantId: merchant.documentId,
  //     subCollectionName: "I18N",
  //     subcollections: merchant.subcollections,
  //   }, { timeout: 240000 });

  //   cy.task("createSubcollection", {
  //     merchantId: merchant.documentId,
  //     subCollectionName: "api_keys",
  //     subcollections: merchant.subcollections,
  //   }, { timeout: 240000 });

  //   cy.task("createSubcollection", {
  //     merchantId: merchant.documentId,
  //     subCollectionName: "counters",
  //     subcollections: merchant.subcollections,
  //   }, { timeout: 240000 });

  //   cy.task("createSubcollection", {
  //     merchantId: merchant.documentId,
  //     subCollectionName: "delivery_pincodes",
  //     subcollections: merchant.subcollections,
  //   }, { timeout: 240000 });

  //   cy.task("createSubcollection", {
  //     merchantId: merchant.documentId,
  //     subCollectionName: "delivery_boy_details",
  //     subcollections: merchant.subcollections,
  //   }, { timeout: 240000 });


  //   cy.task("createSubcollection", {
  //     merchantId: merchant.documentId,
  //     subCollectionName: "itemsType_details",
  //     subcollections: merchant.subcollections,
  //   }, { timeout: 240000 });

  //   cy.task("createSubcollection", {
  //     merchantId: merchant.documentId,
  //     subCollectionName: "messaging_config",
  //     subcollections: merchant.subcollections,
  //   }, { timeout: 240000 });

  //   cy.task("createSubcollection", {
  //     merchantId: merchant.documentId,
  //     subCollectionName: "notifications_config",
  //     subcollections: merchant.subcollections,
  //   }, { timeout: 240000 });

  //   cy.task("createSubcollection", {
  //     merchantId: merchant.documentId,
  //     subCollectionName: "payment_links",
  //     subcollections: merchant.subcollections,
  //   }, { timeout: 240000 });

  //   cy.task("createSubcollection", {
  //     merchantId: merchant.documentId,
  //     subCollectionName: "paypal_config",
  //     subcollections: merchant.subcollections,
  //   }, { timeout: 240000 });

  //   cy.task("createSubcollection", {
  //     merchantId: merchant.documentId,
  //     subCollectionName: "paytm_config",
  //     subcollections: merchant.subcollections,
  //   }, { timeout: 240000 });

  //   cy.task("createSubcollection", {
  //     merchantId: merchant.documentId,
  //     subCollectionName: "stripe_config",
  //     subcollections: merchant.subcollections,
  //   }, { timeout: 240000 });

  //   cy.task("createSubcollection", {
  //     merchantId: merchant.documentId,
  //     subCollectionName: "display_positions",
  //     subcollections: merchant.subcollections,
  //   }, { timeout: 240000 });

  //   cy.task("createSubcollection", {
  //     merchantId: merchant.documentId,
  //     subCollectionName: "razorpay_config",
  //     subcollections: merchant.subcollections,
  //   }, { timeout: 240000 });

  //   cy.task("createSubcollection", {
  //     merchantId: merchant.documentId,
  //     subCollectionName: "seats_details",
  //     subcollections: merchant.subcollections,
  //   }, { timeout: 240000 });

  //   cy.task("createSubcollection", {
  //     merchantId: merchant.documentId,
  //     subCollectionName: "table_configs",
  //     subcollections: merchant.subcollections,
  //   }, { timeout: 240000 });

  //   cy.task("createSubcollection", {
  //     merchantId: merchant.documentId,
  //     subCollectionName: "users",
  //     subcollections: merchant.subcollections,
  //   }, { timeout: 240000 });

  //   cy.task("createSubcollection", {
  //     merchantId: merchant.documentId,
  //     subCollectionName: "merchant_roles",
  //     subcollections: merchant.subcollections,
  //   }, { timeout: 240000 });

});

Given('I open guest order menu page', () => {
  cy.visit('/mIndex/M1_Prakash_Parvat_Test?language=en');
  cy.contains('Prakash', { timeout: 30000 });
  cy.url().should('include', '/M1_Prakash_Parvat_Test');
  cy.task("getOrderById", {}, { timeout: 240000 });
});

Then('I add {string} to cart', (item) => {
  cy.contains(item).click();
  cy.scrollTo(0, 500);
  cy.wait(1000);
  cy.get('[data-cy=customization-add-to-cart]').click();
  cy.wait(1000);
});

Then('I place the order with {string} login', (login) => {
  let loginButtonIds = {
    Guest: 'guest-login-option'
  };
  cy.wait(1000);
  cy.get('[data-cy=guest-header-cart]').click({ force: true });
  cy.get('[data-cy=cart-place-order]').click();
  cy.wait(3000);
  cy.get(`[data-cy=${loginButtonIds[login]}]`).eq(0).click();
});

Then('I select {string} order type', (orderType) => {
  let orderTypeIds = {
    DineIn: 'dine-in-button'
  }
  cy.get(`[data-cy=${orderTypeIds[orderType]}]`).click();
});

And('I go the track order', () => {
  cy.get('[data-cy=order-tracking-make-payment]').click();
});

Then('I verify payment details', (table) => {
  let data = table.rowsHash();
  cy.get('[data-cy=payment-details-token]').then(el => {
    expect(el.text().trim().length).to.equal(5);
  });
  cy.get('[data-cy=payment-details-grand-total]').then(el => {
    expect(el.text().trim()).to.equal(data['grandTotal']);
  });
  cy.get('[data-cy=payment-details-total-qty]').then(el => {
    expect(el.text().trim()).to.equal(data['totalQty']);
  });
  cy.get('[data-cy=payment-details-subtotal]').then(el => {
    expect(el.text().trim()).to.equal(data['subTotal']);
  });
  cy.get('[data-cy=payment-details-tip]').then(el => {
    expect(el.text().trim()).to.equal(data['tip']);
  });
  if (data['SGST']) {
    cy.get('[data-cy=payment-details-sgst]').then(el => {
      expect(el.text().trim()).to.equal(data['SGST']);
    });
    cy.get('[data-cy=payment-details-sgst%]').then(el => {
      expect(el.text().trim()).to.equal(`SGST ${data['sgst%']} On ${data['subTotal']}`);
    });
  }
  if (data['CGST']) {
    cy.get('[data-cy=payment-details-cgst%]').then(el => {
      expect(el.text().trim()).to.equal(`CGST ${data['cgst%']} On ${data['subTotal']}`);
    });
    cy.get('[data-cy=payment-details-cgst]').then(el => {
      expect(el.text().trim()).to.equal(data['CGST']);
    });
  }
  if (data['tax']) {
    cy.get('[data-cy=payment-details-tax]').then(el => {
      expect(el.text().trim()).to.equal(data['tax']);
    });
    cy.get('[data-cy=payment-details-tax-label]').then(el => {
      expect(el.text().trim()).to.equal(`Tax ${data['tax%']} On ${data['subTotal']}`);
    });
  }
  for (let i = 0; i < +data['noOfItems']; i++) {
    cy.get('[data-cy=payment-details-item-name]').eq(0).then(el => {
      let item = data['items'].split(',')[i];
      expect(el.text().trim()).to.equal(item + '.');
    });

    cy.get('[data-cy=payment-details-item-price]').eq(0).then(el => {
      let itemPrice = data['itemPrice'].split(',')[i];
      expect(el.text().trim()).to.equal(itemPrice);
    });

    cy.get('[data-cy=payment-details-item-qty]').eq(0).then(el => {
      let itemQty = data['itemQty'].split(',')[i];
      expect(el.text().trim()).to.equal(itemQty);
    });
  }
});

And('I go the payment options', () => {
  cy.get('[data-cy=payment-details-make-payment]').click();
});

Given('I am on payment options page with title: {string}', (title) => {
  cy.get('[data-cy=payment-options-title]').then(el => {
    expect(el.text().trim()).to.equal(title);
  });
});

Then('I verify total amount to be {string} and select {string}', (total, type) => {
  let paymentOptionsIds = {
    payAtCounter: 'payment-options-pay-at-counter'
  };
  // // cy.get(`[data-cy=payment-options-grand-total]`).then(el => {
  // //   expect(el.text().trim()).to.equal(total);
  // // });
  cy.get(`[data-cy=${paymentOptionsIds['payAtCounter']}]`).click();
  cy.get('[data-cy=payment-options-make-payment]').click();
});

Given('I am on track order page with title: {string}', (title) => {
  cy.get('[data-cy=track-order-title]').then(el => {
    expect(el.text().trim()).to.equal(title);
  });
});

Then('I verify the order in track order', (table) => {
  let data = table.rowsHash();
  cy.wait(1000);
  cy.get(`[data-cy=track-order-token]`).then(el => {
    expect(el.text().trim().length).to.equal(5);
  });
  cy.get(`[data-cy=order-tracking-grand-total]`).then(el => {
    expect(el.text().trim()).to.equal(data['grandTotal']);
  });
  cy.get(`[data-cy=order-tracking-payment-status]`).then(el => {
    expect(el.text().trim()).to.equal(data['paymentStatus']);
  });
  cy.get(`[data-cy=order-tracking-status]`).then(el => {
    expect(el.text().trim()).to.equal(data['status']);
  });
  // cy.get(`[data-cy=order-tracking-table]`).then(el => {
  //   expect(el.text().trim()).to.equal(data['table']);
  // });
  for (let i = 0; i < +data['noOfItems']; i++) {
    cy.get(`[data-cy=order-tracking-name]`).then(el => {
      let item = data['items'].split(',')[i];
      expect(el.text().trim()).to.equal(item + '.');
    });
    cy.get(`[data-cy=order-tracking-suborder-status]`).children().eq(0).children().eq(0).then(el => {
      let item = data['suborderStatus'].split(',')[i];
      expect(el.text().trim()).to.equal(item);
    });
    cy.get(`[data-cy=order-tracking-price]`).then(el => {
      let price = data['itemPrice'].split(',')[i];
      expect(el.text().trim()).to.equal(price);
    });
    cy.get(`[data-cy=order-tracking-item-qty-badge]`).children().eq(0).then(el => {
      let itemQty = data['itemQty'].split(',')[i];
      expect(el.text().trim()).to.equal(itemQty);
    });
    cy.get('[data-cy=track-order-qty-spinner]').eq(i - 1).children().eq(0).children().eq(0).children().eq(1).then(el => {
      let itemQty = data['itemQty'].split(',')[i];
      expect(el.text().trim()).to.equal(itemQty);
    });
  }
});

Given('I am on payment details page with title: {string}', (title) => {
  cy.get('[data-cy=payment-details-title]').then(el => {
    expect(el.text().trim()).to.equal(title);
  });
});

When('I add {int} quantity of item at {int}', (qty, i) => {
  for (let i = 0; i < qty; i++) {
    cy.get('[data-cy=track-order-qty-spinner]').eq(i - 1).children().eq(0).children().eq(0).children().eq(2).click();
  }
  cy.wait(500);
});

When('I decrease {int} quantity of item at {int}', (qty, item) => {
  for (let i = 0; i < qty; i++) {
    cy.get('[data-cy=track-order-qty-spinner]').eq(i - 1).children().eq(0).children().eq(0).children().eq(0).click();
  }
});