Feature: Track Order Page
    @setOrder
    Scenario: Verifying order on track order page
        Given I am on track order page with title: "Track Order"
        Then I verify order on track order page
            | table          | T1              |
            | paymentStatus  | Pending         |
            | status         | Open            |
            | grandTotal     | ₹31.80          |
            | noOfItems      | 1               |
            | items          | Veggie Sandwich |
            | suborderStatus | Open            |
            | itemPrice      | ₹30.00          |
            | itemQty        | 1               |

    Scenario: Increasing suborder quantity in track order page
        Given Item at 1 is "Veggie Sandwich" and quantity is 1
        When I increase quantity by 1 of item at 1
        Then I verify order on track order page
            | table          | T1              |
            | paymentStatus  | Pending         |
            | status         | Open            |
            | grandTotal     | ₹63.60          |
            | noOfItems      | 1               |
            | items          | Veggie Sandwich |
            | suborderStatus | Open            |
            | itemPrice      | ₹60.00          |
            | itemQty        | 2               |
        And I verify order in database

    Scenario: Decreasing suborder quantity in track order page
        Given Item at 1 is "Veggie Sandwich" and quantity is 2
        When I decrease quantity by 1 of item at 1
        Then I verify order on track order page
            | table          | T1              |
            | paymentStatus  | Pending         |
            | status         | Open            |
            | grandTotal     | ₹31.80          |
            | noOfItems      | 1               |
            | items          | Veggie Sandwich |
            | suborderStatus | Open            |
            | itemPrice      | ₹30.00          |
            | itemQty        | 1               |